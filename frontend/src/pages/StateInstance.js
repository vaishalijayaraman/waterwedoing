import React, { Component } from 'react';
import InstanceContainer from '../components/InstanceContainer.js';
import StateInstanceCard from '../components/StateInstanceCard.js';

// Material - UI
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import { Link } from 'react-router-dom';

import { makeStyles } from '@material-ui/core/styles';
import GridList from '@material-ui/core/GridList';
import GridListTile from '@material-ui/core/GridListTile';
import GridListTileBar from '@material-ui/core/GridListTileBar';
import IconButton from '@material-ui/core/IconButton';
import StarBorderIcon from '@material-ui/icons/StarBorder';

import { API } from '../services/api.js';

const StateGrid = (props) => {
  return(
    <StateInstanceCard
      image={props.image}
      instance={props.name}
      ph={props.ph}
      population={props.population}
      sodium={props.sodium}
      turbidity={props.turbidity}
      nitrate={props.nitrate}>
    </StateInstanceCard>
  );
};

const styles = {
  root: {
    justifyContent: 'space-around',
    overflow: 'hidden',
    width: '100%',
  },
  gridList: {
    flexWrap: 'nowrap',
    transform: 'translateZ(0)',
  },
  gridItem: {
    backgroundColor: "#0c6fc1",
  }
};

class StateInstance extends Component {
  constructor(props) {
    super(props);
    this.state = {
      keyS: this.props.match.params.key.substr(1),
      info: [],
      countyTileData: [],
      orgTileData: []
    };
  }

  loadData = (key) => {
    API.getStateVerbose(key).then(json => {
      var organization_links = [];
      var county_links = [];
      var organization_map = {};
      var county_map = {};
      json.map(t =>
        fetch(String.raw`https://api.waterwedoing.me/counties/search?q={"size":500,"_source":["county_name","county_thumb_url"],"query":{"match_all":{}}}`)
        .then((resp) => resp.json())
        .then((resp) => {
        var list = resp['hits']['hits']
        for (var i = 0; i < list.length; i++) {
          county_map[list[i]["_source"]["county_name"]] = list[i]["_source"];
        }
        for (var i = 0; i < t.counties.length; i++) {
          var org = county_map[t.counties[i]]["county_thumb_url"]
          var thumb_url = org;
          if(thumb_url != null) {
            var lastSlash = thumb_url.lastIndexOf("/");
            thumb_url = thumb_url.replace("commons", "commons/thumb") + "/320px-" + thumb_url.substring(lastSlash+1) + ".png";
          }
          county_links.push({
            img: thumb_url,
            title: t.counties[i],
            url: '../counties/:' + t.counties[i]
          });
        }
        this.setState({countyTileData: county_links});
        })
      );
      json.map(t =>
        fetch(String.raw`https://api.waterwedoing.me/organizations/search?q={"size":500,"_source":["org_name","org_identifier","org_image_url"],"query":{"match_all":{}}}`)
        .then((resp) => resp.json())
        .then((resp) => {
        var list = resp['hits']['hits']
        for (var i = 0; i < list.length; i++) {
          organization_map[list[i]["_source"]["org_name"]] = list[i]["_source"];
        }
        console.log("organization_map");
        for (var i = 0; i < t.organizations.length; i++) {
          console.log(t.organizations[i]);
          console.log(organization_map[t.organizations[i][0]]);
          
          var image_url = organization_map[t.organizations[i][0]]["org_image_url"];
          var image = image_url == null ? this.randomImage() : image_url;
          organization_links.push({
            img: image,
            title: t.organizations[i][0],
            url: '../organizations/:' + organization_map[t.organizations[i][0]]["org_identifier"]
          });
        }
        this.setState({orgTileData: organization_links});

        })
      );
      this.setState({
        info: json,
      })
    })
  }
  randomImage = () => {
    var image_urls = [];
    image_urls.push("https://i.imgur.com/WYMlQVD.png");
    image_urls.push("https://i.imgur.com/EC94w0b.png");
    image_urls.push("https://i.imgur.com/6oCfdEK.jpg");
    image_urls.push("https://i.imgur.com/ldZEUVG.png");
    image_urls.push("https://i.imgur.com/8hitHXI.jpg");
    image_urls.push("https://i.imgur.com/ezhOwJz.jpg");
    image_urls.push("https://i.imgur.com/GUwR7k2.png");
    image_urls.push("https://i.imgur.com/qUpoxSs.jpg");
    image_urls.push("https://i.imgur.com/wdB2fyh.jpg");
    image_urls.push("https://i.imgur.com/X0kxIk3.jpg");
  
    return image_urls[Math.floor(Math.random() * image_urls.length)];
    }
  genOrgLinks = (t) => {
    var links = [];
    for(var i = 0; i < t.organizations.length; i++) {
      var link = <a href={"../organizations/:" + t.organizations[i][1]}><button class="instance_button">{t.organizations[i][0]}</button></a>;
      links.push(link);
    }
    return (links);
  }

  genCountyLinks = (t) => {
    var links = [];
    for(var i = 0; i < t.counties.length; i++) {
      var link = <a href={"../counties/:" + t.counties[i]}><button class="instance_button">{t.counties[i]}</button></a>;
      links.push(link);
    }
    return (links);
  }

  render() {
    console.log("KEY")
    console.log(this.state.keyS)
    console.log(typeof(this.state.keyS))

    const states_instance = this.state.info;
    console.log(states_instance)

    return ( 
       <div >
        <br/>
         <br/>
       <InstanceContainer loadData={this.loadData.bind(this)} keyS={this.state.keyS}>
          <Grid container="container" justify="space-around" spacing={3} alignItems="center">
              {states_instance.map(t =>
                <Grid item xs={'auto'} sm={'auto'}>
                <StateInstanceCard image={t.flag_url} instance={t.name} ph={t.ph} turbidity={t.turbidity} population={t.population} sodium={t.sodium} nitrate={t.nitrate}/>
                 <br/><h5 className="Page-header"> Organizations </h5>
                 <div style={styles.root}>
                <GridList style={styles.gridList} cols={3}>
                  {this.state.orgTileData.map(tile => (
                    <GridListTile key={tile.img}>
                      <img src={tile.img} alt={tile.title} />
                      <a href = {tile.url}><GridListTileBar style={styles.gridItem}
                        title={tile.title}
                      /></a>
                    </GridListTile>
                  ))}
                </GridList>
              </div>
               <br/>  <br/> <h5 className="Page-header"> Counties </h5>
               <div style={styles.root}>
                <GridList style={styles.gridList} cols={3}>
                  {this.state.countyTileData.map(tile => (
                    <GridListTile key={tile.img}>
                      <img src={tile.img} alt={tile.title} />
                      <a href = {tile.url}><GridListTileBar style={styles.gridItem}
                        title={tile.title}
                      /></a>
                    </GridListTile>
                  ))}
                </GridList>
              </div>
            </Grid>)}
          </Grid>
      </InstanceContainer>
      </div>
    )
  }
}

export default StateInstance;