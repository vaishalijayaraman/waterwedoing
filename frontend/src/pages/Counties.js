import React, { Component } from 'react';

// Componenets
import ModelContainer from '../components/ModelContainer.js';
import CountyCard from '../components/CountyCard.js';
import CountySearchBar from '../components/CountySearchBar.js';
import CountySortFilter from '../components/CountySortFilter.js';

// Material - UI
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';

import {API} from '../services/api.js';
import RangeFilter from '../services/RangeFilter.js';

// const CountyGrid = (props) => {
//   return(
//     <CountyCard 
//       link={props.link}
//       image={props.image}
//       instance={props.name} 
//       public={props.public}
//       ph={props.ph}
//       sodium={props.sodium} 
//       turbidity={props.turbidity}
//       nitrate={props.nitrate}
//       olddate={props.olddate}
//       newdate={props.newdate}>
//     </CountyCard>
//   );
// }; 

class Counties extends Component {
  constructor(props) {
    super(props);

    this.state = {
      pageNum: 1,
      pageCount: 0,
      info: [],
      // SEARCH PARAMS
      index: "counties",
      county_query: "",
      sort: "county_name.raw",
      sort_direction: "asc",
      filter_ph: null,
      filter_sodium: null,
      filter_nitrate: null,
      filter_turbidity: null,
    };
  }

  loadData = (params) => {
    console.log("LOAD DATA")
    console.log(params.length)

    var fils = [];
    fils.push(params[4]);
    fils.push(params[5]);
    fils.push(params[6]);
    fils.push(params[7]);
    console.log(typeof(fils))

    API.doSearch(this.state.index, params[0], params[1], params[2], params[3], fils).then(json => {
      var p = json.pop();
      var searchList = new Array();
      if (params[1] != "" && params[1] != null) {
        searchList = params[1].replace(/[^A-Za-z0-9\.]+/, " ");
        searchList = searchList.split(" ");
      }
      this.setState({
        info: json,
        pageCount: p,
        pageNum: 1,
        searchTerms: searchList
      })
    })
    console.log(params[0])
    console.log(params[1])
    console.log(params[2])
    console.log(params[3])
  }

  updatePageData = pageNum => {
    this.setState({pageNum: pageNum});
    console.log("PAGE")
    console.log(pageNum)

    // insert pageNum
    let queryParams = [pageNum, this.state.county_query, this.state.sort, this.state.sort_direction, this.state.filter_ph, this.state.filter_sodium, this.state.filter_nitrate, this.state.filter_turbidity]
      
    this.loadData(queryParams)
  }

  updateCountySearchData = query => {
      console.log("UPDATE SEARCH")
      console.log(query)

      if (query !== "") {
        this.setState({county_query: query});
      }
      else{
        this.setState({county_query: ""});
      }

      this.setState({pageNum: 1});

      // insert state_query
      let queryParams = [this.state.pageNum, query, this.state.sort, this.state.sort_direction, this.state.filter_ph, this.state.filter_sodium, this.state.filter_nitrate, this.state.filter_turbidity]

      this.loadData(queryParams)
    }

  updateCountySortData = (sort, sort_direction) => {
    this.setState({pageNum: 1});

    this.setState({sort: sort, sort_direction: sort_direction});
    
    // insert sort, sort_direction
    let queryParams = [this.state.pageNum, this.state.county_query, sort, sort_direction, this.state.filter_ph, this.state.filter_sodium, this.state.filter_nitrate, this.state.filter_turbidity]

    // Load the new data
    this.loadData(queryParams);
  }

  updateCountyFilterPH = (min, max) => {
    if (min == '') { min = null;}
    if (max == '') {max = null;}

    var fil = new RangeFilter("ph", min, max);

    // Update the state's filter params
    this.setState({filter_ph: fil});

    this.setState({pageNum: 1});

    // insert filter
    let queryParams = [this.state.pageNum, this.state.county_query, this.state.sort, this.state.sort_direction, fil, this.state.filter_sodium, this.state.filter_nitrate, this.state.filter_turbidity]

    // Load the new data
    this.loadData(queryParams);
  }

  updateCountyFilterSODIUM = (min, max) => {
    if (min == '') { min = null;}
    if (max == '') {max = null;}

    var fil = new RangeFilter("sodium", min, max);

    // Update the state's filter params
    this.setState({filter_sodium: fil});

    this.setState({pageNum: 1});

    // insert filter
    let queryParams = [this.state.pageNum, this.state.county_query, this.state.sort, this.state.sort_direction, this.state.filter_ph, fil, this.state.filter_nitrate, this.state.filter_turbidity]

    // Load the new data
    this.loadData(queryParams);
  }

  updateCountyFilterNITRATE = (min, max) => {
    if (min == '') { min = null;}
    if (max == '') {max = null;}

    var fil = new RangeFilter("nitrate", min, max);

    // Update the state's filter params
    this.setState({filter_nitrate: fil});

    this.setState({pageNum: 1});

    // insert filter
    let queryParams = [this.state.pageNum, this.state.county_query, this.state.sort, this.state.sort_direction, this.state.filter_ph, this.state.filter_sodium, fil, this.state.filter_turbidity]

    // Load the new data
    this.loadData(queryParams);
  }

  updateCountyFilterTURB = (min, max) => {
    if (min == '') { min = null;}
    if (max == '') {max = null;}

    var fil = new RangeFilter("turbidity", min, max);

    // Update the state's filter params
    this.setState({filter_turbidity: fil});

    this.setState({pageNum: 1});

    // insert filter
    let queryParams = [this.state.pageNum, this.state.county_query, this.state.sort, this.state.sort_direction, this.state.filter_ph, this.state.filter_sodium, this.state.filter_nitrate, fil]

    // Load the new data
    this.loadData(queryParams);
  }


  render() {
      const counties_info = this.state.info;
      console.log("county stuff");
      console.log(counties_info);

    return (
        <div>
            <div>
              <h1 className="Page-header">Counties</h1>
            </div>
            <p className="Model-body">
              Water quality data is available by county, calculated as an average of all water sample sites within that county. These samples are taken by multiple organizations and consolidated by <a href="https://waterservices.usgs.gov/">USGS Water Services</a>. Within each county, you’ll be able to see the water-data organizations which took these samples.
            </p>
            <p className="Model-body">
              Please see <a href="https://water.usgs.gov/data/disclaimer.html">this </a> disclaimer on data taken from the USGS. If you believe a location may have unsafe drinking water, contact the appropriate authorities or the corresponding measuring organizations (listed in each state and county) for more information before taking any action. The entities of waterwedoing.me are not responsible for the accuracy of water quality data or organizational data accessed on this website. 
            </p>
          <br/>
            <div className="County-cards" style={{ paddingLeft: "10%", paddingRight: "10%" }}>
              <ExpansionPanel>
                <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
                    <CountySearchBar query={this.state.county_query} returnSearch={this.updateCountySearchData.bind(this)}/>
                </ExpansionPanelSummary>
                <ExpansionPanelDetails>
                  <CountySortFilter updateCountySortData={this.updateCountySortData.bind(this)} updateCountyFilterPH={this.updateCountyFilterPH.bind(this)} updateCountyFilterNITRATE={this.updateCountyFilterNITRATE.bind(this)} updateCountyFilterSODIUM={this.updateCountyFilterSODIUM.bind(this)} updateCountyFilterTURB={this.updateCountyFilterTURB.bind(this)}/>
                </ExpansionPanelDetails>
              </ExpansionPanel>
              <ExpansionPanel>
                <ExpansionPanelSummary
                  expandIcon={<ExpandMoreIcon />}
                >
                  <div className="Learn-more">Learn More!</div>
                </ExpansionPanelSummary>
                <ExpansionPanelDetails>
                  <Typography>
                    <p className="Model-body">
                      <Typography> <strong> Turbidity: </strong> </Typography> The county’s <a href="https://www.lenntech.com/turbidity.htm">turbidity</a>, which can use varying units of measurement. Turbidity is a measurement of water clarity and is affected by the number of particles present in the water. Turbidity is typically measured in Formazin Turbidity Units (FTUs), Nephelometric Turbidity Units (NTUs), and Jackson Turbidity Units (JTUs). These different units primarily represent the different measurement techniques used, and can generally be treated as equal. <a href="https://www.who.int/water_sanitation_health/hygiene/emergencies/fs2_33.pdf">WHO claims</a> that the turbidity of drinking water should be no greater than 5 NTU/JTU/FTU.
                    </p>
                    <p className="Model-body">
                      <Typography> <strong> Nitrate Level: </strong> </Typography> The county’s <a href="http://psep.cce.cornell.edu/facts-slides-self/facts/nit-heef-grw85.aspx">nitrate concentration</a> measured in milligrams per liter (mg/L). Nitrate is a chemical compound which can occur in water naturally or synthetically. It can be measured as nitrate-N, or nitrate-NO3, depending on whether or not oxygen is included in the measurement. Depending on the state (or sometimes even the county), “nitrate concentration” might refer to either type of measurement. (If unspecified, we have assumed the measurement to be nitrate-N.) <a href="https://www.epa.gov/nutrient-policy-data/estimated-nitrate-concentrations-groundwater-used-drinking">The United States Environmental Protection Agency</a> has a maximum contaminant level of 10 mg/L for nitrate-N (or 45 mg/L for nitrate-NO3), to protect against methemoglobinemia. Nitrate concentration is particularly relevant to individuals whose water systems are private, as private systems are not subject to this federal regulation.
                    </p>
                     <p className="Model-body">
                     <Typography> <strong> Sodium Level: </strong> </Typography> The county’s sodium concentration measured in milligrams per liter (mg/L). Sodium is not the same as salt, and should not be treated as such. <a href="https://www.who.int/water_sanitation_health/dwq/chemicals/sodium.pdf">According to WHO</a>, drinking water the United States can have a sodium concentration anywhere between 1 and 402 mg/L, but only 630 water-systems were sampled to generate this range. A similar study (mentioned in the same WHO paper) found that U.S. sodium concentrations could be up to 1900 mg/L. The amount of sodium one should consume depends on the individual and is hotly debated; an “ideal concentration” is not known for the general population.
                    </p>
                    <p className="Model-body">
                      <Typography> <strong> Oldest Sample Date: </strong> </Typography> The date of the oldest sample used to calculate any of the measurements. Water infrastructure changes over time, possibly altering water quality measurements. While most measurements tend to be taken closer to the modern day (due to technological improvements), it is important to note how far back these samples are recorded. There is always a possibility that some samples may no longer be an accurate modern representation of the water systems they represent.
                    </p>
                    <p className="Model-body">
                      <Typography> <strong> Newest Sample Date: </strong> </Typography>The date of the most recent sample used to calculate any of the measurements. The purpose of this value is more or less the same as the oldest sample date measurement.
                    </p>
                    <p className="Model-body">
                      <Typography> <strong> Average pH Level: </strong> </Typography> The county’s <a href="https://en.wikipedia.org/wiki/PH">pH level</a>. pH is a measurement of how alkaline or acidic a liquid is, where 0 is extremely acidic and 14 is extremely alkaline. Room-temperature pure water has a pH of 7, but most freshwater systems have a pH somewhere between 6 and 8 (according to <a href="https://www.fondriest.com/environmental-measurements/parameters/water-quality/ph/">Fondriest Environmental</a>).
                    </p>
                    <p className="Model-body">
                      <Typography> <strong> State: </strong> </Typography> The name of the state which this county belongs to.
                    </p>
                    <p className="Model-body">
                      <Typography> <strong> Organizations: </strong> </Typography> A list of organizations which have sampled the water quality data available for this county.
                    </p>
                  </Typography>
                </ExpansionPanelDetails>
              </ExpansionPanel>
              <br/>
              <br/>
              <ModelContainer updatePageCallback={this.updatePageData.bind(this)} pageNum={this.state.pageNum} pageCount={this.state.pageCount}>
          <Grid container="container" justify='space-around' spacing={3} alignItems="center">
              {counties_info.map(t => 
            <Grid item xs={'auto'} sm={'auto'}>
            <CountyCard link={"/counties/:"+t.name} image={t.thumb_url} instance={t.name} ph={t.ph} turbidity={t.turbidity} sodium={t.sodium} nitrate={t.nitrate} olddate={t.oldest_date.split("T")[0]} newdate={t.newest_date.split("T")[0]} search={this.state.searchTerms}/>
            </Grid>)}
          </Grid>
          </ModelContainer>
            </div>
        </div>
    );
  }
}
  
  export default Counties;
