import React, { Component } from 'react';
import { Parallax } from 'react-parallax';

// Components
import SplashCard from '../components/SplashCard.js';

// Material-UI
import Grid from '@material-ui/core/Grid';

class Splash extends Component {
    render() {
      return (
      	<div>
	        <div>
				{/* -----dynamic blur-----*/}
				<div className="Splash">
		        <Parallax
		            blur={{ min: -15, max: 15 }}
		            bgImage={require('../files/images/Water.jpg')}
		            bgImageAlt="the water"
		            strength={350}
		            className="Parallax">
		            <div style={{ height: '550px' }} />
		        </Parallax>
				</div>
				<div >
	            <h1 className="Splash-search-header">Jump into the water</h1>
	            <div className="Splash-search-body">Find out more about water quality in America!
		            <br/> <br/> <br/>
		            <br/> <br/> <br/>
	            </div>
	            </div>
	            <div className="Splash-other">What would you like to learn about today?</div>
	        </div>
	        <div className="Splash-cards">
	    		<Grid container="container" spacing={4} justify='center'>
	    		<Grid item className="splash-states-card"><SplashCard link="/states" photo={require('../files/images/splash_state.png')} instance='States' description='Find out water quality data for your state, averaged from the counties!'></SplashCard></Grid>
	    		<Grid item className="splash-counties-card"><SplashCard link="/counties" photo={require('../files/images/splash_county.png')} instance='Counties' description='Find out about the water quality of any county in your state!'></SplashCard></Grid>
	    		<Grid item className="splash-organizations-card"><SplashCard link="/organizations" photo={require('../files/images/splash_organization.png')} instance='Organizations' description='Find local organizations and discover ways to get involved!'></SplashCard></Grid>
	    		</Grid>
	  		</div>
  		</div>
      );
    }
  }
  
  export default Splash;
  