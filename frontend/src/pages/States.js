import React, { Component } from 'react';

// Components
import StateCard from '../components/StateCard.js';
import StateSearchBar from '../components/StateSearchBar.js';
import ModelContainer from '../components/ModelContainer.js';
import StateSortFilter from '../components/StateSortFilter.js';

// Material - UI
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';

import {API} from '../services/api.js';
import RangeFilter from '../services/RangeFilter.js';

// const StateGrid = (props) => {
//   return(
//     <StateCard 
//       link={props.link}
//       image={props.image}
//       instance={props.name} 
//       ph={props.ph}
//       population={props.population}
//       sodium={props.sodium} 
//       turbidity={props.turbidity}
//       nitrate={props.nitrate}>
//     </StateCard>
//   );
// }; 

class States extends Component {
  constructor(props) {
    super(props);

    this.state = {
      pageNum: 1,
      pageCount: 1,
      info: [],
      // SEARCH PARAMS
      index: "states",
      state_query: "",
      sort: "state_name.raw",
      sort_direction: "asc",
      filter_ph: null,
      filter_sodium: null,
      filter_nitrate: null,
      filter_population: null,
      filter_turbidity: null
    };
  }

  loadData = (params) => {
    console.log("LOAD DATA")
    console.log(params.length)

    var fils = [];
    fils.push(params[4]);
    fils.push(params[5]);
    fils.push(params[6]);
    fils.push(params[7]);
    fils.push(params[8]);
    console.log(typeof(fils))

    API.doSearch(this.state.index, params[0], params[1], params[2], params[3], fils).then(json => {
      var p = json.pop();
      console.log("PAGE COUNTTTT");
      console.log(p);
      var searchList = new Array();
      if (params[1] != "" && params[1] != null) {
        searchList = params[1].replace(/[^A-Za-z0-9\.]+/, " ");
        searchList = searchList.split(" ");
      }
      this.setState({
        info: json,
        pageCount: p,
        pageNum: 1,
        searchTerms: searchList
      })
    })
    console.log(params[0])
    console.log(params[1])
    console.log(params[2])
    console.log(params[3])
  }

  updatePageData = pageNum => {
    this.setState({pageNum: pageNum});
    console.log("PAGE")
    console.log(pageNum)

    // insert pageNum
    let queryParams = [pageNum, this.state.state_query, this.state.sort, this.state.sort_direction, this.state.filter_ph, this.state.filter_sodium, this.state.filter_nitrate, this.state.filter_population, this.state.filter_turbidity]
      
    this.loadData(queryParams)
  }

  updateStateSearchData = query => {
      console.log("UPDATE SEARCH")
      console.log(query)

      if (query !== "") {
        this.setState({state_query: query});
      }
      else{
        this.setState({state_query: ""});
      }

      this.setState({pageNum: 1});

      // insert state_query
      let queryParams = [this.state.pageNum, query, this.state.sort, this.state.sort_direction, this.state.filter_ph, this.state.filter_sodium, this.state.filter_nitrate, this.state.filter_population, this.state.filter_turbidity]

      this.loadData(queryParams)
    }

  updateStateSortData = (sort, sort_direction) => {
    this.setState({pageNum: 1});

    this.setState({sort: sort, sort_direction: sort_direction});

    // insert sort, sort_direction
    let queryParams = [this.state.pageNum, this.state.state_query, sort, sort_direction, this.state.filter_ph, this.state.filter_sodium, this.state.filter_nitrate, this.state.filter_population, this.state.filter_turbidity]

    // Load the new data
    this.loadData(queryParams);
  }

  updateStateFilterPH = (min, max) => {
    if (min == '') { min = null;}
    if (max == '') {max = null;}

    var fil = new RangeFilter("ph", min, max);

    // Update the state's filter params
    this.setState({filter_ph: fil});

    this.setState({pageNum: 1});

    // insert filter
    let queryParams = [this.state.pageNum, this.state.state_query, this.state.sort, this.state.sort_direction, fil, this.state.filter_sodium, this.state.filter_nitrate, this.state.filter_population, this.state.filter_turbidity]

    // Load the new data
    this.loadData(queryParams);
  }

  updateStateFilterSODIUM = (min, max) => {
    if (min == '') { min = null;}
    if (max == '') {max = null;}

    var fil = new RangeFilter("sodium", min, max);

    // Update the state's filter params
    this.setState({filter_sodium: fil});

    this.setState({pageNum: 1});

    // insert filter
    let queryParams = [this.state.pageNum, this.state.state_query, this.state.sort, this.state.sort_direction, this.state.filter_ph, fil, this.state.filter_nitrate, this.state.filter_population, this.state.filter_turbidity]

    // Load the new data
    this.loadData(queryParams);
  }

  updateStateFilterNITRATE = (min, max) => {
    if (min == '') { min = null;}
    if (max == '') {max = null;}

    var fil = new RangeFilter("nitrate", min, max);

    // Update the state's filter params
    this.setState({filter_nitrate: fil});

    this.setState({pageNum: 1});

    // insert filter
    let queryParams = [this.state.pageNum, this.state.state_query, this.state.sort, this.state.sort_direction, this.state.filter_ph, this.state.filter_sodium, fil, this.state.filter_population, this.state.filter_turbidity]

    // Load the new data
    this.loadData(queryParams);
  }

  updateStateFilterPOP = (min, max) => {
    if (min == '') { min = null;}
    if (max == '') {max = null;}

    var fil = new RangeFilter("population", min, max);

    // Update the state's filter params
    this.setState({filter_population: fil});

    this.setState({pageNum: 1});

    // insert filter
    let queryParams = [this.state.pageNum, this.state.state_query, this.state.sort, this.state.sort_direction, this.state.filter_ph, this.state.filter_sodium, this.state.filter_nitrate, fil, this.state.filter_turbidity]

    // Load the new data
    this.loadData(queryParams);
  }

  updateStateFilterTURB = (min, max) => {
    if (min == '') { min = null;}
    if (max == '') {max = null;}

    var fil = new RangeFilter("turbidity", min, max);

    // Update the state's filter params
    this.setState({filter_turbidity: fil});

    this.setState({pageNum: 1});

    // insert filter
    let queryParams = [this.state.pageNum, this.state.state_query, this.state.sort, this.state.sort_direction, this.state.filter_ph, this.state.filter_sodium, this.state.filter_nitrate, this.state.filter_population, fil]

    // Load the new data
    this.loadData(queryParams);
  }

  render() {
    const states_info = this.state.info;
    console.log("state stuff");
    console.log(states_info);

    console.log("Sanity check")
    console.log(this.state.state_query)

    return (
      <div>
        <div>
            <h1 className="Page-header">States</h1>
        </div>
        <p className="Model-body">
          Water quality data is available by state and is primarily calculated as an average of the counties of that state. Bear in mind that the accuracy of these state-wide averages is dependent upon the number of counties recorded on this site. Within each state, you’ll be able to see the counties which contributed to these averages, along with the water-data organizations present in that state. 
        </p>
        <p className="Model-body">
          Please see <a href="https://water.usgs.gov/data/disclaimer.html">this </a> disclaimer on data taken from the USGS. If you believe a location may have unsafe drinking water, contact the appropriate authorities or the corresponding measuring organizations (listed in each state and county) for more information before taking any action. The entities of waterwedoing.me are not responsible for the accuracy of water quality data or organizational data accessed on this website. 
        </p>
        <br/>
        <div className="States-cards" style={{ paddingLeft: "10%", paddingRight: "10%" }}>
          <ExpansionPanel>
          <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
              <StateSearchBar query={this.state.state_query} returnSearch={this.updateStateSearchData.bind(this)}/>
          </ExpansionPanelSummary>
          <ExpansionPanelDetails>
            <StateSortFilter updateStateSortData={this.updateStateSortData.bind(this)} updateStateFilterPH={this.updateStateFilterPH.bind(this)} updateStateFilterNITRATE={this.updateStateFilterNITRATE.bind(this)} updateStateFilterSODIUM={this.updateStateFilterSODIUM.bind(this)} updateStateFilterPOP={this.updateStateFilterPOP.bind(this)} updateStateFilterTURB={this.updateStateFilterTURB.bind(this)}/>
          </ExpansionPanelDetails>
        </ExpansionPanel>
        <ExpansionPanel>
          <ExpansionPanelSummary
            expandIcon={<ExpandMoreIcon />}>
            <div className="Learn-more">Learn More!</div>
          </ExpansionPanelSummary>
          <ExpansionPanelDetails>
            <Typography>
              <p className="Model-body">
                <Typography> <strong> Population: </strong> </Typography> The state’s population as measured or estimated by the United States Census Bureau’s Population Estimates Program. The population estimate used is the most recently available for the state. At this time, these estimates range from July of 2020 to July of 2010. If no population estimate is available, the most recent census population measurement is used (April 2010). The population of each state ought to be useful for users who are curious about water quality as it relates to population.
              </p>
              <p className="Model-body">
                <Typography> <strong> Turbidity: </strong> </Typography> The state’s <a href="https://www.lenntech.com/turbidity.htm">turbidity</a>, averaged from each of its counties and has varying units of measurement. Turbidity is a measurement of water clarity and is affected by the number of particles present in the water. These different units primarily represent the different measurement techniques used, and can generally be treated as equal. <a href="https://www.who.int/water_sanitation_health/hygiene/emergencies/fs2_33.pdf">WHO claims</a> that the turbidity of drinking water should be no greater than 5 NTU/JTU/FTU.
              </p>
              <p className="Model-body">
                <Typography> <strong> Nitrate Level: </strong> </Typography> The state’s <a href="http://psep.cce.cornell.edu/facts-slides-self/facts/nit-heef-grw85.aspx">nitrate concentration</a>, averaged from each of its counties and measured as a concentration in milligrams per liter (mg/L). Nitrate is a chemical compound which can occur in water naturally or synthetically. It can be measured as nitrate-N, or nitrate-NO3, depending on whether or not oxygen is included in the measurement. Depending on the state (or sometimes even the county), “nitrate concentration” might refer to either type of measurement. (If unspecified, we have assumed the measurement to be nitrate-N.) <a href="https://www.epa.gov/nutrient-policy-data/estimated-nitrate-concentrations-groundwater-used-drinking">The United States Environmental Protection Agency</a> has a maximum contaminant level of 10 mg/L for nitrate-N (or 45 mg/L for nitrate-NO3), to protect against methemoglobinemia. Nitrate concentration is particularly relevant to individuals whose water systems are private, as private systems are not subject to this federal regulation.
              </p>
              <p className="Model-body">
                <Typography> <strong> Sodium Level: </strong> </Typography> The state’s sodium concentration, averaged from each of its counties and measured as a concentration in milligrams per liter (mg/L). Sodium is not the same as salt, and should not be treated as such. <a href="https://www.who.int/water_sanitation_health/dwq/chemicals/sodium.pdf">According to WHO</a>, drinking water the United States can have a sodium concentration anywhere between 1 and 402 mg/L, but only 630 water-systems were sampled to generate this range. A similar study (mentioned in the same WHO paper) found that U.S. sodium concentrations could be up to 1900 mg/L. The amount of sodium one should consume depends on the individual and is hotly debated; an “ideal concentration” is not known for the general population.
              </p>
              <p className="Model-body">
                <Typography> <strong> Average pH Level: </strong> </Typography> The state’s <a href="https://en.wikipedia.org/wiki/PH">pH level</a>, averaged from each of its counties. pH is a measurement of how alkaline or acidic a liquid is, where 0 is extremely acidic and 14 is extremely alkaline. Room-temperature pure water has a pH of 7, but most freshwater systems have a pH somewhere between 6 and 8 (according to <a href="https://www.fondriest.com/environmental-measurements/parameters/water-quality/ph/">Fondriest Environmental</a>).
              </p>
              <p className="Model-body">
                <Typography> <strong> Organizations: </strong> </Typography> A list of organizations which have sampled the water quality data available for this state.
              </p>
            </Typography>
          </ExpansionPanelDetails>
        </ExpansionPanel>
        <br/>
        <br/>
          <ModelContainer updatePageCallback={this.updatePageData.bind(this)} pageNum={this.state.pageNum} pageCount={this.state.pageCount}>
          { 
            <Grid container="container" justify='space-around' spacing={3} alignItems="center">
              {states_info.map(t => 
            <Grid item xs={'auto'} sm={'auto'}>
            <StateCard link={"/states/:"+t.name} image={t.flag_url} instance={t.name} ph={t.ph} turbidity={t.turbidity} population={t.population} sodium={t.sodium} nitrate={t.nitrate} search={this.state.searchTerms}/>
            </Grid>)}
            </Grid>
          }
          </ModelContainer>
        </div>        
      </div>
    );
    }
  }
  
  export default States;
