import React, { Component } from 'react';

// Components
import AboutCard from '../components/AboutCard.js';
import LogoCard from '../components/LogoCard.js';

// Material UI
import Grid from '@material-ui/core/Grid';

const styles = {
  media: {
    height: 600,
    width: 300
  }
};

class About extends Component {
  constructor(props) {
    super(props);

    this.state = {
      vaishalijayaraman: {
        name:'Vaishali Jayaraman',
        bio:"Vaishali is a senior finishing her CS degree.",
        role:"Frontend",
        link: "https://www.linkedin.com/in/vaishalijayaraman/",
        commits: 0,
        issues: 0,
        unitTests: 0,
      },
      nimbus22: {
        name:'Andrew Kim',
        bio:"Andrew is a CS Student at UT Austin.",
        role:"Backend - API",
        link: "https://www.linkedin.com/in/andrew-kim-32609055/",
        commits: 0,
        issues: 0,
        unitTests: 21,
      },
      antdece: {
        name:'Antonio De Cesare',
        bio:"Antonio's greatest accomplishment is creating the name of this website.",
        role:"Backend - Database",
        link: "https://www.linkedin.com/in/antonio-de-cesare-15279914b/",
        commits: 0,
        issues: 0,
        unitTests: 0,
      },
      bencarter87: {
        name:'Benjamin Carter',
        bio:"Ben likes watching soccer and writing unit tests.",
        role:"Backend - API",
        link: "https://www.linkedin.com/in/benjamin-carter-b28739179/",
        commits: 0,
        issues: 0,
        unitTests: 134,
      },
      shanders: {
        name:'Shane Anders',
        bio:"Shane likes to write on whiteboards and dislikes mittens.",
        role:"Frontend/Backend Interaction",
        link: "https://www.linkedin.com/in/shane-anders-919889152/",
        commits: 0,
        issues: 0,
        unitTests: 0,
      },
      WesternBit: {
        name:'Andrew Hill',
        bio:"Andrew is an incoming junior eager to learn more about CS.",
        role:"Backend - Database",
        link: "https://www.linkedin.com/in/andrewwestbrookhill",
        commits: 0,
        issues: 0,
        unitTests: 0,
      },
      Team: {
        name: 'Team',
        link: "https://gitlab.com/vaishalijayaraman/waterwedoing/tree/master",
        commits: 0,
        issues: 0,
        unitTests: 0,
      }
    };

    this.keyNames = ["Andrew Hill", "Andrew Kim", "Antonio De Cesare", "Benjamin Carter", "Shane Anders", "Vaishali Jayaraman"];
    this.names = {"Andrew Westbrook Hill":"WesternBit", "WesternBit":"WesternBit", "Andrew Hill": "WesternBit", "Andrew Kim":"nimbus22", 
    "Hyun Kim":"nimbus22", "nimbus22":"nimbus22", "Antonio De Cesare":"antdece", "antdece":"antdece", "Benjamin Carter":"bencarter87", 
    "bencarter87":"bencarter87", "Shane Anders":"shanders", "shanders":"shanders", 
    "Vaishali Jayaraman":"vaishalijayaraman", "vaishalijayaraman":"vaishalijayaraman"};


    this.countCommits();
    this.countIssues();
  }

  componentDidMount() {
    let currentComponent = this;
    const usernames = [
      ['WesternBit', 'Andrew Westbrook Hill'],
      ['antdece', 'Antonio De Cesare'],
      ['bencarter87', 'Benjamin Carter'],
      ['nimbus22', 'Hyun Kim'],
      ['shanders', 'Shane Anders']
      ['vaishalijayaraman', 'Vaishali Jayaraman']
    ];
  }

  render() {
    return (
      <div>
          <h1 className="Page-header">About</h1>
          <p className="About-body">
            The goal of our website is to provide a place where anyone in America can find important information about the water in their area. Across the country, water supplies have widely varying contents of pollution and harmful chemicals. The only way to be sure that water is safe to drink is by knowing what is in it, and we hope to make this information easily accessible to the public. We also want to inform our users about organizations which may be of service to them if they have further questions or concerns about the water in their area. 
          </p>
          <p className="About-links">
          <a href="https://documenter.getpostman.com/view/7915444/SVSEuWqr?version=latest" target="_blank">Our Restful API</a>
          <br/>
          <a href="https://gitlab.com/vaishalijayaraman/waterwedoing" target="_blank">Our GitLab Repo</a>
          </p>
          <br/>
          <div className="About-cards">
            <Grid container="container" spacing={5} justify='center'>
            <Grid item><AboutCard link={this.state.nimbus22.link} photo={require('../files/member_pictures/AndrewKim.jpg')} name='Andrew Kim' bio={' ' + this.state['nimbus22']['bio']} role={' ' + this.state['nimbus22']['role']} link={this.state['nimbus22']['link']} commits={' ' + this.state['nimbus22']['commits']} issues={' ' + this.state['nimbus22']['issues']} unitTests={' ' + this.state['nimbus22']['unitTests']}></AboutCard></Grid>
            <Grid item><AboutCard link={this.state.WesternBit.link} photo={require('../files/member_pictures/AndrewHill.jpg')} name='Andrew Westbrook Hill' bio={' ' + this.state['WesternBit']['bio']} role={' ' + this.state['WesternBit']['role']} link={this.state['WesternBit']['link']} commits={' ' + this.state['WesternBit']['commits']} issues={' ' + this.state['WesternBit']['issues']} unitTests={' ' + this.state['WesternBit']['unitTests']}></AboutCard></Grid>
            <Grid item><AboutCard link={this.state.bencarter87.link} photo={require('../files/member_pictures/BenCarter.jpg')} name='Benjamin Carter' bio={' ' + this.state['bencarter87']['bio']} role={' ' + this.state['bencarter87']['role']} link={this.state['bencarter87']['link']} commits={' ' + this.state['bencarter87']['commits']} issues={' ' + this.state['bencarter87']['issues']} unitTests={' ' + this.state['bencarter87']['unitTests']}></AboutCard></Grid>
            <Grid item><AboutCard link={this.state.antdece.link} photo={require('../files/member_pictures/AntonioDeCesare.jpg')} name='Antonio De Cesare' bio={' ' + this.state['antdece']['bio']} role={' ' + this.state['antdece']['role']} link={this.state['antdece']['link']} commits={' ' + this.state['antdece']['commits']} issues={' ' + this.state['antdece']['issues']} unitTests={' ' + this.state['antdece']['unitTests']}></AboutCard></Grid>
            <Grid item><AboutCard link={this.state.shanders.link} photo={require('../files/member_pictures/ShaneAnders.jpg')} name='Shane Anders' bio={' ' + this.state['shanders']['bio']} role={' ' + this.state['shanders']['role']} link={this.state['shanders']['link']} commits={' ' + this.state['shanders']['commits']} issues={' ' + this.state['shanders']['issues']} unitTests={' ' + this.state['shanders']['unitTests']}></AboutCard></Grid>
            <Grid item><AboutCard link={this.state.vaishalijayaraman.link} photo={require('../files/member_pictures/VaishaliJayaraman.jpg')} name='Vaishali Jayaraman' bio={' ' + this.state['vaishalijayaraman']['bio']} role={' ' + this.state['vaishalijayaraman']['role']} link={this.state['vaishalijayaraman']['link']} commits={' ' + this.state['vaishalijayaraman']['commits']} issues={' ' + this.state['vaishalijayaraman']['issues']} unitTests={' ' + this.state['vaishalijayaraman']['unitTests']}></AboutCard></Grid>
            </Grid>
            <br/><br/>
            <p className="About-links">
              <a href="https://gitlab.com/vaishalijayaraman/waterwedoing/commits/master" target="_blank">Total Commits: {this.state["Team"].commits}</a> 
              <br/>
              <a href="https://gitlab.com/vaishalijayaraman/waterwedoing/issues" target="_blank">Total Issues: {this.state["Team"].issues}</a> 
              <br/>
              <a href="https://gitlab.com/vaishalijayaraman/waterwedoing/pipelines" target="_blank">Total Unit Tests: 155</a> 
            </p>
          </div>
          <h1 className="Page-header">Tools</h1>
          <p className="Resources-body">
            Our team used the following tools, libraries, frameowrks and languages to build this web application. 
          </p>
          <br/>
          <div className="Logo-cards">
            <Grid container="container" spacing={10} justify='center'>
            <Grid item="item">
              <LogoCard name='AWS' link='https://aws.amazon.com' photo={require('../files/logo_images/awslogo.png')} alt_text='AWS Logo' description="Cloud computing services."></LogoCard>
            </Grid>
            <Grid item="item">
            	<LogoCard name='D3' link='https://d3js.org/' photo={require('../files/logo_images/d3logo.png')} alt_text='D3 Logo' description="Manipulate documents based on data."></LogoCard>
            </Grid>
            <Grid item="item">
              <LogoCard name='Docker' link='https://www.docker.com/' photo={require('../files/logo_images/dockerlogo.jpg')} alt_text='Docker Logo' description="A development platform to build, share and run apps."></LogoCard>
            </Grid>
            <Grid item="item">
              <LogoCard name='Elastic Search' link='https://www.elastic.co/products/elasticsearch' photo={require('../files/logo_images/elasticsearchlogo.png')} alt_text='Elastic Search Logo' description="A distributed, RESTful search and analytics engine."></LogoCard>
            </Grid>
            <Grid item="item">
              <LogoCard name='Flask' link='http://flask.pocoo.org/' photo={require('../files/logo_images/flasklogo.png')} alt_text='Flask Logo' description="A Python web application framework."></LogoCard>
            </Grid>
            <Grid item="item">
              <LogoCard name='Flask Restless' link='https://flask-restless.readthedocs.io/en/stable/' photo={require('../files/logo_images/flaskrestlesslogo.png')} alt_text='Flask Restless Logo' description="Create RESTful JSON APIs from SQLAlchemy models."></LogoCard>
            </Grid>
            <Grid item="item">
              <LogoCard name='Flask Sqlalchemy' link='https://flask-sqlalchemy.palletsprojects.com/en/2.x/' photo={require('../files/logo_images/flasksqlalchemylogo.png')} alt_text='Flask SQLA Logo' description="Support SQLAlchemy in Flask app."></LogoCard>
            </Grid>
            <Grid item="item">
              <LogoCard name='GitLab' link='https://about.gitlab.com' photo={require('../files/logo_images/gitlablogo.png')} alt_text='GitLab Logo' description="A DevOps Tool to plan and manage source code."></LogoCard>
            </Grid>
            <Grid item="item">
              <LogoCard name='Material UI' link='https://material-ui.com' photo={require('../files/logo_images/materialuilogo.png')} alt_text='Material UI Logo' description="A component based React User Interface Framework."></LogoCard>
            </Grid>
            <Grid item="item">
              <LogoCard name='Mocha' link='https://mochajs.org/' photo={require('../files/logo_images/mochalogo.png')} alt_text='Mocha Logo' description="A JavaScript Testing Framework."></LogoCard>
            </Grid>
             <Grid item="item">
              <LogoCard name='Namecheap' link='https://www.namecheap.com/' photo={require('../files/logo_images/namecheaplogo.png')} alt_text='Name Cheap Logo' description="Domain names."></LogoCard>
            </Grid>
            <Grid item="item">
              <LogoCard name='PostgreSQL' link='https://www.postgresql.org/' photo={require('../files/logo_images/postgresqllogo.png')} alt_text='PostgreSQL Logo' description="An open source Relational Database."></LogoCard>
            </Grid>
            <Grid item="item">
              <LogoCard name='Postman' link='https://www.getpostman.com' photo={require('../files/logo_images/postmanlogo.png')} alt_text='Postman Logo' description="An API development and design environment."></LogoCard>
            </Grid>
            <Grid item="item">
              <LogoCard name='React' link='https://reactjs.org/' photo={require('../files/logo_images/reactlogo.svg')} alt_text='React Logo' description="JavaScript Library for creating user interfaces."></LogoCard>
            </Grid>
            <Grid item="item">
              <LogoCard name='Selenium' link='https://www.seleniumhq.org/' photo={require('../files/logo_images/seleniumlogo.png')} alt_text='Selenium Logo' description="A automated browser based GUI testing library."></LogoCard>
            </Grid>
            <Grid item="item">
              <LogoCard name='Slack' link='https://slack.com' photo={require('../files/logo_images/slacklogo.png')} alt_text='Slack Logo' description="A team collaboration software tool."></LogoCard>
            </Grid>
            </Grid>
          </div>
          <br/>
          <h1 className="Page-header">Data</h1>
          <p className="Resources-body">
            Our team used the following APIs to get data for our different models.
          </p>
          <br/>
          <div className="Logo-cards">
            <Grid container="container" spacing={8} justify='center'>
            <Grid item="item">
              <LogoCard name='USCensus' link='https://www.census.gov/' photo={require('../files/logo_images/censuslogo.png')} alt_text='US Census Logo' description="United States Census Bureau."></LogoCard>
            </Grid>
            <Grid item="item">
              <LogoCard name='USGS' link='https://www.usgs.gov' photo={require('../files/logo_images/usgslogo.svg')} alt_text='USGS Logo' description="United States Geological Survey."></LogoCard>
            </Grid>
            <Grid item="item">
              <LogoCard name='Wikipedia' link='https://www.wikipedia.org' photo={require('../files/logo_images/wikilogo.png')} alt_text='Wiki Logo' description="A free online encyclopedia."></LogoCard>
            </Grid>
          </Grid>
          </div>
      </div>

    );
  }

  countIssues(){
    var url = 'https://gitlab.com/api/v4/projects/12978758/issues?state=closed&per_page=100&page=';
    for (var i = 1; i <= 10; i++){
      var full_url = url + i
      fetch(full_url)
          .then(response => response.json())
          .then(data => {
            //process the data
            for (var i in data) {
              let issue_data = data[i];
              if (issue_data.closed_by.name in this.names) {
                this.state[this.names[issue_data.closed_by.name]].issues += 1;
                this.state["Team"].issues += 1
              }
            }
            this.setState({})
          })
          .catch(e => {
            console.log(e)
          })
    }
  }

  countCommits(){
   var urlDev = 'https://gitlab.com/api/v4/projects/12978758/repository/commits?ref_name=development&per_page=100&page=';
    var urlMas = 'https://gitlab.com/api/v4/projects/12978758/repository/commits?per_page=100&page=';
    
    for (var i = 1; i <= 10; i++) {
      var full_urlDev = urlDev + i;
      fetch(full_urlDev)
        .then(response => response.json())
        .then(data => {
          //process the data
          for (var i in data) {
            let commit_data = data[i];
            this.state[this.names[commit_data.author_name]].commits += 1;
            this.state["Team"].commits += 1
          }
          this.setState({})
        })
        .catch(e => {
          console.log(e)
        })
    }
  }

}

export default About;
