import React, { Component } from 'react';

// Components
import StateCard from '../components/StateCard.js';
import OrganizationCard from '../components/OrganizationCard.js';
import CountyCard from '../components/CountyCard.js';
import ModelContainer from '../components/ModelContainer.js';
import UniversalSearchBar from '../components/UniversalSearchBar.js';

// Material - UI
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import SwipeableViews from 'react-swipeable-views';

import { createMuiTheme } from '@material-ui/core/styles';
import { ThemeProvider } from '@material-ui/styles';

import { API } from '../services/api.js';

const avenir = {
  fontFamily: 'Avenir',
  src: `
    local('Avenir'),
    url('../files/fonts/Avenir-Medium.ttf'),
  `,
}

const myTheme = createMuiTheme({
  palette: {
    primary: {
      main: "#ffffff",
    },
  },
  typography : {
      fontFamily: [
      'Avenir',
      ].join(','),
  },
  overrides: {
    MuiCssBaseline: {
      '@global': {
        '@font-face': [avenir],
      },
    },
  },
});

// const StateGrid = (props) => {
//   return(
//     <StateCard 
//       link={props.link}
//       image={props.image}
//       instance={props.name} 
//       ph={props.ph}
//       population={props.population}
//       sodium={props.sodium} 
//       turbidity={props.turbidity}
//       nitrate={props.nitrate}>
//     </StateCard>
//   );
// }; 

// const CountyGrid = (props) => {
//   return(
//     <CountyCard 
//       link={props.link}
//       image={props.image}
//       instance={props.name} 
//       public={props.public}
//       ph={props.ph}
//       sodium={props.sodium} 
//       turbidity={props.turbidity}
//       nitrate={props.nitrate}
//       olddate={props.olddate}
//       newdate={props.newdate}>
//     </CountyCard>
//   );
// }; 

// const OrganizationGrid = (props) => {
//   return(
//     <OrganizationCard 
//       link={props.link}
//       image={props.image}
//       instance={props.name} 
//       type={props.type}
//       city={props.city}
//       state={props.state}
//       public={props.public}
//       address={props.address}
//       email={props.email}
//       phone={props.phone}
//       identifier={props.identifier}>
//     </OrganizationCard>
//   );
// }; 

class SearchPage extends Component {
  constructor(props) {
    super(props);

    this.state = {
      pageNum_states: 1,
      pageNum_counties: 1,
      pageNum_orgs: 1,

      pageCount_states: 0,
      pageCount_counties: 0,
      pageCount_orgs: 0,

      info_states: [],
      info_counties: [],
      info_orgs: [],

      query: "",
      tabIndex: 0,

      searchTerms: [],
    };
  }

  randomImage = (t) => {
  if(t.img_url != null) {
    return t.img_url;
  }
  var image_urls = [];
  image_urls.push("https://i.imgur.com/WYMlQVD.png");
  image_urls.push("https://i.imgur.com/EC94w0b.png");
  image_urls.push("https://i.imgur.com/6oCfdEK.jpg");
  image_urls.push("https://i.imgur.com/ldZEUVG.png");
  image_urls.push("https://i.imgur.com/8hitHXI.jpg");
  image_urls.push("https://i.imgur.com/ezhOwJz.jpg");
  image_urls.push("https://i.imgur.com/GUwR7k2.png");
  image_urls.push("https://i.imgur.com/qUpoxSs.jpg");
  image_urls.push("https://i.imgur.com/wdB2fyh.jpg");
  image_urls.push("https://i.imgur.com/X0kxIk3.jpg");

  return image_urls[Math.floor(Math.random() * image_urls.length)];
  }

  componentDidMount() {
    if (this.state.query !== "") {
      this.updateGlobalSearchData(this.state.query);
    }
  }

  updateGlobalSearchData = query => {
    if (query !== "") {this.setState({query: query.split(" ")});} 
    else {this.setState({query: ""});}

    this.setState({pageNum_states: 1});
    this.setState({pageNum_orgs: 1});
    this.setState({pageNum_counties: 1});

    var searchList = new Array();
    var query_parse = query
    if (query_parse !== "" && query_parse !== null) {
        searchList = query_parse.replace(/[^A-Za-z0-9\.]+/, " ");
        searchList= searchList.split(" ");
    }

    this.setState({searchTerms: searchList});

    // Add in the filter and sort params
    let queryParams = [query, this.state.pageNum_states, this.state.pageNum_counties, this.state.pageNum_orgs]

    this.loadStateData(queryParams);
    this.loadCountyData(queryParams);
    this.loadOrgData(queryParams);
  }

  // PARAMS: query, pageNum_state, pageNum_county, pageNum_org
  loadStateData = (params) => {
    API.doSearch("states", params[1], params[0], null, null, null).then(json => {
      var ps = json.pop();
      this.setState({
        info_states: json,
        pageCount_states: ps,
        pageNum_states: 1,
      })
    })
  }

  // PARAMS: query, pageNum_state, pageNum_county, pageNum_org
  loadCountyData = (params) => {
     API.doSearch("counties", params[2], params[0], null, null, null).then(json => {
      var pc = json.pop();
      this.setState({
        info_counties: json,
        pageCount_counties: pc,
        pageNum_counties: 1,
      })
    })
  }

  // PARAMS: query, pageNum_state, pageNum_county, pageNum_org
  loadOrgData = (params) => {
    API.doSearch("organizations", params[3], params[0], null, null, null).then(json => {
      var po = json.pop();
      this.setState({
        info_orgs: json,
        pageCount_orgs: po,
        pageNum_orgs: 1,
      })
    })
  }

  updateStatePage = pageNum => {
    this.setState({pageNum_states: pageNum});

    let queryParams = [this.state.query, pageNum, this.state.pageNum_counties, this.state.pageNum_orgs]

    this.loadStateData(queryParams);
  }

  updateCountyPage = pageNum => {
    this.setState({pageNum_counties: pageNum});

    let queryParams = [this.state.query, this.state.pageNum_states, pageNum, this.state.pageNum_orgs]

    this.loadCountyData(queryParams);
  }

  updateOrgPage = pageNum => {
    this.setState({pageNum_orgs: pageNum});

    let queryParams = [this.state.query, this.state.pageNum_states, this.state.pageNum_counties, pageNum]

    this.loadOrgData(queryParams);
  }

  handleChange = (event, tabIndex) => {
    this.setState({tabIndex});
  };

  handleChangeIndex = index => {
    this.setState({tabIndex: index});
  };

  render() {
    const states_info = this.state.info_states;
    const org_info = this.state.info_orgs;
    const counties_info = this.state.info_counties;
    console.log("state stuff");
    console.log(states_info);
    console.log("org stuff");
    console.log(org_info);
    console.log("county stuff");
    console.log(counties_info);

    return (
      <div>
        <div>
            <h1 className="Page-header">Search</h1>
        </div>
        <div>
        <div className="Search-body">Find out more about water quality in America!</div>
        <br/> <br/>
        <div style={{ paddingLeft: "33.33%", }}>
          <UniversalSearchBar query={this.state.query} returnSearch={this.updateGlobalSearchData.bind(this)}/>
        </div>
        <br/> <br/>
        </div>
        <br/>

        <ThemeProvider theme={myTheme}>
        <Tabs paddingTop="100px" value={this.state.tabIndex} onChange={this.handleChange} indicatorColor="primary" fontFamily="Avenir" variant="fullWidth">
        <Tab id="StateSearchTab" label={"States: " + this.state.pageCount_states + " pages"} style={{ backgroundColor: "#0c6fc1", color: "#ffffff", textTransform: 'capitalize', fontSize:'18px'}}/>
        <Tab id="CountySearchTab" label={"Counties: " + this.state.pageCount_counties + " pages"} style={{ backgroundColor: "#0c6fc1", color: "#ffffff", textTransform: 'capitalize', fontSize:'18px'}}/>
        <Tab id="OrganizationSearchTab" label={"Organizations: " + this.state.pageCount_orgs + " pages"} style={{backgroundColor: "#0c6fc1", color: "#ffffff",textTransform: 'capitalize', fontSize:'18px'}}/>
        </Tabs>  
        </ThemeProvider>

        <SwipeableViews axis={'x'} index={this.state.tabIndex} onChangeIndex={this.handleChangeIndex}>
        
        <TabContainer>
          {
            <div className="State-cards" style={{ paddingLeft: "10%", paddingRight: "10%" }}>
            <ModelContainer updatePageCallback={this.updateStatePage.bind(this)} pageNum={this.state.pageNum_states} pageCount={this.state.pageCount_states}>
              <Grid container="container" justify='space-around' spacing={3} alignItems="center">
                <h3 className="Page-header">States</h3>
                  {states_info.map(t => 
                <Grid item xs={'auto'} sm={'auto'}>
                <StateCard link={"/states/:"+t.name} image={t.flag_url} instance={t.name} ph={t.ph} turbidity={t.turbidity} population={t.population} sodium={t.sodium} nitrate={t.nitrate} search={this.state.searchTerms}/>
                </Grid>)}
              </Grid>
            </ModelContainer>
            </div>
          }
        </TabContainer>

        <TabContainer>
          {
            <div className="County-cards" style={{ paddingLeft: "10%", paddingRight: "10%" }}>
            <ModelContainer updatePageCallback={this.updateCountyPage.bind(this)} pageNum={this.state.pageNum_counties} pageCount={this.state.pageCount_counties}>
              <Grid container="container" justify='space-around' spacing={3} alignItems="center">
                <h3 className="Page-header">Counties</h3>
                  {counties_info.map(t => 
              <Grid item xs={'auto'} sm={'auto'}>
              <CountyCard link={"/counties/:"+t.name} image={t.thumb_url} instance={t.name} ph={t.ph} turbidity={t.turbidity} sodium={t.sodium} nitrate={t.nitrate} olddate={t.oldest_date.split("T")[0]} newdate={t.newest_date.split("T")[0]} search={this.state.searchTerms}/>
              </Grid>)}
              </Grid>
            </ModelContainer>
            </div>
          }
        </TabContainer>

        <TabContainer>
          {
            <div className="Organization-cards" style={{ paddingLeft: "10%", paddingRight: "10%" }}>
            <ModelContainer updatePageCallback={this.updateOrgPage.bind(this)} pageNum={this.state.pageNum_orgs} pageCount={this.state.pageCount_orgs}>
              <Grid container="container" justify='space-around' spacing={3} alignItems="center">
                <h3 className="Page-header">Organizations</h3>
                  {org_info.map(t => 
                <Grid item xs={'auto'} sm={'auto'}>
                <OrganizationCard link={"/organizations/:"+t.id} image={this.randomImage(t)} id={t.id} instance={t.name}  state={t.states} phone={t.phone} address={t.address} type={t.type}  city={t.city} email={t.email} search={this.state.searchTerms}/>
                </Grid>)}
              </Grid>
            </ModelContainer>
            </div>
          }
        </TabContainer>

        </SwipeableViews>

      </div>
    );
    }
  }

function TabContainer({children, dir}) {
  return (<Typography component="div" dir={dir} style={{
      padding: 8 * 3,
    }}>
    {children}
  </Typography>);
}
  
export default SearchPage;
  