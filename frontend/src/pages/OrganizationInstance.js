import React, { Component } from 'react';
import { Link } from 'react-router-dom';

// Components
import InstanceContainer from '../components/InstanceContainer.js';
import OrganizationInstanceCard from '../components/OrganizationInstanceCard.js';

// Material - UI
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';

import { makeStyles } from '@material-ui/core/styles';
import GridList from '@material-ui/core/GridList';
import GridListTile from '@material-ui/core/GridListTile';
import GridListTileBar from '@material-ui/core/GridListTileBar';
import IconButton from '@material-ui/core/IconButton';
import StarBorderIcon from '@material-ui/icons/StarBorder';

import { API } from '../services/api.js';

const OrganizationGrid = (props) => {
  return(
    <OrganizationInstanceCard 
      image={props.image}
      instance={props.name} 
      type={props.type}
      city={props.city}
      state={props.state}
      public={props.public}
      address={props.address}
      email={props.email}
      phone={props.phone}
      identifier={props.identifier}>
    </OrganizationInstanceCard>
  );
}; 

const styles = {
  root: {
    justifyContent: 'space-around',
    overflow: 'hidden',
    width: '100%',
  },
  gridList: {
    flexWrap: 'nowrap',
    transform: 'translateZ(0)',
  },
  gridItem: {
    backgroundColor: "#0c6fc1",
  }
};

class OrganizationInstance extends Component {
  constructor(props) {
    super(props);
    this.state = {
      keyS: this.props.match.params.key.substr(1),
      info: [],
      stateTileData: [],
      countyTileData: []
    };
  }

  loadData = (key) => {
    API.getOrganizationVerbose(key).then(json => {
      var state_links = [];
      var county_links = [];
      var state_map = {};
      var county_map = {};
      json.map(t =>
        fetch(String.raw`https://api.waterwedoing.me/counties/search?q={"size":500,"_source":["county_name","county_thumb_url"],"query":{"match_all":{}}}`)
        .then((resp) => resp.json())
        .then((resp) => {
        var list = resp['hits']['hits']
        for (var i = 0; i < list.length; i++) {
          county_map[list[i]["_source"]["county_name"]] = list[i]["_source"];
        }
        for (var i = 0; i < t.counties.length; i++) {
          var org = county_map[t.counties[i]]["county_thumb_url"]
          var thumb_url = org;
          if(thumb_url != null) {
            var lastSlash = thumb_url.lastIndexOf("/");
            thumb_url = thumb_url.replace("commons", "commons/thumb") + "/320px-" + thumb_url.substring(lastSlash+1) + ".png";
          }
          county_links.push({
            img: thumb_url,
            title: t.counties[i],
            url: '../counties/:' + t.counties[i]
          });
        }
        this.setState({countyTileData: county_links});
        })
      );
      json.map(t =>
        fetch(String.raw`https://api.waterwedoing.me/states/search?q={"size":500,"_source":["state_name","state_flag_url"],"query":{"match_all":{}}}`)
        .then((resp) => resp.json())
        .then((resp) => {
        var list = resp['hits']['hits']
        for (var i = 0; i < list.length; i++) {
          state_map[list[i]["_source"]["state_name"]] = list[i]["_source"];
        }
        for (var i = 0; i < t.states.length; i++) {
          state_links.push({
            img: state_map[t.states[i]]["state_flag_url"],
            title: t.states[i],
            url: '../states:' + t.states[i]
          });
        }
        this.setState({stateTileData: state_links});

        })
      );
      this.setState({
        info: json,
      });
    });
  }

  randomImage = (t) => {
    if(t.img_url != null) {
      return t.img_url;
    }
    var image_urls = [];
    image_urls.push("https://i.imgur.com/WYMlQVD.png");
    image_urls.push("https://i.imgur.com/EC94w0b.png");
    image_urls.push("https://i.imgur.com/6oCfdEK.jpg");
    image_urls.push("https://i.imgur.com/ldZEUVG.png");
    image_urls.push("https://i.imgur.com/8hitHXI.jpg");
    image_urls.push("https://i.imgur.com/ezhOwJz.jpg");
    image_urls.push("https://i.imgur.com/GUwR7k2.png");
    image_urls.push("https://i.imgur.com/qUpoxSs.jpg");
    image_urls.push("https://i.imgur.com/wdB2fyh.jpg");
    image_urls.push("https://i.imgur.com/X0kxIk3.jpg");
  
    return image_urls[Math.floor(Math.random() * image_urls.length)];
    }




  render() {
    console.log("KEY")
    console.log(this.state.keyS)
    console.log(typeof(this.state.keyS))

    const organization_instance = this.state.info;
    console.log(organization_instance)

    return (
       <div>
       <InstanceContainer loadData={this.loadData.bind(this)} keyS={this.state.keyS}>
          <Grid container="container" justify='space-around' spacing={3} alignItems="center">
              {organization_instance.map(t => 
            <Grid item xs={'auto'} sm={'auto'}>
            <OrganizationInstanceCard image={this.randomImage(t)} id={t.id} instance={t.name}  state={t.states} phone={t.phone} address={t.address} type={t.type}  city={t.city} email={t.email}/>
            <br/> <h5 className="Page-header"> Counties </h5>
              <div style={styles.root}>
                <GridList style={styles.gridList} cols={3}>
                  {this.state.countyTileData.map(tile => (
                    <GridListTile key={tile.img}>
                      <img src={tile.img} alt={tile.title} />
                      <a href = {tile.url}><GridListTileBar style={styles.gridItem}
                        title={tile.title}
                      /></a>
                    </GridListTile>
                  ))}
                </GridList>
              </div>
              <br/>  <br/> <h5 className="Page-header"> States </h5>
              <div style={styles.root}>
                <GridList style={styles.gridList} cols={3}>
                  {this.state.stateTileData.map(tile => (
                    <GridListTile key={tile.img}>
                      <img src={tile.img} alt={tile.title} />
                      <a href = {tile.url}><GridListTileBar style={styles.gridItem}
                        title={tile.title}
                      /></a>
                    </GridListTile>
                  ))}
                </GridList>
              </div>
            </Grid>)}
          </Grid>
      </InstanceContainer>
      </div>
    )
  }
}

export default OrganizationInstance;
