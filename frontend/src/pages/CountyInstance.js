import React, { Component, Container } from 'react';
import { Link } from 'react-router-dom';
import ReactDOM from 'react-dom';

// Components
import InstanceContainer from '../components/InstanceContainer.js';
import CountyInstanceCard from '../components/CountyInstanceCard.js';

// Material - UI
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';

import { makeStyles } from '@material-ui/core/styles';
import GridList from '@material-ui/core/GridList';
import GridListTile from '@material-ui/core/GridListTile';
import GridListTileBar from '@material-ui/core/GridListTileBar';
import IconButton from '@material-ui/core/IconButton';
import StarBorderIcon from '@material-ui/icons/StarBorder';

import { API } from '../services/api.js';

const CountyGrid = (props) => {
  return(
    <Container
      image={props.image}
      instance={props.name} 
      type={props.type}
      city={props.city}
      state={props.state}
      public={props.public}
      address={props.address}
      email={props.email}
      phone={props.phone}
      identifier={props.identifier}>
      <Typography>
        <h4>{props.name}</h4>
      </Typography>
      <Typography>
        <img src={props.image}></img>
      </Typography>
    </Container>
  );
}; 

const styles = {
  root: {
    justifyContent: 'space-around',
    overflow: 'hidden',
    width: '100%',
  },
  gridList: {
    flexWrap: 'nowrap',
    transform: 'translateZ(0)',
  },
  gridItem: {
    backgroundColor: "#0c6fc1",
  }
};

class CountyInstance extends Component {
  constructor(props) {
    super(props);
    this.state = {
      keyS: this.props.match.params.key.substr(1).replace(/%20/g, " "),
      info: [],
      stateTileData: [],
      orgTileData: []
    };
  }

  loadData = (key) => {
    API.getCountyVerbose(key).then(json => {
      var organization_links = [];
      var state_links = [];
      var organization_map = {};
      var state_map = {};
      json.map(t =>
        fetch(String.raw`https://api.waterwedoing.me/organizations/search?q={"size":500,"_source":["org_name","org_identifier","org_image_url"],"query":{"match_all":{}}}`)
        .then((resp) => resp.json())
        .then((resp) => {
        var list = resp['hits']['hits']
        for (var i = 0; i < list.length; i++) {
          organization_map[list[i]["_source"]["org_name"]] = list[i]["_source"];
        }
        console.log("organization_map");
        for (var i = 0; i < t.organizations.length; i++) {
          console.log(t.organizations[i]);
          console.log(organization_map[t.organizations[i][0]]);
          
          var image_url = organization_map[t.organizations[i][0]]["org_image_url"];
          var image = image_url == null ? this.randomImage() : image_url;
          organization_links.push({
            img: image,
            title: t.organizations[i][0],
            url: '../organizations/:' + organization_map[t.organizations[i][0]]["org_identifier"]
          });
        }
        this.setState({orgTileData: organization_links});
        })
      );
      json.map(t =>
        fetch(String.raw`https://api.waterwedoing.me/states/search?q={"size":500,"_source":["state_name","state_flag_url"],"query":{"match_all":{}}}`)
        .then((resp) => resp.json())
        .then((resp) => {
        var list = resp['hits']['hits']
        for (var i = 0; state_links.length == 0; i++) {
          if (t.state === list[i]['_source']['state_name']) {
            state_links.push({
              img: list[i]['_source']['state_flag_url'],
              title: list[i]['_source']['state_name'],
              url: '../states/:' + list[i]['_source']['state_name']
            });
            break;
          }
        }

        this.setState({stateTileData: state_links});

        })
      );
      this.setState({
        info: json,
      })
    })
  }

  randomImage = () => {
    var image_urls = [];
    image_urls.push("https://i.imgur.com/WYMlQVD.png");
    image_urls.push("https://i.imgur.com/EC94w0b.png");
    image_urls.push("https://i.imgur.com/6oCfdEK.jpg");
    image_urls.push("https://i.imgur.com/ldZEUVG.png");
    image_urls.push("https://i.imgur.com/8hitHXI.jpg");
    image_urls.push("https://i.imgur.com/ezhOwJz.jpg");
    image_urls.push("https://i.imgur.com/GUwR7k2.png");
    image_urls.push("https://i.imgur.com/qUpoxSs.jpg");
    image_urls.push("https://i.imgur.com/wdB2fyh.jpg");
    image_urls.push("https://i.imgur.com/X0kxIk3.jpg");
  
    return image_urls[Math.floor(Math.random() * image_urls.length)];
    }

  render() {
    console.log("KEY")
    console.log(this.state.keyS)
    console.log(typeof(this.state.keyS))

    const county_instance = this.state.info;
    console.log(county_instance)

    return (
       <div >
        <br/>
         <br/>
       <InstanceContainer loadData={this.loadData.bind(this)} keyS={this.state.keyS}>
          <Grid container="container" justify='space-around' spacing={3} alignItems="center">
              {county_instance.map(t =>
            <Grid item xs={'auto'} sm={'auto'}>
            <CountyInstanceCard image={t.thumb_url} instance={t.name} ph={t.ph} turbidity={t.turbidity} population={t.population} sodium={t.sodium} nitrate={t.nitrate} olddate={t.oldest_date.split("T")[0]} newdate={t.newest_date.split("T")[0]}/>
	           <br/><h5 className="Page-header"> Organizations </h5>
             <div style={styles.root}>
                <GridList style={styles.gridList} cols={3}>
                  {this.state.orgTileData.map(tile => (
                    <GridListTile key={tile.img}>
                      <img src={tile.img} alt={tile.title} />
                      <a href = {tile.url}><GridListTileBar style={styles.gridItem}
                        title={tile.title}
                      /></a>
                    </GridListTile>
                  ))}
                </GridList>
              </div>
            <br/>  <br/> <h5 className="Page-header"> States </h5>
            <div style={styles.root}>
                <GridList style={styles.gridList} cols={3}>
                  {this.state.stateTileData.map(tile => (
                    <GridListTile key={tile.img}>
                      <img src={tile.img} alt={tile.title}/>
                      <a href = {tile.url}><GridListTileBar style={styles.gridItem}
                        title={tile.title}
                      /></a>
                    </GridListTile>
                  ))}
                </GridList>
              </div>
            </Grid>)}
          </Grid>
      </InstanceContainer>
      </div>
    )
  }
}

export default CountyInstance;
