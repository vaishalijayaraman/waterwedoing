import React, { Component } from 'react';

// Components
import OrganizationCard from '../components/OrganizationCard.js';
import OrganizationSearchBar from '../components/OrganizationSearchBar.js';
import ModelContainer from '../components/ModelContainer.js';
import OrganizationSortFilter from '../components/OrganizationSortFilter.js';

// Material - UI
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';

import {API} from '../services/api.js';
import TermFilter from '../services/TermFilter.js';
import ExistsFilter from '../services/ExistsFilter.js';

// const OrganizationGrid = (props) => {
//   return(
//     <OrganizationCard 
//       link={props.link}
//       image={props.image}
//       instance={props.name} 
//       type={props.type}
//       city={props.city}
//       state={props.state}
//       public={props.public}
//       address={props.address}
//       email={props.email}
//       phone={props.phone}
//       identifier={props.identifier}>
//     </OrganizationCard>
//   );
// }; 

class Organizations extends Component {
  constructor(props) {
    super(props);

    this.state = {
      pageNum: 1,
      pageCount: 0,
      info: [],
      // SEARCH PARAMS
      index: "organizations",
      organization_query: "",
      sort: "org_name.raw",
      sort_direction: "asc",
      filter_email: null,
      filter_type: null,
      filter_address: null,
      filter_phone: null,
      filter_city: null,
    };
  }

  loadData = (params) => {
    console.log("LOAD DATA")
    console.log(params.length)

    var fils = [];
    fils.push(params[4]);
    fils.push(params[5]);
    fils.push(params[6]);
    fils.push(params[7]);
    fils.push(params[8]);
    console.log(typeof(fils))

    API.doSearch("organizations", params[0], params[1], params[2], params[3], fils).then(json => {
      var p = json.pop();
      var searchList = new Array();
      if (params[1] !== "" && params[1] !== null) {
        searchList = params[1].replace(/[^A-Za-z0-9\.]+/, " ");
        searchList = searchList.split(" ");
      }
      this.setState({
        info: json,
        pageCount: p,
        pageNum: 1,
        searchTerms: searchList
      })
    })
    console.log(params[0])
    console.log(params[1])
    console.log(params[2])
    console.log(params[3])
  }

  randomImage = (t) => {
  if(t.image_url != null) {
    return t.image_url;
  }
  else {
  var image_urls = [];
    image_urls.push("https://i.imgur.com/WYMlQVD.png");
    image_urls.push("https://i.imgur.com/EC94w0b.png");
    image_urls.push("https://i.imgur.com/6oCfdEK.jpg");
    image_urls.push("https://i.imgur.com/ldZEUVG.png");
    image_urls.push("https://i.imgur.com/8hitHXI.jpg");
    image_urls.push("https://i.imgur.com/ezhOwJz.jpg");
    image_urls.push("https://i.imgur.com/GUwR7k2.png");
    image_urls.push("https://i.imgur.com/qUpoxSs.jpg");
    image_urls.push("https://i.imgur.com/wdB2fyh.jpg");
    image_urls.push("https://i.imgur.com/X0kxIk3.jpg");

    return image_urls[Math.floor(Math.random() * image_urls.length)];
  }
  }

  updatePageData = pageNum => {
    this.setState({pageNum: pageNum});
    console.log("PAGE")
    console.log(pageNum)

    // insert pageNum
    let queryParams = [pageNum, this.state.organization_query, this.state.sort, this.state.sort_direction, this.state.filter_email, this.state.filter_type, this.state.filter_address, this.state.filter_phone, this.state.filter_city]
      
    this.loadData(queryParams)
  }

  updateOrganizationSearchData = query => {
      console.log("UPDATE SEARCH")
      console.log(query)

      if (query !== "") {
        this.setState({organization_query: query});
      }
      else{
        this.setState({organization_query: ""});
      }

      this.setState({pageNum: 1});

      // insert state_query
      let queryParams = [this.state.pageNum, query, this.state.sort, this.state.sort_direction, this.state.filter_email, this.state.filter_type, this.state.filter_address, this.state.filter_phone, this.state.filter_city]

      this.loadData(queryParams)
    }

  updateOrganizationSortData = (sort, sort_direction) => {
    this.setState({pageNum: 1});

    this.setState({sort: sort, sort_direction: sort_direction});

    // insert sort, sort_direction
    let queryParams = [this.state.pageNum, this.state.organization_query, sort, sort_direction, this.state.filter_email, this.state.filter_type, this.state.filter_address, this.state.filter_phone, this.state.filter_city]

    // Load the new data
    this.loadData(queryParams);
  }

  updateOrganizationFilterEMAIL = (apply_email_filter) => {
    var fil;

    if (apply_email_filter) {  
      fil = new ExistsFilter("email");
    } else {
      fil = null;
    }

    // Update the state's filter params
    this.setState({filter_email: fil});

    this.setState({pageNum: 1});

    // insert filter
    let queryParams = [this.state.pageNum, this.state.organization_query, this.state.sort, this.state.sort_direction, fil, this.state.filter_type, this.state.filter_address, this.state.filter_phone, this.state.filter_city]

    // Load the new data
    this.loadData(queryParams);
  }

  updateOrganizationFilterADD = (apply_address_filter) => {
    var fil;

    if (apply_address_filter) {  
      fil = new ExistsFilter("address");
    } else {
      fil = null;
    }

    // Update the state's filter params
    this.setState({filter_address: fil});

    this.setState({pageNum: 1});

    // insert filter
    let queryParams = [this.state.pageNum, this.state.organization_query, this.state.sort, this.state.sort_direction, this.state.filter_email, this.state.filter_type, fil, this.state.filter_phone, this.state.filter_city]

    // Load the new data
    this.loadData(queryParams);
  }

  updateOrganizationFilterPHONE = (apply_phone_filter) => {
    var fil;
    
    if (apply_phone_filter) {  
      fil = new ExistsFilter("phone");
    } else {
      fil = null;
    }

    // Update the state's filter params
    this.setState({filter_phone: fil});

    this.setState({pageNum: 1});

    // insert filter
    let queryParams = [this.state.pageNum, this.state.organization_query, this.state.sort, this.state.sort_direction, this.state.filter_email, this.state.filter_type, this.state.filter_address, fil, this.state.filter_city]

    // Load the new data
    this.loadData(queryParams);
  }

  updateOrganizationFilterCITY = (apply_city_filter) => {
    var fil;
    
    if (apply_city_filter) {  
      fil = new ExistsFilter("city");
    } else {
      fil = null;
    }

    // Update the state's filter params
    this.setState({filter_city: fil});

    this.setState({pageNum: 1});

    // insert filter
    let queryParams = [this.state.pageNum, this.state.organization_query, this.state.sort, this.state.sort_direction, this.state.filter_email, this.state.filter_type, this.state.filter_address, this.state.filter_phone, fil]

    // Load the new data
    this.loadData(queryParams);
  }

  updateOrganizationFilterTYPE = (orgType) => {
    var fil;

    if (orgType === "" || orgType === "?*"){ //Should be called "any"
      fil = new ExistsFilter("org_type");
    } else if (orgType === "null") {
      fil = null;
    } else {
      fil = new TermFilter("org_type", orgType);
    }

    // Update the state's filter params
    this.setState({filter_type: fil});

    this.setState({pageNum: 1});

    // insert filter
    let queryParams = [this.state.pageNum, this.state.organization_query, this.state.sort, this.state.sort_direction, this.state.filter_email, fil, this.state.filter_address, this.state.filter_phone, this.state.filter_city]

    // Load the new data
    this.loadData(queryParams);
  }

    render() {
      const organizations_info = this.state.info;
      console.log("org stuff");
      console.log(organizations_info);

      return (
        <div>
          <div>
              <h1 className="Page-header">Organizations</h1>
          </div>
          <p className="Model-body">
            There are many organizations responsible for measuring water quality data in the United States. These organizations may be federal, state, or privately owned. To assist you in environmental engagement, we’ve provided the contact information of these organizations. Bear in mind that some organizations may have chosen to limit or exclude their contact information (rather than giving it to the USGS). You can also view the state(s) and county/counties that each organization collected their water samples from.
          </p>
        <br/>
          <div className="Organization-cards" style={{ paddingLeft: "10%", paddingRight: "10%" }}>
            <ExpansionPanel>
              <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
                  <OrganizationSearchBar query={this.state.organization_query} returnSearch={this.updateOrganizationSearchData.bind(this)}/>
              </ExpansionPanelSummary>
              <ExpansionPanelDetails>
                <OrganizationSortFilter updateOrganizationSortData={this.updateOrganizationSortData.bind(this)} updateOrganizationFilterEMAIL={this.updateOrganizationFilterEMAIL.bind(this)} updateOrganizationFilterTYPE={this.updateOrganizationFilterTYPE.bind(this)} updateOrganizationFilterADD={this.updateOrganizationFilterADD.bind(this)} updateOrganizationFilterPHONE={this.updateOrganizationFilterPHONE.bind(this)} updateOrganizationFilterCITY={this.updateOrganizationFilterCITY.bind(this)}/>
              </ExpansionPanelDetails>
            </ExpansionPanel>
            <ExpansionPanel>
	            <ExpansionPanelSummary
	              expandIcon={<ExpandMoreIcon />}
	            >
	              <div className="Learn-more">Learn More!</div>
	            </ExpansionPanelSummary>
	            <ExpansionPanelDetails>
	              <Typography>
	                <p className="Model-body">
			            <Typography> <strong> Type: </strong> </Typography> The organization’s level of government involvement. Organizations may be federal, state, local, private, volunteer, etc.
			          </p>
			          <p className="Model-body">
			            <Typography> <strong> City: </strong> </Typography>  The city which the organization is based out of.
			          </p>
			          <p className="Model-body">
			            <Typography> <strong> Counties: </strong> </Typography>  The counties which this organization has sampled water data from.
			          </p>
			          <p className="Model-body">
			            <Typography> <strong> State: </strong> </Typography>  The states which the this organization has sampled water data from.
			          </p>
			          <p className="Model-body">
			            <Typography> <strong> Email: </strong> </Typography>  The organization’s official contact email.
			          </p>
			          <p className="Model-body">
			            <Typography> <strong> Phone Number: </strong> </Typography>  The organization’s official contact phone number.
			          </p>
			          <p className="Model-body">
			            <Typography> <strong> Address: </strong> </Typography>  The street address of the organization. It can be a street number and street name, P.O. box, or anything else the organization chooses to list as its address.
			          </p>
			          <p className="Model-body">
			            <Typography> <strong> Identifier: </strong> </Typography>  The organization’s Water Quality Exchange Organization ID <a href="https://www3.epa.gov/storet/wqx/wqxweb.html">as listed under the United States Environmental Protection Agency</a>. These organizations can often have similar names, so an organization’s official identifier is typically more reliable than its formal name. For example, the California State Water Resource Control Board (identifier CABEACH_WQX) is different from the California State Water Resources Control Board (identifier CEDEN).
			          </p>
	              </Typography>
	            </ExpansionPanelDetails>
	          </ExpansionPanel>
            <br/>
            <br/>  
            <ModelContainer updatePageCallback={this.updatePageData.bind(this)} pageNum={this.state.pageNum} pageCount={this.state.pageCount}>
          <Grid container="container" justify='space-around' spacing={3} alignItems="center">
              {organizations_info.map(t => 
            <Grid item xs={'auto'} sm={'auto'}>
            <OrganizationCard link={"/organizations/:"+t.id} image={this.randomImage(t)} id={t.id} instance={t.name}  state={t.states} phone={t.phone} address={t.address} type={t.type}  city={t.city} email={t.email} search={this.state.searchTerms}/>
            </Grid>)}
          </Grid>
          </ModelContainer>
          </div>
        </div>
      );
    }
  }
  
  export default Organizations;
