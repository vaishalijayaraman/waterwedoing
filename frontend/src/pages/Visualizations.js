import React, { Component } from 'react';

// Visuals - Water We Doing
import MapPh from '../visuals/MapPh.js';
import BarOrg from '../visuals/BarOrg.js';
import HeatMap from '../visuals/HeatMap.js';

// Visuals - 2020 Guide
import HexIssueGraph from '../visuals/2020guide/HexIssueGraph.js';
import WordCloud from '../visuals/2020guide/WordCloud.js';

// Material UI
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import SwipeableViews from 'react-swipeable-views';

import { createMuiTheme } from '@material-ui/core/styles';
import { ThemeProvider } from '@material-ui/styles';

const avenir = {
  fontFamily: 'Avenir',
  src: `
    local('Avenir'),
    url('../files/fonts/Avenir-Medium.ttf'),
  `,
}

const myTheme = createMuiTheme({
  palette: {
    primary: {
      main: "#ffffff",
    },
  },
  typography : {
      fontFamily: [
      'Avenir',
      ].join(','),
  },
  overrides: {
    MuiCssBaseline: {
      '@global': {
        '@font-face': [avenir],
      },
    },
  },
});

class Visualizations extends Component {
  constructor(props) {
    super(props);

    this.state = {
      tabIndex: 0,
    };
  }

  handleChange = (event, tabIndex) => {
    this.setState({tabIndex});
  };

  handleChangeIndex = index => {
    this.setState({tabIndex: index});
  };

  render() {
    return (
      <div>
          <h1 className="Page-header">Visualizations</h1>
          <p className="About-body">
            <center>Here are some cool visualizations for our website!</center>
          </p>
          <br/>

          <ThemeProvider theme={myTheme}>
          <Tabs paddingTop="100px" value={this.state.tabIndex} onChange={this.handleChange} indicatorColor="primary" fontFamily="Avenir" variant="fullWidth">
          <Tab id="StateSearchTab" label={"Water We Doing"} style={{ backgroundColor: "#0c6fc1", color: "#ffffff", textTransform: 'capitalize', fontSize:'18px'}}/>
          <Tab id="CountySearchTab" label={"2020 Guide"} style={{ backgroundColor: "#0c6fc1", color: "#ffffff", textTransform: 'capitalize', fontSize:'18px'}}/>
          </Tabs>
          </ThemeProvider>

           <SwipeableViews axis={'x'} index={this.state.tabIndex} onChangeIndex={this.handleChangeIndex}>
            <TabContainer>
              {
                <div className="" style={{ paddingLeft: "10%", paddingRight: "10%" }}>
                  <Grid container="container" justify='space-around' spacing={3} alignItems="center">
                    <h4 className="Page-header">Number of organizations in States</h4>
                    <BarOrg/>
                    <h4 className="Page-header">Heat Map of the Turbidity in Counties</h4>
                    <HeatMap/>
                    <h4 className="Page-header">Map of pH of States</h4>
                    <MapPh/>
                  </Grid>
                </div>
              }
            </TabContainer>

            <TabContainer>
              {
                <div className="" style={{ paddingLeft: "10%", paddingRight: "10%" }}>
                  <Grid container="container" justify='space-around' spacing={3} alignItems="center">
                    <h4 className="Page-header">Word Cloud of Candidate Names</h4>
                    <WordCloud/>
                    <h4 className="Page-header">Heptagonal Candidate Advocacy Graphs</h4>
                    <HexIssueGraph/>
                  </Grid>
                </div>
              }
            </TabContainer>
          </SwipeableViews>
          <div>
          </div>
      </div>

    );
  }
}

function TabContainer({children, dir}) {
  return (<Typography component="div" dir={dir} style={{
      padding: 8 * 3,
    }}>
    {children}
  </Typography>);
}

export default Visualizations;
