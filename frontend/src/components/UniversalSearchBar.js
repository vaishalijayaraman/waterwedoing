import React, {Component} from 'react'
import PropTypes from 'prop-types';

// Material - UI
import Typography from '@material-ui/core/Typography';
import Paper from '@material-ui/core/Paper';
import InputBase from '@material-ui/core/InputBase';
import IconButton from '@material-ui/core/IconButton';
import SearchIcon from '@material-ui/icons/Search';

const styles = {
  root: {
    padding: '2px 12px',
    display: 'flex',
    alignItems: 'center',
    width: 500,
    height: 50,
    backgroundColor: 'rgb(255,255,255,0.8)',
    borderRadius: 50
  },
  input: {
    marginLeft: 8,
    flex: 1,
    color: '#000'
  },
  iconButton: {
    padding: 10,
    color: '#0c6fc1'
  },
  divider: {
    width: 1,
    height: 28,
    margin: 4
  }
};

class UniversalSearchBar extends Component {
  constructor(props) {
    super(props);

    this.state = {
      query: '' // search query
    };
  }

  handleInputChange = event => {
    this.setState({query: event.target.value})
  }

  handleKeyPress = (event) => {
    if (event.key === 'Enter') {
      this.handleSearchClick();  
    }
  }

  handleSearchClick = () => {
    this.props.returnSearch(this.state.query);
  }

  render() {
    return (
    <Paper style={styles.root} elevation={1}>
      <Typography variant='h6' align='left' style={{
          'paddingLeft' : '10px',
          'paddingBottom' : '0px',
          'paddingRight' : '12px',
          'color' : '#0c6fc1'
        }}><strong>Search</strong></Typography>

      <InputBase id="UniversalSearchBar" style={styles.input} value={this.state.query} placeholder='States, Couties and Organizations!' onChange={this.handleInputChange} onKeyPress={this.handleKeyPress}/>
      <IconButton id="UniversalSearchButton" style={styles.iconButton} aria-label="Search" onClick={this.handleSearchClick}>
      <SearchIcon/>
      </IconButton>
    </Paper>)
  }
}

UniversalSearchBar.propTypes = {
  classes: PropTypes.object.isRequired
};

export default UniversalSearchBar;
