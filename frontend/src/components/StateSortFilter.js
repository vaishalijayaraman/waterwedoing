import React, {Component} from 'react'
import PropTypes from "prop-types";

// Components
import Sort from './Sort.js';
import RangeFilter from './RangeFilter.js';

// Material - UI
import Typography from '@material-ui/core/Typography';

const choices = [
  {
    value: '_score',
    displayName: 'Relevance',
    defOrder: 'asc'
  }, {
    value: 'state_name.raw',
    displayName: 'Name',
    defOrder: 'asc'
  }, {
    value: 'population',
    displayName: 'Population',
    defOrder: 'desc'
  },{
    value: 'ph',
    displayName: 'pH',
    defOrder: 'asc'
  }, {
    value: 'turbidity',
    displayName: 'Turbidity',
    defOrder: 'asc'
  }, {
    value: 'nitrate',
    displayName: 'Nitrate',
    defOrder: 'asc'
  }, {
    value: 'sodium',
    displayName: 'Sodium',
    defOrder: 'asc'
  }
];

class StateSortFilter extends Component {
  render() {
    return (
      <div>
        <div>
        <div className="filter-sort-container">
          <div className="filter-body">
            <Typography variant='subtitle1' align='left' style={{'fontWeight' : '600','color': '#0c6fc1'}}>Filter By</Typography>
            <div className="filter-item">
              <Typography variant='subtitle2' align='left' style={{'fontWeight' : '600'}}>Population <RangeFilter returnFilter={this.props.updateStateFilterPOP.bind(this)}/></Typography>
            </div>
            <div className="filter-item">
              <Typography variant='subtitle2' align='left' style={{'fontWeight' : '600'}}>pH <RangeFilter returnFilter={this.props.updateStateFilterPH.bind(this)}/></Typography>
            </div>
            <div className="filter-item">
              <Typography variant='subtitle2' align='left' style={{'fontWeight' : '600'}}>Sodium <RangeFilter returnFilter={this.props.updateStateFilterSODIUM.bind(this)}/></Typography>     
            </div>
            <div className="filter-item">
              <Typography variant='subtitle2' align='left' style={{'fontWeight' : '600'}}>Nitrate <RangeFilter returnFilter={this.props.updateStateFilterNITRATE.bind(this)}/></Typography>
            </div>
            <div className="filter-item">
              <Typography variant='subtitle2' align='left' style={{'fontWeight' : '600'}}>Turbidity <RangeFilter returnFilter={this.props.updateStateFilterTURB.bind(this)}/></Typography>
            </div>
          </div>
          <div className="sort-body">
            <Typography variant='subtitle1' align='left' width='25%' style={{'fontWeight' : '600','color': '#0c6fc1'}}>Sort By</Typography>
            <Sort options={choices} updateSortData={this.props.updateStateSortData.bind(this)}/>
          </div>
      </div>
      </div>
    </div>)
  }
}

StateSortFilter.propTypes = {
  classes: PropTypes.object.isRequired
};

export default StateSortFilter;
