import React from 'react';
import PropTypes from 'prop-types';

// Material UI
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Typography from '@material-ui/core/Typography';
import Tooltip from '@material-ui/core/Tooltip';
import { withStyles } from '@material-ui/core/styles';


import PhScale from './PhScaleInstance.js';

const styles = {
  card: {
    maxWidth: '100%'
  },
  media: {
    height: 558,
  },
  link: {
    textDecoration: 'none'
  },
  image: {
    height: 300,
    align:'center'
  },
  textSpacing: {
    marginBottom: '10px',
    align: 'center'
  }
};

const LightTooltip = withStyles(theme => ({
  tooltip: {
    backgroundColor: theme.palette.common.white,
    color: 'rgba(0, 0, 0, 0.87)',
    boxShadow: theme.shadows[1],
    fontSize: 14,
    maxWidth: '80%',
    paddingTop: 15,
  },
}))(Tooltip);

const pop_text = <><p className="Model-body">
  <Typography> <strong> Population: </strong> </Typography> The state’s population as measured or estimated by the United States Census Bureau’s Population Estimates Program. The population estimate used is the most recently available for the state. At this time, these estimates range from July of 2020 to July of 2010. If no population estimate is available, the most recent census population measurement is used (April 2010). The population of each state ought to be useful for users who are curious about water quality as it relates to population.
</p></>
const turb_text = <><p className="Model-body">
  <Typography> <strong> Turbidity: </strong> </Typography> The state’s <a href="https://www.lenntech.com/turbidity.htm">turbidity</a>, averaged from each of its counties and has varying units of measurement. Turbidity is a measurement of water clarity and is affected by the number of particles present in the water. These different units primarily represent the different measurement techniques used, and can generally be treated as equal. <a href="https://www.who.int/water_sanitation_health/hygiene/emergencies/fs2_33.pdf">WHO claims</a> that the turbidity of drinking water should be no greater than 5 NTU/JTU/FTU.
</p></>
const nit_text = <><p className="Model-body">
  <Typography> <strong> Nitrate Level: </strong> </Typography> The state’s <a href="http://psep.cce.cornell.edu/facts-slides-self/facts/nit-heef-grw85.aspx">nitrate concentration</a>, averaged from each of its counties and measured as a concentration in milligrams per liter (mg/L). Nitrate is a chemical compound which can occur in water naturally or synthetically. It can be measured as nitrate-N, or nitrate-NO3, depending on whether or not oxygen is included in the measurement. Depending on the state (or sometimes even the county), “nitrate concentration” might refer to either type of measurement. (If unspecified, we have assumed the measurement to be nitrate-N.) <a href="https://www.epa.gov/nutrient-policy-data/estimated-nitrate-concentrations-groundwater-used-drinking">The United States Environmental Protection Agency</a> has a maximum contaminant level of 10 mg/L for nitrate-N (or 45 mg/L for nitrate-NO3), to protect against methemoglobinemia. Nitrate concentration is particularly relevant to individuals whose water systems are private, as private systems are not subject to this federal regulation.
</p></>
const sod_text = <><p className="Model-body">
  <Typography> <strong> Sodium Level: </strong> </Typography> The state’s sodium concentration, averaged from each of its counties and measured as a concentration in milligrams per liter (mg/L). Sodium is not the same as salt, and should not be treated as such. <a href="https://www.who.int/water_sanitation_health/dwq/chemicals/sodium.pdf">According to WHO</a>, drinking water the United States can have a sodium concentration anywhere between 1 and 402 mg/L, but only 630 water-systems were sampled to generate this range. A similar study (mentioned in the same WHO paper) found that U.S. sodium concentrations could be up to 1900 mg/L. The amount of sodium one should consume depends on the individual and is hotly debated; an “ideal concentration” is not known for the general population.
</p></>

const StateInstanceCard = (props) => {
  return (
    <Card style={styles.media}>
      <CardMedia style={styles.image} image={props.image} title={props.image}/>
      <CardContent>
        <Typography gutterBottom="gutterBottom" variant='h6' component="h4" style={styles.textSpacing}>
          <strong>{props.instance}</strong>
        </Typography>
        <Typography component="p" variant="caption" align="center">
          <LightTooltip title={pop_text} enterDelay={200} leaveDelay={100} interactive>
            <strong>Population: </strong>
          </LightTooltip>
          {props.population} people
        </Typography>
        <Typography component="p" variant="caption" align="center">
          <LightTooltip title={turb_text} enterDelay={200} leaveDelay={100} interactive>
            <strong>Turbidity: </strong>
          </LightTooltip>
          {props.turbidity} NTU
        </Typography>
        <Typography component="p" variant="caption" align="center">
          <LightTooltip title={nit_text} enterDelay={200} leaveDelay={100} interactive>
            <strong>Nitrate: </strong>
          </LightTooltip>
          {props.nitrate} mg/L
        </Typography>
        <Typography component="p" variant="caption" align="center">
          <LightTooltip title={sod_text} enterDelay={200} leaveDelay={100} interactive>
            <strong>Sodium: </strong>
          </LightTooltip>
          {props.sodium} mg/L
        </Typography>
        <div align="center">
	      <PhScale pH={parseFloat(props.ph)} wid={styles.media.width}></PhScale>
        </div>
      </CardContent>
    </Card>
  )
}

Card.propTypes = {
  classes: PropTypes.object.isRequired
};

export default StateInstanceCard;
