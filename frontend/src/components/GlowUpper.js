
import React, { Component } from 'react';

class Glow extends Component {

  constructor(props) {
    super(props);
    // dummy styles
    // highlight styles
    // text of block

    console.log("text: " + this.props.text);
    console.log("dummy: " + this.props.dummy);
    console.log("highlight: " + this.props.highlight);

    var textst = this.props.text;

    this.state = {
      mylist: []
    };

    // this.state = this.getInitialState();
    while(textst != null) {
      textst = this.parseAsGlow(textst);
    }
  }

  // getInitialState = () => {
  //     return [];
  // };

  addChild = (styling, textstr) => {
    this.setState(this.state.mylist.concat([
      {istyle:styling, itext:textstr}
    ]));

    var oldlist = this.state.mylist;
    oldlist.push({istyle:styling, itext:textstr});

    this.setState({
      mylist: oldlist
    });
    console.log("set new child to " + textstr);
  };

  parseAsGlow = (textst) => {
    if(textst != null && textst != "") {
      var begin = textst.indexOf("**");
      if(begin == -1) {
        this.addChild(this.props.dummy, textst);
        return null;
      }
      var end = textst.indexOf("**", begin+2);
      // return <>{str.substring(0, begin)}<span style={styles.highlight}>{str.substring(begin+2, end)}</span>{str.substring(end+2)}</>;
      this.addChild(this.props.dummy, textst.substring(0, begin));
      this.addChild(this.props.highlight, textst.substring(begin + 2, end));
      console.log(this.state.mylist);
      if(end + 2 >= textst.length()) {
        return null;
      }
      return textst.substring(end + 2);
    }
    console.log(this.state.mylist);
    return null;
  };

  render() {
    const ml = this.state.mylist;
    console.log("LIST IS " + ml + " WITH LENGTH " + ml.length);
    return (
      <>
        {
          ml.map((item) => (
            <span styles={item.istyle}>{item.itext}</span>
          ))
        }
      </>
    );
  }
}

export default Glow;
