import React from 'react';
import PropTypes from 'prop-types';

// Material UI
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardMedia from '@material-ui/core/CardMedia';
import Typography from '@material-ui/core/Typography';
import Link from '@material-ui/core/Link';

const styles = {
  card: {
    maxWidth: '100%'
  },
  media: {
    height: 600,
    width: 300,
  },
  image: {
    height: 350,
    align:'center'
  },
  link: {
    textDecoration: 'none'
  },
  textSpacing: {
    marginBottom: '5px',
    align: 'center'
  }
};

const AboutCard = (props) => {
  return (<Link href={props.link} target="_blank" style={styles.link}>
    <CardActionArea>
    <Card style={styles.media}>
      <CardMedia style={styles.image} image={props.photo} title=""/>
      <CardContent>
        <Typography gutterBottom="gutterBottom" variant='h6' component="h4" style={styles.textSpacing}>
          <strong>{props.name}</strong>
        </Typography>
        <Typography component="p" align="left">
          <strong>Water We Doing:</strong>
          {props.bio}
        </Typography>
        <Typography component="p" align="left" style={styles.textSpacing}>
          <strong>Team Role:</strong>
          {props.role}
        </Typography>
        <Typography component="p" variant="caption" align="left">
          <strong>Commits:
          </strong>
          {props.commits}
        </Typography>
        <Typography component="p" variant="caption" align="left">
          <strong>Issues:</strong>
          {props.issues}
        </Typography>
        <Typography component="p" variant="caption" align="left">
          <strong>Unit Tests:</strong>
          {props.unitTests}
        </Typography>
      </CardContent>
    </Card>
    </CardActionArea>
    </Link>
  )
}

Card.propTypes = {
  classes: PropTypes.object.isRequired
};


export default AboutCard;