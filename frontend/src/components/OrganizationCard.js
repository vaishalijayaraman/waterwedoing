import React from 'react';
import PropTypes from 'prop-types';

// Material UI
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardMedia from '@material-ui/core/CardMedia';
import Typography from '@material-ui/core/Typography';
import Link from '@material-ui/core/Link';
import Highlighter from "react-highlight-words";

const styles = {
  card: {
    maxWidth: '100%'
  },
  media: {
    height: 575,
    width: 300,
  },
  image: {
    height: 300,
    align:'center'
  },
  link: {
    textDecoration: 'none'
  },
  textSpacing: {
    marginBottom: '10px',
    align: 'center'
  },
  highlight: {
    color: 'red'
  },
  dummy: {}
};

const OrganizationCard = (props) => {
  return (
    <Link href={props.link} style={styles.link}>
    <CardActionArea>
    <Card style={styles.media}>
      <CardMedia style={styles.image} image={props.image} title=""/>
      <CardContent>
        <Typography gutterBottom="gutterBottom" variant='h6' component="h4" style={styles.textSpacing}>
          <strong><Highlighter
              highlightClassName="YourHighlightClass"
              searchWords={props.search}
              autoEscape={true}
              textToHighlight={props.instance}
            />
          </strong>
        </Typography>
        <Typography component="p" variant="caption" align="left">
          <strong>Type: </strong><Highlighter
              highlightClassName="YourHighlightClass"
              searchWords={props.search}
              autoEscape={true}
              textToHighlight={props.type}
            />
        </Typography>
        <Typography component="p" variant="caption" align="left">
          <strong>City: </strong><Highlighter
              highlightClassName="YourHighlightClass"
              searchWords={props.search}
              autoEscape={true}
              textToHighlight={props.city}
            />
        </Typography>
        <Typography component="p" variant="caption" align="left">
          <strong>Email: </strong><Highlighter
              highlightClassName="YourHighlightClass"
              searchWords={props.search}
              autoEscape={true}
              textToHighlight={props.email}
            />
        </Typography>
        <Typography component="p" variant="caption" align="left">
          <strong>Phone #: </strong><Highlighter
              highlightClassName="YourHighlightClass"
              searchWords={props.search}
              autoEscape={true}
              textToHighlight={props.phone}
            />
        </Typography>
        <Typography component="p" variant="caption" align="left">
          <strong>Address: </strong><Highlighter
              highlightClassName="YourHighlightClass"
              searchWords={props.search}
              autoEscape={true}
              textToHighlight={props.address}
            />
        </Typography>
        <Typography component="p" variant="caption" align="left">
          <strong>ID: </strong>
          <Highlighter
              highlightClassName="YourHighlightClass"
              searchWords={props.search}
              autoEscape={true}
              textToHighlight={props.id}
            />
        </Typography>
      </CardContent>
    </Card>
    </CardActionArea>
    </Link>
  )
}

Card.propTypes = {
  classes: PropTypes.object.isRequired
};


export default OrganizationCard;
