import React from 'react';
import PropTypes from 'prop-types';

// Material UI
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardMedia from '@material-ui/core/CardMedia';
import Typography from '@material-ui/core/Typography';
import Link from '@material-ui/core/Link';

const styles = {
  card: {
    maxWidth: '100%'
  },
  media: {
    height: 400,
    width: 300,
  },
  link: {
    textDecoration: 'none'
  },
  image: {
    height: 275,
    align:'center'
  },
  textSpacing: {
    marginBottom: '10px',
    align: 'center'
  }
};

const SplashCard = (props) => {
  return (
    <Link href={props.link} style={styles.link}>
    <CardActionArea>
    <Card style={styles.media}>
      <CardMedia style={styles.image} image={props.photo} title=""/>
      <CardContent>
        <Typography gutterBottom="gutterBottom" variant='h6' component="h4" style={styles.textSpacing}>
          <strong>{props.instance}</strong>
        </Typography>
        <Typography component="p" variant="caption" align="left">
          {props.description}
        </Typography>
      </CardContent>
    </Card>
    </CardActionArea>
    </Link>
  )
}

Card.propTypes = {
  classes: PropTypes.object.isRequired
};


export default SplashCard;