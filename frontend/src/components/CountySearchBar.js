import React, {Component} from 'react'
import PropTypes from 'prop-types';

// Material - UI
import Typography from '@material-ui/core/Typography';
import InputBase from '@material-ui/core/InputBase';
import IconButton from '@material-ui/core/IconButton';
import SearchIcon from '@material-ui/icons/Search';

const styles = {
  root: {
    padding: '2px 12px',
    display: 'flex',
    alignItems: 'center',
    width: 500,
    height: 50,
    backgroundColor: '#eee',
    borderRadius: 50
  },
  input: {
    marginLeft: 8,
    flex: 1,
    color: '#000'
  },
  iconButton: {
    padding: 10,
    color: '#0c6fc1'
  },
  divider: {
    width: 1,
    height: 28,
    margin: 4
  }
};

class CountySearchBar extends Component {
  constructor(props) {
    super(props);

    this.state = {
      query: '' //search query
    };
  }

  handleInputChange = event => {
    this.setState({query: event.target.value}) 
  }

  handleKeyPress = (event) => {
    if (event.key === 'Enter') {
      this.handleSearchClick();
      this.setState({query: event.target.value}) 
    }
    this.setState({query: event.target.value}) 
  }
  
  handleSearchClick = () => {
    this.props.returnSearch(this.state.query);
  }

  render() {
    return (<div style={styles.root} elevation={1}>
      <Typography variant='h6' align='left' style={{
          'paddingLeft' : '10px',
          'paddingBottom' : '0px',
          'paddingRight' : '12px',
          'color' : '#0c6fc1'
        }}><strong>Search</strong></Typography>

      <InputBase id="CountySearchInput" style={styles.input} placeholder="Counties" value={this.state.query} onChange={this.handleInputChange} onKeyPress={this.handleKeyPress}/>
      <IconButton id="CountySearchButton" style={styles.iconButton} aria-label="Search" onClick={this.handleSearchClick}>
        <SearchIcon/>
      </IconButton>
    </div>)
  }
}

CountySearchBar.propTypes = {
  classes: PropTypes.object.isRequired
};

export default CountySearchBar;
