import React from 'react';
import PropTypes from 'prop-types';

// Material - UI
import TextField from '@material-ui/core/TextField';
import {withStyles} from '@material-ui/core/styles';

const styles = theme => ({
  container: {
    display: 'flex',
    flexWrap: 'wrap'
  },
  textField: {
    marginRight: theme.spacing.unit,
    width: 100
  },
  dense: {
    marginTop: 16
  },
  menu: {
    width: 250
  },
});

class RangeFilter extends React.Component {
  state = {
    min: '',
    max: '',
    returnFilter: this.props.returnFilter
  };

  handleChange = name => event => {
    var newValue = event.target.value;
    if (newValue < 0) {
      newValue = 0;
    }
    this.setState({[name]: newValue});
    let min_max = {
      min: this.state.min,
      max: this.state.max
    };
    min_max[name] = newValue;
    // If both a min and max are set and min > max, then don't do anything
    if (min_max.min !== '' && min_max.max !== '' && parseInt(min_max.min) > parseInt(min_max.max)) {
      return;
    }
    // Pass the values to the callback
    this.state.returnFilter(min_max.min, min_max.max);

    console.log("inside Min Max")
    console.log(this.state.min)
    console.log(this.state.max)
  };

  render() {
    const {classes} = this.props;

    return (
      <div>
      <form className={classes.container} noValidate="noValidate" autoComplete="off">
      <TextField id="outlined-number" label="min" color="#000000"className={classes.textField} value={this.state.min} onChange={this.handleChange('min')} type="number" InputLabelProps={{
          shrink: true
        }} margin="normal" variant="outlined"/>
      <TextField id="outlined-number" label="max" className={classes.textField} value={this.state.max} onChange={this.handleChange('max')} type="number" InputLabelProps={{
          shrink: true
        }} margin="normal" variant="outlined"/>
    </form>
    </div>);
  }
}

RangeFilter.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withStyles(styles)(RangeFilter);
