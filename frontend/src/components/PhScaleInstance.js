import React from 'react';
import PropTypes from 'prop-types';
import { withStyles, makeStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Slider from '@material-ui/lab/Slider';
import Typography from '@material-ui/core/Typography';
import Tooltip from '@material-ui/core/Tooltip';
import Highlighter from "react-highlight-words";

const useStyles = makeStyles(theme => ({
	  root: {
		      width: 300 - 15 * 2,
		      padding: 10,
		    },
	  margin: {
		      height: theme.spacing(3),
		    },
}));

function ValueLabelComponent(props) {
	  const { children, open, value } = props;

	  const popperRef = React.useRef(null);
	  React.useEffect(() => {
		      if (popperRef.current) {
			            popperRef.current.update();
			          }
		    });

	  return (
		      <Tooltip
		        PopperProps={{
				        popperRef,
					      }}
		        open={open}
		        enterTouchDelay={0}
		        placement="top"
		        title={value}
		      >
		        {children}
		      </Tooltip>
		    );
}

ValueLabelComponent.propTypes = {
	  children: PropTypes.element.isRequired,
	  open: PropTypes.bool.isRequired,
	  value: PropTypes.number.isRequired,
};

const marks = [
	  { value: 0,  label:'0'  },
	  { value: 2,  label:'2'  },
	  { value: 4,  label:'4'  },
	  { value: 6,  label:'6'  },
	  { value: 7,  label:'7'  },
	  { value: 8,  label:'8'  },
	  { value:10,  label:'10' },
	  { value:12,  label:'12' },
	  { value:14,  label:'14' }
];

const IOSSlider = withStyles({
	  root: {
		      color: '#fff',
		      height: 2,
		      padding: '15px 0',
		    },
	  thumb: {
		      opacity: 0
		    },
	  active: {},
	  valueLabel: {
		      opacity: 0
		    },
	  track: {
		      top: 6,
		      height: 19,
		      backgroundColor: '#fff0',
		      border: '0px solid black',
		      borderWidth: '0px 2px 0px 0px',
		    },
	  rail: {
		      left: 0,
		      top: 9,
		      height: 13,
		      opacity: 1,
		      backgroundColor: '#fff0',
		      background: 'linear-gradient(to right, #ff0000 0%, #ffff00 40%, #00ff00 50%, #ffff00 60%, #ff0000 100%)'
		    },
	  mark: {
		      opacity: 0
		    },
	  markActive: {
		      opacity: 0
		    },
})(Slider);

const ph_text = <><p className="Model-body">
  <Typography> <strong> Average pH Level: </strong> </Typography> The county’s <a href="https://en.wikipedia.org/wiki/PH">pH level</a>. pH is a measurement of how alkaline or acidic a liquid is, where 0 is extremely acidic and 14 is extremely alkaline. Room-temperature pure water has a pH of 7, but most freshwater systems have a pH somewhere between 6 and 8 (according to <a href="https://www.fondriest.com/environmental-measurements/parameters/water-quality/ph/">Fondriest Environmental</a>).
</p></>;

const LightTooltip = withStyles(theme => ({
  tooltip: {
    backgroundColor: theme.palette.common.white,
    color: 'rgba(0, 0, 0, 0.87)',
    boxShadow: theme.shadows[1],
    fontSize: 14,
    maxWidth: '80%',
    paddingTop: 15,
  },
}))(Tooltip);

const PhScale = (props) => {
	  const classes = useStyles();
	  if (props.search != null) {
	  return (
		      <Paper className={classes.root}>
		      	<LightTooltip title={ph_text} enterDelay={200} leaveDelay={100} interactive>
		        	<Typography gutterBottom><strong>pH: </strong><Highlighter
              highlightClassName="YourHighlightClass"
              searchWords={props.search}
              autoEscape={true}
              textToHighlight={props.pH.toString()}
            /> </Typography>
		        </LightTooltip>
		        <IOSSlider disabled min={0} max={14} aria-label="iOS slider" defaultValue={props.pH} marks={marks} valueLabelDisplay="on" />
		      </Paper>
			);
	  } else {
		return (
			<Paper className={classes.root}>
				<LightTooltip title={ph_text} enterDelay={200} leaveDelay={100} interactive>
				  <Typography gutterBottom><strong>pH: </strong>{props.pH.toString()}</Typography>
			  </LightTooltip>
			  <IOSSlider disabled min={0} max={14} aria-label="iOS slider" defaultValue={props.pH} marks={marks} valueLabelDisplay="on" />
			</Paper>
		  );
	  }
}

Slider.propTypes = {
	  classes: PropTypes.object.isRequired
};

export default PhScale;
