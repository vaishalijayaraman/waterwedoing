import React from 'react';
import PropTypes from 'prop-types';

// Material UI
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Typography from '@material-ui/core/Typography';
import Tooltip from '@material-ui/core/Tooltip';
import { withStyles } from '@material-ui/core/styles';


const styles = {
  card: {
    maxWidth: '100%'
  },
  media: {
    height: 558,
  },
  image: {
    height: 300,
    align:'center'
  },
  link: {
    textDecoration: 'none'
  },
  textSpacing: {
    marginBottom: '10px',
    align: 'center'
  }
};

const LightTooltip = withStyles(theme => ({
  tooltip: {
    backgroundColor: theme.palette.common.white,
    color: 'rgba(0, 0, 0, 0.87)',
    boxShadow: theme.shadows[1],
    fontSize: 14,
    maxWidth: '80%',
    paddingTop: 15,
  },
}))(Tooltip);

const type_text = <><p className="Model-body">
  <Typography> <strong> Type: </strong> </Typography> The organization’s level of government involvement. Organizations may be federal, state, local, private, volunteer, etc.
</p></>
const city_text = <><p className="Model-body">
  <Typography> <strong> City: </strong> </Typography>  The city which the organization is based out of.
</p></>
const email_text = <><p className="Model-body">
  <Typography> <strong> Email: </strong> </Typography>  The organization’s official contact email.
</p></>
const phone_text = <><p className="Model-body">
  <Typography> <strong> Phone Number: </strong> </Typography>  The organization’s official contact phone number.
</p></>
const address_text = <><p className="Model-body">
  <Typography> <strong> Address: </strong> </Typography>  The street address of the organization. It can be a street number and street name, P.O. box, or anything else the organization chooses to list as its address.
</p></>
const id_text = <><p className="Model-body">
  <Typography> <strong> Identifier: </strong> </Typography>  The organization’s Water Quality Exchange Organization ID <a href="https://www3.epa.gov/storet/wqx/wqxweb.html">as listed under the United States Environmental Protection Agency</a>. These organizations can often have similar names, so an organization’s official identifier is typically more reliable than its formal name. For example, the California State Water Resource Control Board (identifier CABEACH_WQX) is different from the California State Water Resources Control Board (identifier CEDEN).
</p></>

const OrganizationInstanceCard = (props) => {
  return (
    <Card style={styles.media}>
      <CardMedia style={styles.image} image={props.image} title=""/>
      <CardContent>
        <Typography gutterBottom="gutterBottom" variant='h6' component="h4" style={styles.textSpacing}>
          <strong>{props.instance}</strong>
        </Typography>
        <Typography component="p" variant="caption" align="center">
          <LightTooltip title={type_text} enterDelay={200} leaveDelay={100} interactive>
            <strong>Type: </strong>
          </LightTooltip>
          {props.type}
        </Typography>
        <Typography component="p" variant="caption" align="center">
          <LightTooltip title={city_text} enterDelay={200} leaveDelay={100} interactive>
            <strong>City: </strong>
          </LightTooltip>
          {props.city}
        </Typography>
        <Typography component="p" variant="caption" align="center">
          <LightTooltip title={email_text} enterDelay={200} leaveDelay={100} interactive>
            <strong>Email: </strong>
          </LightTooltip>
          {props.email}
        </Typography>
        <Typography component="p" variant="caption" align="center">
          <LightTooltip title={phone_text} enterDelay={200} leaveDelay={100} interactive>
            <strong>Phone #: </strong>
          </LightTooltip>
          {props.phone}
        </Typography>
        <Typography component="p" variant="caption" align="center">
          <LightTooltip title={address_text} enterDelay={200} leaveDelay={100} interactive>
            <strong>Address: </strong>
          </LightTooltip>
          {props.address}
        </Typography>
        <Typography component="p" variant="caption" align="center">
          <LightTooltip title={id_text} enterDelay={200} leaveDelay={100} interactive>
            <strong>ID: </strong>
          </LightTooltip>
          {props.id}
        </Typography>
      </CardContent>
    </Card>
  )
}

Card.propTypes = {
  classes: PropTypes.object.isRequired
};


export default OrganizationInstanceCard;
