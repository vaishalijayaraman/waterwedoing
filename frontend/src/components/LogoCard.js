import React from 'react';
import PropTypes from 'prop-types';

// Material UI
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardMedia from '@material-ui/core/CardMedia';
import Link from '@material-ui/core/Link';

const styles = {
  card: {
    maxWidth: '100%'
  },
  media: {
    height: 200,
    width: 200
  },
  link: {
    textDecoration: 'none'
  },
  textSpacing: {
    marginBottom: '10px',
    weight: 'bold'
  },
  cardMedia: {
    align: 'center'
  },
  image: {
    marginTop: '15px',
    marginBottom: '5px',
    height: '75px',
    width: 'auto',
  }
};

const LogoCard = (props) => {
  return (
    <Link href={props.link} target="_blank" style={styles.link}>
    <CardActionArea>
      <Card style={styles.media}>
        <CardMedia src='img' align='center' style={styles.CardMedia}>
          <img src={props.photo} alt={props.alt_text} style={styles.image}/>
        </CardMedia>
        <CardContent>
          <Typography gutterBottom="gutterBottom" variant='h6' component="h3" style={styles.textSpacing} align='center'>
            {props.name}
          </Typography>
          <Typography component="p" variant='caption' align="center" style={styles.textSpacing}>
            {props.description}
          </Typography>
        </CardContent>
      </Card>
    </CardActionArea>
  </Link>)
}

Card.propTypes = {
  classes: PropTypes.object.isRequired
};

export default LogoCard;
