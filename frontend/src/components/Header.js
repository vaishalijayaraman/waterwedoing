import React from 'react';

// Reactstrap
import {Navbar, Nav, NavItem, NavLink} from 'reactstrap';

import logo from '../files/logo.png'; // Logo

class Header extends React.Component {
	
  render() {
    return (
      <Navbar className="Header" style={{backgroundColor: ' #0c6fc1'}}>
	    <Nav style={{marginBottom: 15}} justify="center">
        <NavItem>
          <NavLink className="splash-button" href ="/"><img src={logo} height="60px" style={{display: 'block', float: 'left', marginRight: 10}}/></NavLink>
        </NavItem>
      </Nav>
      <Nav>
        <NavItem><NavLink className="state-button" href ="/states"><span class="navitem">States</span></NavLink></NavItem>
        <NavItem><NavLink className="county-button" href ="/counties"><span class="navitem">Counties</span></NavLink></NavItem>
        <NavItem><NavLink className="org-button" href ="/organizations"><span class="navitem">Organizations</span></NavLink></NavItem>
        <NavItem><NavLink className="viz-button" href ="/visualizations"><span class="navitem">Visualizations</span></NavLink></NavItem>
        <NavItem><NavLink className="search-button" href ="/search"><span class="navitem">Search</span></NavLink></NavItem>
        <NavItem><NavLink className="about-button" href ="/about"><span class="navitem">About</span></NavLink></NavItem>
      </Nav>
      </Navbar>
    );
  }
}

export default Header;
