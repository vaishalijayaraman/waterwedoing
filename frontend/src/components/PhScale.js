import React from 'react';
import PropTypes from 'prop-types';
import { withStyles, makeStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Slider from '@material-ui/lab/Slider';
import Typography from '@material-ui/core/Typography';
import Tooltip from '@material-ui/core/Tooltip';
import Highlighter from "react-highlight-words";

const useStyles = makeStyles(theme => ({
	  root: {
		      width: 300 - 15 * 2,
		      padding: 10,
		    },
	  margin: {
		      height: theme.spacing(3),
		    },
}));

function ValueLabelComponent(props) {
	  const { children, open, value } = props;

	  const popperRef = React.useRef(null);
	  React.useEffect(() => {
		      if (popperRef.current) {
			            popperRef.current.update();
			          }
		    });

	  return (
		      <Tooltip
		        PopperProps={{
				        popperRef,
					      }}
		        open={open}
		        enterTouchDelay={0}
		        placement="top"
		        title={value}
		      >
		        {children}
		      </Tooltip>
		    );
}

ValueLabelComponent.propTypes = {
	  children: PropTypes.element.isRequired,
	  open: PropTypes.bool.isRequired,
	  value: PropTypes.number.isRequired,
};

const marks = [
	  { value: 0,  label:'0'  },
	  { value: 2,  label:'2'  },
	  { value: 4,  label:'4'  },
	  { value: 6,  label:'6'  },
	  { value: 7,  label:'7'  },
	  { value: 8,  label:'8'  },
	  { value:10,  label:'10' },
	  { value:12,  label:'12' },
	  { value:14,  label:'14' }
];

const IOSSlider = withStyles({
	  root: {
		      color: '#fff',
		      height: 2,
		      padding: '15px 0',
		    },
	  thumb: {
		      opacity: 0
		    },
	  active: {},
	  valueLabel: {
		      opacity: 0
		    },
	  track: {
		      top: 6,
		      height: 19,
		      backgroundColor: '#fff0',
		      border: '0px solid black',
		      borderWidth: '0px 2px 0px 0px',
		    },
	  rail: {
		      left: 0,
		      top: 9,
		      height: 13,
		      opacity: 1,
		      backgroundColor: '#fff0',
		      background: 'linear-gradient(to right, #ff4444 0%, #ffff44 40%, #44ff44 50%, #ffff44 60%, #ff4444 100%)'
		    },
	  mark: {
		      opacity: 0
		    },
	  markActive: {
		      opacity: 0
		    },
})(Slider);

const PhScale = (props) => {
	  const classes = useStyles();
	  if (props.search != null) {
		return (
				<Paper className={classes.root}>
					<Typography gutterBottom><strong>pH: </strong><Highlighter
				highlightClassName="YourHighlightClass"
				searchWords={props.search}
				autoEscape={true}
				textToHighlight={props.pH.toString()}
				/> </Typography>
					<IOSSlider disabled min={0} max={14} aria-label="iOS slider" defaultValue={props.pH} marks={marks} valueLabelDisplay="on" />
				</Paper>
				);
	  } else {
		return (
			<Paper className={classes.root}>
				<Typography gutterBottom><strong>pH: </strong>{props.pH.toString()}</Typography>
				<IOSSlider disabled min={0} max={14} aria-label="iOS slider" defaultValue={props.pH} marks={marks} valueLabelDisplay="on" />
			</Paper>
			);
	  }
}

Slider.propTypes = {
	  classes: PropTypes.object.isRequired
};

export default PhScale;
