import React from 'react';
import PropTypes from 'prop-types';

// Material UI
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardMedia from '@material-ui/core/CardMedia';
import Typography from '@material-ui/core/Typography';
import Link from '@material-ui/core/Link';
import Highlighter from "react-highlight-words";

import PhScale from './PhScale.js';

const styles = {
  card: {
    maxWidth: '100%'
  },
  media: {
    height: 558,
    width: 300,
  },
  link: {
    textDecoration: 'none'
  },
  image: {
    height: 300,
    align:'center'
  },
  textSpacing: {
    marginBottom: '10px',
    align: 'center'
  }
};

const StateCard = (props) => {
  return (
      <Link href={props.link} style={styles.link}>
    <CardActionArea>
    <Card style={styles.media}>
      <CardMedia style={styles.image} image={props.image} title={props.image}/>
      <CardContent>
        <Typography gutterBottom="gutterBottom" variant='h6' component="h4" style={styles.textSpacing}>
          <strong><Highlighter
              highlightClassName="YourHighlightClass"
              searchWords={props.search}
              autoEscape={true}
              textToHighlight={props.instance}
            />
          </strong>
        </Typography>
        <Typography component="p" variant="caption" align="left">
          <strong>Population: </strong>
            <Highlighter
              highlightClassName="YourHighlightClass"
              searchWords={props.search}
              autoEscape={true}
              textToHighlight={props.population} 
            /> people
        </Typography>
        <Typography component="p" variant="caption" align="left">
          <strong>Turbidity: </strong>
          <Highlighter
              highlightClassName="YourHighlightClass"
              searchWords={props.search}
              autoEscape={true}
              textToHighlight={props.turbidity} 
            /> NTU
        </Typography>
        <Typography component="p" variant="caption" align="left">
          <strong>Nitrate: </strong>
          <Highlighter
              highlightClassName="YourHighlightClass"
              searchWords={props.search}
              autoEscape={true}
              textToHighlight={props.nitrate}
            /> mg/L
        </Typography>
        <Typography component="p" variant="caption" align="left">
          <strong>Sodium: </strong>
          <Highlighter
              highlightClassName="YourHighlightClass"
              searchWords={props.search}
              autoEscape={true}
              textToHighlight={props.sodium}
            /> mg/L
        </Typography>
        {console.log(typeof props.ph)}
	<PhScale pH={parseFloat(props.ph)} wid={styles.media.width} search={props.search}></PhScale>
      </CardContent>
    </Card>
    </CardActionArea>
    </Link>
  )
}

Card.propTypes = {
  classes: PropTypes.object.isRequired
};

export default StateCard;
