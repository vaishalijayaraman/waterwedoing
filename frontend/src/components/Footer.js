import React from 'react';

import Typography from '@material-ui/core/Typography';

const Footer = () => {
  return (
    <div className='Footer'>
      <Typography variant='subtitle2' style={{color: 'white', paddingBottom: '10px'}}>Copyright © 2019 WaterWeDoing.me</Typography>
      <Typography variant='subtitle2' style={{color: 'white'}}>Made by UT Austin students</Typography>
    </div>
  )
}

export default Footer;
