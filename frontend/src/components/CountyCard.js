import React from 'react';
import PropTypes from 'prop-types';

// Material UI
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardMedia from '@material-ui/core/CardMedia';
import Typography from '@material-ui/core/Typography';
import Link from '@material-ui/core/Link';
import Highlighter from "react-highlight-words";

import PhScale from './PhScale.js';

const styles = {
  card: {
    maxWidth: '100%'
  },
  media: {
    height: 600,
    width: 300,
  },
  image: {
    height: 300,
    align:'center'
  },
  link: {
    textDecoration: 'none'
  },
  textSpacing: {
    marginBottom: '10px',
    align: 'center'
  },
  highlight: {
    color: 'red'
  },
  dummy: {}

};

const CountyCard = (props) => {
  return (
    <Link href={props.link} style={styles.link}>
    <CardActionArea>
    <Card style={styles.media}>
      <CardMedia style={styles.image} image={props.image} title=""/>
      <CardContent>
        <Typography gutterBottom="gutterBottom" variant='h6' component="h4" style={styles.textSpacing}>
          <strong><Highlighter
              highlightClassName="YourHighlightClass"
              searchWords={props.search}
              autoEscape={true}
              textToHighlight={props.instance}
            /></strong>
        </Typography>
        <Typography component="p" variant="caption" align="left">
          <strong>Turbidity: </strong><Highlighter
              highlightClassName="YourHighlightClass"
              searchWords={props.search}
              autoEscape={true}
              textToHighlight={props.turbidity}
            /> NTU
        </Typography>
        <Typography component="p" variant="caption" align="left">
          <strong>Nitrate: </strong><Highlighter
              highlightClassName="YourHighlightClass"
              searchWords={props.search}
              autoEscape={true}
              textToHighlight={props.nitrate}
            /> mg/L
        </Typography>
        <Typography component="p" variant="caption" align="left">
          <strong>Sodium: </strong> <Highlighter
              highlightClassName="YourHighlightClass"
              searchWords={props.search}
              autoEscape={true}
              textToHighlight={props.sodium}
            /> mg/L
        </Typography>
        <Typography component="p" variant="caption" align="left">
          <strong>Date of Oldest Sample: </strong><Highlighter
              highlightClassName="YourHighlightClass"
              searchWords={props.search}
              autoEscape={true}
              textToHighlight={props.olddate.toString()}
            />
        </Typography>
        <Typography component="p" variant="caption" align="left">
          <strong>Date of Newest Sample: </strong><Highlighter
              highlightClassName="YourHighlightClass"
              searchWords={props.search}
              autoEscape={true}
              textToHighlight={props.newdate.toString()}
            />
            {console.log("date + " + props.newdate.toString())}
        </Typography>
	<PhScale pH={parseFloat(props.ph)} wid={styles.media.width} search={props.search}></PhScale>
      </CardContent>
    </Card>
    </CardActionArea>
    </Link>
  )
}

Card.propTypes = {
  classes: PropTypes.object.isRequired
};


export default CountyCard;
