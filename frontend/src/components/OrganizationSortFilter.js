import React, {Component} from 'react'
import PropTypes from "prop-types";

// Components
import Sort from './Sort.js';
import CheckBox from './Checkbox.js';
import DropdownFilter from './DropdownFilter.js';

// Material - UI
import Typography from '@material-ui/core/Typography';

const choices = [
  {
    value: '_score',
    displayName: 'Relevance',
    defOrder: 'asc'
  }, {
    value: 'org_name.raw',
    displayName: 'Name',
    defOrder: 'asc'
  }, {
    value: 'email.raw',
    displayName: 'Email',
    defOrder: 'desc'
  }, {
    value: 'phone.raw',
    displayName: 'Phone',
    defOrder: 'desc'
  }, {
    value: 'city.raw',
    displayName: 'City',
    defOrder: 'asc'
  }, {
    value: 'org_identifier.raw',
    displayName: 'ID',
    defOrder: 'desc'
  },{
    value: 'org_type.raw',
    displayName: 'Type',
    defOrder: 'desc'
  }
];

const checklist_email = [
  { label: 'Email', value: 'email'},
];

const checklist_add = [
  { label: 'Address', value: 'address'},
];

const checklist_phone = [
  { label: 'Phone', value: 'phone'},
];

const checklist_city = [
  { label: 'City', value: 'city'},
];

const orgTypes= [
  {
    value: "null",
    name: 'None',
  },{
    value: '?*',
    name: 'All',
  },
  {
    value: 'tribal',
    name: 'Tribal',
  },{ 
    value: 'government',
    name: 'State',
  },{
    value: 'comsn',
    name: 'Interstate',
  },{
    value: 'federal',
    name: 'Federal',
  },{
    value: 'industry',
    name: 'Private, Industry',
  },{
    value: 'industrial',
    name: 'Private, Non-Industry',
  },{
    value: 'college',
    name: 'University',
  },{
    value: 'volunteer',
    name: 'Volunteer',
  }
];

class OrganizationSortFilter extends Component {
  render() {
    return (
      <div>
        <div>
        <div className="filter-sort-container">
          <div className="filter-body">
          <Typography variant='subtitle1' align='left' style={{'fontWeight' : '600','color': '#0c6fc1'}}>Filter By</Typography>
            <div className="filter-item">
              <Typography variant='subtitle2' align='left' style={{'fontWeight' : '600'}}>Type <DropdownFilter updateSelection={this.props.updateOrganizationFilterTYPE.bind(this)} list={orgTypes}/></Typography>
            </div>
            <div className="filter-item">
              <Typography variant='subtitle2' align='left' style={{'fontWeight' : '600'}}>Email <CheckBox returnFilter={this.props.updateOrganizationFilterEMAIL.bind(this)} checklist={checklist_email}/></Typography>
            </div>
            <div className="filter-item">
              <Typography variant='subtitle2' align='left' style={{'fontWeight' : '600'}}>Address <CheckBox returnFilter={this.props.updateOrganizationFilterADD.bind(this)} checklist={checklist_add}/></Typography>
            </div>
            <div className="filter-item">
              <Typography variant='subtitle2' align='left' style={{'fontWeight' : '600'}}>Phone <CheckBox returnFilter={this.props.updateOrganizationFilterPHONE.bind(this)} checklist={checklist_phone}/></Typography>
            </div>
            <div className="filter-item">
              <Typography variant='subtitle2' align='left' style={{'fontWeight' : '600'}}>City <CheckBox returnFilter={this.props.updateOrganizationFilterCITY.bind(this)} checklist={checklist_city}/></Typography>
            </div>
          </div>
          <div className="sort-body">
            <Typography variant='subtitle1' align='left' width='25%' style={{'fontWeight' : '600','color': '#0c6fc1'}}>Sort By</Typography>
            <Sort options={choices} updateSortData={this.props.updateOrganizationSortData.bind(this)}/>
          </div>
      </div>
      </div>
    </div>)
  }
}

OrganizationSortFilter.propTypes = {
  classes: PropTypes.object.isRequired
};

export default OrganizationSortFilter;
