import React, { Component } from 'react';

class InstanceContainer extends Component {

    componentDidMount() {
      this.props.loadData(this.props.keyS);
    }

    render() {
      return (
        <div>
          {this.props.children}
        </div>
      );
    }
  }

  export default InstanceContainer;
