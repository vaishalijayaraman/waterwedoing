import React, { Component } from 'react';
import ReactPaginate from 'react-paginate';

class ModelContainer extends Component {
    componentDidMount() {
      this.props.updatePageCallback(this.props.pageNum);
      // this.props.updatePageCallback(1);
    }

    handlePageClick = data => {
      let selected = data.selected;
      console.log("IN PAGE CLICK");
      console.log(selected);
      this.props.updatePageCallback(selected + 1);
      window.scrollTo(0, 0);
    };

    render() {
      return (
        <div>
          {this.props.children}
            <ReactPaginate
              previousLabel={'previous'}
              nextLabel={'next'}
              breakLabel={'...'}
              breakClassName={'break-me'}
              // forcePage={this.props.pageNum}
              pageCount={this.props.pageCount} 
              marginPagesDisplayed={1}
              pageRangeDisplayed={2}
              onPageChange={this.handlePageClick}
              containerClassName={'pagination'}
              subContainerClassName={'pages pagination'}
              activeClassName={'currPage'}
            />
        </div>
      );
    }
  }

  export default ModelContainer;


