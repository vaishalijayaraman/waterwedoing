import requests
import json


urls = ['http://api.2020guide.me/candidates?page=1', 
	'http://api.2020guide.me/candidates?page=2', 
	'http://api.2020guide.me/candidates?page=3', 
	'http://api.2020guide.me/candidates?page=4']

state_counts = {}
total = 0

for url in urls:
	response = requests.get(url)
	candidates = json.loads(response.text)['objects']
	for cand in candidates:
		state = cand['state']
		if state in state_counts:
			state_counts[state] += 1
		else:
			state_counts[state] = 1
		total += 1


print(state_counts)
# {'DC': 1, 'IN': 1, 'TX': 3, 'NJ': 1, 'AS': 1, 'NY': 7, 'CA': 1, 'PA': 3, 'WA': 1, 'MN': 2, 'OK': 1, 'India': 1, 'MT': 1, 'MA': 2, 'FL': 1, 'OH': 1, 'IA': 1}

print(total)
# 29
