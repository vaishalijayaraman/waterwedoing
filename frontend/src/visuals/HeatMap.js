// modeled after https://codepen.io/trey-davis/pen/pwgBYo?editors=0110

import React, { Component } from 'react';

// D3
import * as d3 from "d3";

const styles = {
	chart: {
	  width: 800
	},
	glow: {
	  fill: "#0c6fc1"
	},
	container: {
		width: 1000,
		textAlign: "center"
	},
	yAxis:{ 
		path: "line",
		stroke: "white"
	},
	leftjust: {
	  display: "block",
	  textAlign: "left",
	},
	rightjust: {
	  display: "block",
	  textAlign: "right",
	},
	box: {
		display: "flex",
		alignItems: "center",
		justifyContent: "center",
		fontFamily: "Avenir",
		fontWeight: "bold",
	},
	box2: {
		display: "flex",
		alignItems: "center",
		justifyContent: "center",
		fontFamily: "Avenir",
		textAlign: "center",
	},
	b1: {
		backgroundColor: "#c82433",
		color: "#ffffff"
	},
	b2: {
		margin: "5",
		backgroundColor: "black",
		color: "#ffffff"
	}
}

class HeatMap extends Component {
	componentDidMount(){
		// console.log("REQUESTING");
		fetch(String.raw`https://api.waterwedoing.me/counties/search?q={%22size%22:500,%20%22_source%22:[%22county_name%22,%20%22turbidity%22,%20%22state%22],%20%22query%22:{%22match_all%22:{}}}`)
		.then((resp) => resp.json())
    	.then(function(resp) {
      		// console.log(resp);
      		var turbDict = {};
      		var stateDict = {};
      		var stateNumberDict = {};
      		var countyNumDict = {};
      		for(var i = 0; i < resp['hits']['hits'].length; i++) {
		        var item = resp['hits']['hits'][i]._source;
		        // console.log("ITEM " + i + ": " + item);
		        turbDict[item['county_name']] = item['turbidity'];
		        // console.log(item['county_name'] + " => " + item['turbidity']);
		        stateDict[item['county_name']] = item['state'];
		        // console.log(item['county_name'] + " => " + item['state']);
		        if (item['state'] in stateNumberDict){
		        	countyNumDict[item['county_name']] = stateNumberDict[item['state']];
		        	stateNumberDict[item['state']] = stateNumberDict[item['state']]+1;
		        }else{
		        	stateNumberDict[item['state']] = 1;
		        	countyNumDict[item['county_name']] = 0;
		        }
		    }
		    // console.log(turbDict);
		    // console.log(stateDict);

		    const width = 650,
            	height = 750,
            	margins = {top:20, right: 50, bottom: 100, left: 100};

            const yScale = d3.scaleLinear()
		        .range([height-10,0])
		        .domain([50, 1])

	       	const chart = d3.select('.chart')
		        .attr('width', width + margins.right + margins.left)
		        .attr('height', height + margins.top + margins.bottom)
		        .append('g')
		        .attr('transform','translate(' + margins.left + ',' + margins.top + ')');
		    
		    const tooltip = d3.select('.container').append('div')
        		.attr('class','tooltip')
        		.html('Tooltip')

        	const barWidth = width / 10,
            	barHeight = height / 50

            var stateNumDict = {"Alabama": 1, "Alaska": 2, "Arizona": 3, "Arkansas": 4, "California": 5, "Colorado": 6, "Connecticut": 7, "Delaware": 8, "Florida": 9, "Georgia": 10, "Hawaii": 11, "Idaho": 12, "Illinois": 13, "Indiana": 14, "Iowa": 15, "Kansas": 16, "Kentucky": 17, "Louisiana": 18, "Maine": 19, "Maryland": 20, "Massachusetts": 21, "Michigan": 22, "Minnesota": 23, "Mississippi": 24, "Missouri": 25, "Montana": 26, "Nebraska": 27, "Nevada": 28, "New Hampshire": 29, "New Jersey": 30, "New Mexico": 31, "New York": 32, "North Carolina": 33, "North Dakota": 34, "Ohio": 35, "Oklahoma": 36, "Oregon": 37, "Pennsylvania": 38, "Rhode Island": 39, "South Carolina": 40, "South Dakota": 41, "Tennessee": 42, "Texas": 43, "Utah": 44, "Vermont": 45, "Virginia": 46, "Washington": 47, "West Virginia": 48, "Wisconsin": 49, "Wyoming": 50};
            var numToState = {1:"Alabama", 2:"Alaska", 3:"Arizona", 4:"Arkansas", 5:"California", 6:"Colorado", 7:"Connecticut", 8:"Delaware", 9:"Florida", 10:"Georgia", 11:"Hawaii", 12:"Idaho", 13:"Illinois", 14:"Indiana", 15:"Iowa", 16:"Kansas", 17:"Kentucky", 18:"Louisiana", 19:"Maine", 20:"Maryland", 21:"Massachusetts", 22:"Michigan", 23:"Minnesota", 24:"Mississippi", 25:"Missouri", 26:"Montana", 27:"Nebraska", 28:"Nevada", 29:"New Hampshire", 30:"New Jersey", 31:"New Mexico", 32:"New York", 33:"North California", 34:"North Dakota", 35:"Ohio", 36:"Oklahoma", 37:"Oregon", 38:"Pennsylvania", 39:"Rhode Island", 40:"South Carolina", 41:"South Dakota", 42:"Tennessee", 43:"Texas", 44:"Utah", 45:"Vermont", 46:"Virginia", 47:"Washington", 48:"West Virginia", 49:"Wisconsin", 50:"Wyoming"};
            var abbrevs = ["AL", "AK", "AZ", "AR", "CA", "CO", "CT", "DE", "FL", "GA", "HI", "ID", "IL", "IN", "IA", "KS", "KY", "LA", "ME", "MD", "MA", "MI", "MN", "MS", "MO", "MT", "NE", "NV", "NH", "NJ", "NM", "NY", "NC", "ND", "OH", "OK", "OR", "PA", "RI", "SC", "SD", "TN", "TX", "UT", "VT", "VA", "WA", "WV", "WI", "WY"];
            var data = [];
            for (var county in turbDict){
            	var tempDict = {};
            	tempDict['state'] = stateNumDict[stateDict[county]];
            	tempDict['county'] = countyNumDict[county];
            	tempDict['county_name'] = county;
            	tempDict['turbidity'] = turbDict[county];
            	data.push(tempDict);
            }

            const colorScale = d => {
            	if (d.turbidity < 0){
            		return 'black';
            	}
            	if(d.turbidity > 90){
            		return '#c82433';
            	}
		        var turbScale = d.turbidity / 100;
		        var bmax = 20, bmin = 80;

				var bval = Math.floor(turbScale * (bmax - bmin) + bmin);
				console.log("BRIGHTNESS VALUE: ");
				console.log(bval);

				return ("hsl(207, 88%, " + String(bval) + "%)"); // /*Math.floor(dict[d3.select(d)._groups[0][0].properties.NAME] / 14.0 * 255)*/);
		    };

		    const parseFormat = d => {
		        return numToState[d];
		    };

		    const printTurb = d => {
		    	if(d.turbidity == -1)
		    		return 'N/A';
		    	else
		    		return d.turbidity + ' NTU';
		    }

		    const getHTML = d => {
		    	if(d.turbidity== -1){
          			return d.county_name + '<br/>Turbidity: N/A';
          		}
          		else{
          			return d.county_name +'<br/>Turbidity: '+d.turbidity+' NTU';
          		}
		    }

            chart.selectAll('g')
		        .data(data).enter().append('g')
		        .append('rect')
		        .attr('x', d => {return (d.county) * barWidth})
		        .attr('y', d => {return (d.state) * barHeight - 23})
		        .style('fill', colorScale)
		        .attr('width', barWidth)
		        .attr('height', barHeight)
		        .attr("class", "axis")
		        .on('mouseover', d => {
		          tooltip.html(getHTML(d))
		            .style('left', d3.event.pageX + 'px')
		            .style('top', d3.event.pageY - 375 + 'px')
		            .style('opacity', .75)
		            .style('font-family', 'Avenir')
		            .style('background-color', "lightgray")
					.style('position', 'absolute')
					.style('opacity', "0px")
					.style('padding', '5px');
		        }).on("click",function(d) {
			        window.location = "https://waterwedoing.me/counties/:" + d.county_name;
				    })
		        .on('mouseout', () => {
		          	tooltip.style('opacity', 0)
		            .style('left', '0px');
		        });
		      
		    chart.append('text')
		        .attr('transform','translate(-40,' + (height / 2)  + ') rotate(-90)')
		        .style('text-anchor','middle')
		        .style('font-family', 'Avenir')
		        .style('font-weight', 'bold')
		        .style('font-size', '20')
		        .text('State');
		      
		    chart.append('text')
		        .attr('transform','translate(' + (width / 2) + ',' + (height + 40) + ')')
		        .style('text-anchor','middle')
		        .style('font-family', 'Avenir')
		        .style('font-weight', 'bold')
		        .style('font-size', '20')
		        .text('County');

		    //Append y axis
		    chart.append('g')
			    .attr('transform','translate(0,-' + barHeight / 5 + ')')
			    .call(d3.axisLeft(yScale)
			    	.tickFormat(function(d) {return abbrevs[d-1];})
			    	.ticks(50))
			    .style('font-family', 'Avenir')
			    .attr('class','yAxis');
      	});
	}
	render() {
    	return(
    		<div>
        	<div className='container'></div>
        	<svg className='chart'></svg>
        	<div style={styles.box}>
			<div style={styles.leftjust}>{'0 NTU'}</div> 
	        <div style={{width: 250, height: 25, backgroundImage: "linear-gradient(to right, " + "#ffffff" + ", " + "#0c6fc1" + ")"}}></div>
	        <div style={styles.rightjust}>{'100 NTU'}</div>
	        </div>
	        <br/>
	        <div style={styles.box2}>
	        <sp style={styles.b1}>&nbsp;&nbsp;100+&nbsp;&nbsp;</sp>
	        &nbsp;&nbsp;
	        &nbsp;&nbsp;
	        <sp style={styles.b2}>&nbsp;&nbsp;N/A&nbsp;&nbsp;</sp>
	        </div>
	        <br/> <br/>
	        </div>
    	);
    }
}

export default HeatMap;