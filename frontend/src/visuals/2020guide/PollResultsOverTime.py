import plotly
#import plotly.express
import pandas as pd
from pandas import DataFrame as df
import requests
import plotly.express as plotly_express
import plotly.graph_objs as plotly_graph_objs
import plotly.offline as plotly_offline

class PollResultsOverTime:
    api_base_URL = "http://api.2020guide.me/"
    api_polls_URL = api_base_URL + "polls?"
    
    results_per_page = 1000
    max_results_per_page = 1000
    min_page = 1

    def __init__(self):

        self.outer_keys_to_keep = ("created_at", "poll_samples")
        self.inner_keys_to_keep = ("answer", "candidate_id", "pct")

        self.query_parameters = {"results_per_page": self.results_per_page,
            "max_results_per_page": self.max_results_per_page} #, "page": starting_page}
        
        self.request = self.build_request_URL()
        self.max_pages = int(requests.get(self.request + "1").json()["total_pages"])
        
    def auto_run(self):
        
        data = self.convert_to_pandas_DataFrame(
            self.average_all_candidates(
                self.consolidate_dates(
                    self.get_query_results(
                        self.min_page, self.outer_keys_to_keep, self.inner_keys_to_keep
                    )
                )
            )
        )
        
        fig = plotly_express.line(data, x = "Date", y = "Vote Percentage", title='Voting Percentage Over Time')
        fig.show()
        return fig


    """
    def generate_graph(data_frame):
        temp_frame = pd.DataFrame({'contributing_factors':contributing_factors.value_counts()})
        df = df.sort_values(by='contributing_factors', ascending=True)
        
        data  = plotly_graph_objs.Data([
                    plotly_graph_objs.Bar(
                    y = df.index,
                    x = df.contributing_factors,
                    orientation='h'
                )])

        layout = plotly_graph_objs.Layout(
                height = '1000',
                margin=plotly_graph_objs.Margin(l=300),
                title = "Contributing Factors for Vehicle Collisions in 2015"
        )
        fig  = plotly_graph_objs.Figure(data=data, layout=layout)
        plotly_offline.iplot(fig)
    """
    
    def build_request_URL(self):
        result = self.api_polls_URL

        for key in self.query_parameters:
            result += "".join((key, "=", str(self.query_parameters[key]), "&"))
        
        result += "page="

        return result

    #Create a dictionary where each key is a polling date and each value is a list of poll samples from that date
    def consolidate_dates(self, query_results):
        polls_by_date = dict(list())

        for poll in query_results:
            #If this date is already present in the dictionary, append list
            if poll["created_at"] in polls_by_date:
                polls_by_date[poll["created_at"]] += poll["poll_samples"]
            else:
                polls_by_date[poll["created_at"]] = poll["poll_samples"]

        return polls_by_date

    """
    polls_by_date: A dictionary where each key is a polling date and each value is a list of poll samples from that date
    returns: A dictionary where each key is a polling date and each value is a dictionary whose keys are candidates and values are the average for that candidate on that day.
    """
    def average_all_candidates(self, polls_by_date):
        result = dict()

        # For each date in these dates
        for poll_date in polls_by_date:
            processed_names = list()
            result[poll_date] = dict()

            # for each sample (dictionary) in these samples
            for poll_sample in polls_by_date[poll_date]:
                if (poll_sample["answer"] not in processed_names):
                    processed_names += poll_sample["answer"]
                    
                    # Get a list of all the samples (dictionaries) for this candidate on this date
                    dicts_to_average = [sample for sample in polls_by_date[poll_date] if sample["answer"] == poll_sample["answer"]]

                    ratings = [float(sample["pct"]) for sample in dicts_to_average]
                    
                    # Get average for this candidate on this date
                    average = float(sum(ratings)/len(ratings))

                    # Add average to this candidate on this date
                    result[poll_date][poll_sample["answer"]] = average
        
        return result

    """
    Creates a pandas DataFrame based on dictionary (meant to be run after average_all_candidates)
    dictionary = {outer_key: {inner_key: inner_value}}
    Combines dictionaries that share the same inner key
    """
    
    def convert_to_pandas_DataFrame(self, dictionary):
        lines = dict()
        indicies = list()

        for date in dictionary:

            indicies.append(date)
            
            for candidate in dictionary[date]:

                value_to_add = dictionary[date][candidate]

                if (candidate in lines):
                    lines[candidate].append(value_to_add)
                
                else:
                    lines[candidate] = [value_to_add]
        """
        print("lines:\n")
        print(lines)
        print("\nindicies:\n")
        print(indicies)
        """
        return pd.DataFrame(lines, indicies)

    """
    Get all results (polls) and put them in a list
    returns: list(dict("created_date": some_date, "poll_samples": list(dict())))
    """
    def __get_raw_results__(self, starting_page):
        
        raw_results = [requests.get(
            "".join(
                (self.request, str(page))
            )
            ).json()
            for page in range(starting_page, self.max_pages + 1)
        ]

        return raw_results

    """
    Get rid of unneccessary key-value pairs in inner and outer dictionaries
    results: A list of dictionaries containing dictionaries: list(dict(dict()))
    """
    def __trim_results__(self, results, outer_keys_to_keep, inner_keys_to_keep):

        trimmed_results = [result["objects"] for result in results]
        #trimmed_results = [*trimmed_results]
        new_trimmed_results = list()

        for trimmed_result in trimmed_results:
            new_trimmed_results += trimmed_result
        trimmed_results = new_trimmed_results
        
        """
        super_new_trimmed_results = list()

        for trimmed_result in trimmed_results:
            super_new_trimmed_results += {"created_at": trimmed_result["created_at"], "poll_samples": trimmed_result["poll_samples"]}
        """
        #trimmed_results = super_new_trimmed_results

        #trimmed_results = [{"created_at": trimmed_result["created_at"], "poll_samples": trimmed_result["poll_samples"]} for trimmed_result in trimmed_results]
        even_more_trimmed_results = list()

        for trimmed_result in trimmed_results:

            date = trimmed_result["created_at"]
            trimmed_result = {"created_at": trimmed_result["created_at"], "poll_samples": trimmed_result["poll_samples"]}

            new_sample_list = list()

            for sample in trimmed_result["poll_samples"]:
                sample = {
                    "answer": sample["answer"], "candidate_id": sample["candidate_id"], "pct": sample["pct"]
                }
                new_sample_list.append(sample)

            trimmed_result["poll_samples"] = new_sample_list

            even_more_trimmed_results.append(trimmed_result)

        trimmed_results = even_more_trimmed_results
        
        #trimmed_results = [{trimmed_result["created_at"]: trimmed_result["poll_samples"]} for trimmed_result in trimmed_results]

        """
        print(trimmed_results)
        exit()
        for result in results:
            try:
                if (result["created_at"] == None):
                    print("Error: No \"created_at\" field in result: " + str(result))
            except Exception as e:
                print(e)
                print(result)
                exit()
        
        # Get rid of unneccessary key-value pairs in outer dictionaries
        trimmed_results = [
            {key: result[key] for key in outer_keys_to_keep}
            for result in results
        ]
        """
        """
        # Get rid of unneccessary key-value pairs in inner dictionaries
        for result in trimmed_results:
            result["poll_samples"] = {
                inner_key: result["poll_samples"][inner_key] 
                for inner_key in inner_keys_to_keep
            }
        """
        return trimmed_results

    # Get all results (polls) in one list, with unneccessary values trimmed.
    def get_query_results(self, starting_page, outer_keys_to_keep, inner_keys_to_keep):
        
        return self.__trim_results__( 
            self.__get_raw_results__(starting_page), 
            outer_keys_to_keep, inner_keys_to_keep)
        

        # CLEAN: Delete this old code if the new one works
        """
        query_results = list()

        while True:
            result = requests.get("".join((self.request, str(starting_page)))).json()
            
            if len(result["objects"]): #If results found, aggregate them. Otherwise, break.
                query_results += result["objects"]
                starting_page += 1 #Could just use a generator if max pages was known
            else:
                break

        return query_results
        """

def main():
    results = PollResultsOverTime()
    
    results.auto_run()
    print("Done!")

if __name__== "__main__":
    main()
