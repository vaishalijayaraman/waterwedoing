import React, { Component } from 'react';

// D3
import * as d3 from "d3";

const getCandidatePolyPointString = (candidateDict, whitelist, cx, cy, scale) => {
	var polypts = "";
	var offset = Math.PI / 2;
	console.log("MADE IT INTO THE FUNCTION");
	console.log(candidateDict);
	var ct = Object.keys(candidateDict).length;
	console.log(ct);
	for(var i = 0; i < whitelist.length; i++) {
		if(i > 0) {
			polypts += " ";
		}
		var angle = Math.PI * 2 * i / whitelist.length;
		var radius = candidateDict[whitelist[i]];
		var x = cx + radius * scale * Math.cos(angle + offset);
		var y = cy + radius * scale * Math.sin(angle + offset);
		polypts += Math.floor(x) + "," + Math.floor(y);
	}

	return polypts;
};

const getFullPolyPointString = (whitelist, cx, cy, scale) => {
	var polypts = "";
	var offset = Math.PI / 2;
	for(var i = 0; i < whitelist.length; i++) {
		if(i > 0) {
			polypts += " ";
		}
		var angle = Math.PI * 2 * i / whitelist.length;
		var x = cx + 1 * scale * Math.cos(angle + offset);
		var y = cy + 1 * scale * Math.sin(angle + offset);
		polypts += Math.floor(x) + "," + Math.floor(y);
	}

	return polypts;
};

const renderAbbrevOnPoly = (g, abbrevs, cx, cy, scale) => {
	var offset = Math.PI / 2;
	for(var i = 0; i < abbrevs.length; i++) {
		var angle = Math.PI * 2 * i / abbrevs.length;
		var x = cx + 1 * scale * Math.cos(angle + offset);
		var y = cy + 1 * scale * Math.sin(angle + offset);

		var text = g.append("text")
		.text(abbrevs[i])
		.attr("fill", "#000000")
		.attr("x", x)
		.attr("y", y)
		.attr("font-size", 10);

		var bbox = text.node().getBBox();

		text.attr("x", x - bbox.width / 2);
		text.attr("y", y - bbox.height - 40);
	}
};

class HexIssueGraph extends Component {

  constructor(props) {
    super(props);
    this.state = {};
  }

  componentDidMount() {

    var w = 800;
    var h = 1300;

    // Calculate bounding box transforms for entire collection
    // var b = path.bounds( json ),
    // s = .95 / Math.max((b[1][0] - b[0][0]) / w, (b[1][1] - b[0][1]) / h),
    // t = [(w - s * (b[1][0] + b[0][0])) / 2, (h - s * (b[1][1] + b[0][1])) / 2];

		// fetch(String.raw`https://api.2020guide.me/candidates?results_per_page=28`)
		// .then((resp) => resp.json())
		// .then(function(resp) {
		//
		// 	console.log("RESPONSE TO FETCH 1:");
		// 	console.log(resp);
		//
		// 	var candidateData = {};
		//
		// 	console.log("CREATING CANDIDATE DATA DICT");
		// 	for(var i = 0; i < resp['objects'].length; i++) {
		// 		var candidate = resp['objects'][i];
		// 		var candidateName = candidate['name'];
		// 		var candidateImage = candidate['image'];
		// 		var candidateParty = candidate['party'];
		// 		candidateData[candidateName] = {};
		// 		candidateData[candidateName]['image'] = candidateImage;
		// 		candidateData[candidateParty]['party'] = candidateParty;
		// 	}
		// 	console.log("COMPLETED CANDIDATE DATA DICT");
		//
		// 	console.log("RETURNING CANDIDATE DATA TO BE USED LATER");
		// 	return candidateData;
		// })
		// .then(function(candidateData) {

			console.log("MAKING SECOND FETCH");
	    fetch(String.raw`https://api.2020guide.me/positions?results_per_page=77`)
	    .then((resp) => resp.json())
	    .then( function(resp) {

	      console.log("RUNNING FETCH");

	      var issueDict = {};
	      var candidateDict = {};
				var whitelist = [
					"Foreign involvement",
					"Environmental issues",
					"Abortion",
					"Criminal justice",
					"LGBT issues",
					"Labor and welfare issues",
					"Health care"
				];
				var whitelistAbbrev = [
					"FORIN",
					"ENVIR",
					"ABRTN",
					"CRMJS",
					"LGBTI",
					"LBNWL",
					"HTHCR"
				];

	      // Iterate over all issues
	      for(var issueNum in resp['objects']) {

	        var issue = resp['objects'][issueNum];

	        var issueGroup = issue['issue']['name'];

					// skip non-whitelisted issue groups
					if(whitelist.indexOf(issueGroup) === -1) {
						continue;
					}

	        // Increment the number of issues in the issue group
	        if(issueGroup in issueDict) {
	          issueDict[issueGroup] = issueDict[issueGroup] + 1;
	        } else {
	          issueDict[issueGroup] = 1;
	        }

					// console.log("TESTING");

	        // Iterate over candidate positions on this issue
	        for(var candidateNum in issue['candidates']) {

	          var candidate = issue['candidates'][candidateNum];

	          // console.log(candidate);

	          var candidateName = candidate['candidate'];

	          // console.log(candidateName);

	          // Determine candidate existence
	          if(!(candidateName in candidateDict)) {
	            // console.log("EMPTY CANDIDATE DICTIONARY CREATED");
	            candidateDict[candidateName] = {};
	          }
	          // console.log("ESCAPED");

	          var candidateIssueDict = candidateDict[candidateName];

	          if(candidate['position'] === "Yes") {

	            // Increment this issue group's value for this candidate
	            if(issueGroup in candidateIssueDict) {
	              candidateIssueDict[issueGroup] = candidateIssueDict[issueGroup] + 1;
	            } else {
	              candidateIssueDict[issueGroup] = 1;
	            }
	          }
	        }

					// console.log("ISSUE WAS " + issueGroup);
	      }

				candidateDict['Donald Trump'] = {};

				// console.log("MADE IT THIS FAR");

	      for(var candidate in candidateDict) {
	        for(var issueGroup in issueDict) {
						var candidateIssueDict = candidateDict[candidate];
						if(!(issueGroup in candidateIssueDict)) {
							candidateIssueDict[issueGroup] = 0;
						} else {
							candidateIssueDict[issueGroup] /= issueDict[issueGroup];
						}
					}
	      }

				var svg = d3.select("#hexDiv")
		                .append("svg")
		                .attr("width", w)
		                .attr("height", h);

				var offx = 215, offy = 1145;

				// svg.append("rect")
				// .attr("x", 0)
				// .attr("y", 0)
				// .attr("width", w)
				// .attr("height", h)
				// .attr("fill", "#FFFF00");

				svg.append("rect")
				.attr("x", offx + 48)
				.attr("y", offy - 31)
				.attr("width", 275)
				.attr("height", 180)
				.attr("fill", "#E5E5E5");

				for(var i = 0; i < whitelist.length; i++) {
					svg.append("text")
					.text(whitelistAbbrev[i])
					.attr("fill", "#000000")
					.attr("x", offx + 65)
					.attr("y", offy + i * 22)
					.attr("font-size", 16);

					svg.append("text")
					.text("- " + whitelist[i])
					.attr("fill", "#000000")
					.attr("x", offx + 65 + 55)
					.attr("y", offy + i * 22)
					.attr("font-size", 16);
				}

				var candidateKeys = Object.keys(candidateDict);
				for(var i = 0; i < candidateKeys.length; i++) {
					var posx = 115 + (i % 4) * 190,
						  posy = 90 + Math.floor(i / 4) * 160;

					svg.append("polygon")
					.attr("points", getFullPolyPointString(whitelist, posx, posy, 50))
					.style("fill", "#D5D5D5");

					svg.append("polygon")
					.attr("points", getCandidatePolyPointString(candidateDict[candidateKeys[i]], whitelist, posx, posy, 50))
					.style("fill", "#0c6fc1");

					console.log("MADE POLYGON");

					var text = svg.append("text")
					.text(candidateKeys[i])
					.attr("fill", "#000000")
					.attr("x", posx)
					.attr("y", posy)
					.attr("font-size", 20);

					var bbox = text.node().getBBox();
					console.log(bbox);

					text.attr("x", posx - bbox.width / 2);
					text.attr("y", posy - bbox.height - 40);

					renderAbbrevOnPoly(svg, whitelistAbbrev, posx, posy + 54, 50);

				}

				console.log("APPENDED TEXT");

	      console.log("ISSUE DICTIONARY");
	      console.log(issueDict);
	      console.log("CANDIDATE DICTIONARY");
	      console.log(candidateDict);

	    // //Bind data and create one path per GeoJSON feature
		  // svg.selectAll("path")
		  //    .data(json.features)
		  //    .enter()
		  //    .append("path")
		  //    .attr("d", path)
		  //    .attr("className", "glow")
		  //    .style("fill", "#FF0000")
	    //    .style("fill", function(d){
	    //       console.log("xxxx I AM BEING CALLED xxxx");
	    //       var statename = d3.select(d)._groups[0][0].properties.NAME;
	    //       console.log("STATE:");
	    //       console.log(statename);
	    //
	    //       if(statename == "District of Columbia") {
	    //         // return "rgba(0,0,0,0)";
	    //         statename = "Maryland";
	    //       }
	    //
	    //       var entry = dict[statename];
	    //       console.log("DICTIONARY ENTRY: ");
	    //       console.log(entry);
	    //
	    //       console.log("CALLING FUNC");
	    //       console.log(entry);
	    //       console.log(minph);
	    //       console.log(maxph);
	    //       var colorval = getColorFromPh(entry, minph, maxph);
	    //       console.log("RETURNED FROM THE FUNCTION");
	    //       console.log(colorval);
	    //     return colorval;
	    //    })
		  //    .on("click",function(d) {
		  //      console.log("just had a mouseover", d3.select(d)._groups[0][0].properties.NAME);
	    //      window.location = "https://waterwedoing.me/states/:" + d3.select(d)._groups[0][0].properties.NAME;
		  //    })
	    //    .on("mouseover", function(d) {
	    //      d3.select(this).style("fill-opacity", "0.5");
	    //    })
	    //    .on("mouseout", function(d) {
	    //      d3.select(this).style("fill-opacity", "1.0");
	    //    });
	    //
		  //    var mincolor = getColorFromPh(this.state.minph, this.state.minph, this.state.maxph);
		  //    var maxcolor = getColorFromPh(this.state.maxph, this.state.minph, this.state.maxph);
		  //    console.log("PH - ")
		  //    console.log(mincolor)
		  //    console.log(maxcolor)
	    //
	    //    this.setState(
	    //      {}
	    //    );

		  });
		// });
  }

  render (props) {

	  return (
		  <div className="hexDiv" id="hexDiv" componentDidMount={this.componentDidMount.bind(this)}></div>
	  );
  }
}

export default HexIssueGraph;
