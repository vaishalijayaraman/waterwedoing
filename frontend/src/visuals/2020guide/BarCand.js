import React, { Component } from 'react';

// D3
import * as d3 from "d3";

const styles = {
    barLabel:{
        fontFamily: ['Avenir',
      ].join(','),
        color: "#3d3d3d",
    },
}

class BarOrg extends Component {

    componentDidMount() {
    	const width_temp = 1000;
        const height_temp = 400;

        const svg = d3.select("#drawchart")
            .append('svg')
            .attr('id', 'chart')
            .attr('width', width_temp)
            .attr('height', height_temp);

        const margin = {
            top: 60,
            bottom: 100,
            left: 80,
            right: 40
        };

        var nameToAbbrev = {"Alabama": "AL", "Alaska": "AK", "Arizona": "AZ", "Arkansas": "AR", "California": "CA", "Colorado": "CO", "Connecticut": "CT", "Delaware": "DE", "Florida": "FL", "Georgia": "GA", "Hawaii": "HI", "Idaho": "ID", "Illinois": "IL", "Indiana": "IN", "Iowa": "IA", "Kansas": "KS", "Kentucky": "KY", "Louisiana": "LA", "Maine": "ME", "Maryland": "MD", "Massachusetts": "MA", "Michigan": "MI", "Minnesota": "MN", "Mississippi": "MS", "Missouri": "MO", "Montana": "MT", "Nebraska": "NE", "Nevada": "NV", "New Hampshire": "NH", "New Jersey": "NJ", "New Mexico": "NM", "New York": "NY", "North Carolina": "NC", "North Dakota": "ND", "Ohio": "OH", "Oklahoma": "OK", "Oregon": "OR", "Pennsylvania": "PA", "Rhode Island": "RI", "South Carolina": "SC", "South Dakota": "SD", "Tennessee": "TN", "Texas": "TX", "Utah": "UT", "Vermont": "VT", "Virginia": "VA", "Washington": "WA", "West Virginia": "WV", "Wisconsin": "WI", "Wyoming": "WY"};

        const chart = svg.append('g')
            .classed('display', true)
            .attr('transform', `translate(${margin.left},${margin.top})`);

        const width = width_temp - margin.left - margin.right;
        const height = height_temp - margin.top - margin.bottom;

        console.log("WIDTH");
        console.log(width);
        console.log("HEIGHT");
        console.log(height);

        console.log("INFO");
    	
    	// GET DATA!!!
    	fetch(String.raw`https://api.waterwedoing.me/states/search?q={"size":500,"_source":["state_name","organizations"],"query":{"match_all":{}},"sort":[{"state_name.raw":{"order":"asc"}}]}`)
        .then((resp) => resp.json())
        .then((resp) => {

        var dataStuff = [];
        var list = resp['hits']['hits']

        for (var i = 0; i < list.length; i++) {
            dataStuff.push({
              state: nameToAbbrev[list[i]['_source']['state_name']],
              org: JSON.parse(list[i]['_source']['organizations'].replace(/\$/g, "\"")).length
            });

            console.log(list[i]);
        }
        
        const data =  dataStuff;

        // create scales!
        const xScale = d3.scaleBand()
            .domain(data.map(d => d.state))
            .range([0, width]);

        const yScale = d3.scaleLinear()
            .domain([0, d3.max(data, d => d.org)])
            .range([height, 0]);

        const colorScale = d3.scaleOrdinal(d3.schemeBlues[9]);

        chart.selectAll('.bar')
            .data(data)
            .enter()
            .append('rect')
            .classed('bar', true)
            .attr('x', d => xScale(d.state))
            .attr('y', d => yScale(d.org))
            .attr('height', d => (height - yScale(d.org)))
            .attr('width', d => xScale.bandwidth())
            .style('fill', (d, k) => colorScale(k));

        chart.selectAll('.barLabel')
            .data(data)
            .enter()
            .append('text')
            .classed('barLabel', true)
            .attr('x', d => xScale(d.state) + xScale.bandwidth()/2)
            .attr('y', d => yScale(d.org))
            .attr('dy', -5)
            .text(d => d.org)
            .style('font-size', '10px')
            .style('text-anchor', 'middle')
            .style('font-family', 'Avenir');

        const xAxis = d3.axisBottom()
            .scale(xScale);
            
        chart.append('g')
            .classed('x axis', true)
            .attr('transform', `translate(0,${height})`)
            .call(xAxis);

        const yAxis = d3.axisLeft()
            .ticks(5)
            .scale(yScale);

        chart.append('g')
            .classed('y axis', true)
            .attr('transform', 'translate(0,0)')
            .call(yAxis);

        chart.select('.x.axis')
            .append('text')
            .attr('x',  width/2)
            .attr('y', 60)
            .attr('fill', '#000')
            .style('font-size', '20px')
            .style('font-weight', 'bold')
            .style('font-family', 'Avenir')
            .style('text-anchor', 'middle')
            .text('State');    
            
        chart.select('.y.axis')
            .append('text')
            .attr('x', 0)
            .attr('y', 0)
            .attr('transform', `translate(-50, ${height/2}) rotate(-90)`)
            .attr('fill', '#000')
            .style('font-size', '20px')
            .style('font-weight', 'bold')
            .style('font-family', 'Avenir')
            .style('text-anchor', 'middle')
            .text('Number of Organizations');   
            
        // const yGridlines = d3.axisLeft()
        //     .scale(yScale)
        //     .ticks(5)
        //     .tickSize(-width,0,0)
        //     .tickFormat('')

        // chart.append('g')
        //     .call(yGridlines)
        //     .classed('gridline', true);
        })
    }

  render (props) {
	return (
        <div>
		<div id="drawchart" style={styles.barLabel} componentDidMount={this.componentDidMount.bind(this)}></div>
        </div>
	);
  }
}

export default BarOrg;