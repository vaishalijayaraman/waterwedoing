import React, { Component } from 'react';
import { Route } from 'react-router-dom';

// Material UI
import {MuiThemeProvider, createMuiTheme} from '@material-ui/core/styles';

// CSS
import './App.css';

// Components
import Header from './components/Header.js';
import Footer from './components/Footer.js';

// Pages
import About from './pages/About.js';
import Counties from './pages/Counties.js';
import States from './pages/States.js';
import Organizations from './pages/Organizations.js';
import Splash from './pages/Splash.js';
import SearchPage from './pages/SearchPage.js';
import Visualizations from './pages/Visualizations.js';

// Instance Pages
import StateInstance from './pages/StateInstance.js';
import CountyInstance from './pages/CountyInstance.js';
import OrganizationInstance from './pages/OrganizationInstance.js';


class App extends Component {
  render() {
    return (
      <MuiThemeProvider theme={theme}>
	<div className="App">
        <header>
        <Header/>
        </header>
        <Route path="/" exact component={Splash}/>
        <Route path="/search" exact component={SearchPage}/>
        <Route path="/counties" exact component={Counties}/>
        <Route path="/states" exact component={States}/>
        <Route path="/organizations" exact component={Organizations}/>
        <Route path="/about" exact component={About}/>
        <Route path="/states/:key" exact component={StateInstance}/>
        <Route path="/counties/:key" exact component={CountyInstance}/>
        <Route path="/organizations/:key" exact component={OrganizationInstance}/>
        <Route path="/visualizations" exact component={Visualizations}/>
      </div>
      <Footer/>
    </MuiThemeProvider>);
  }
}

const theme = createMuiTheme({
  typography: {
    fontFamily: 'Avenir, Trebuchet',
  },
  palette: {
    primary: {
      main: '#0c6fc1',
      light: '#0c6fc1',
      dark: '#0c6fc1'
    },
    secondary: {
      main: '#fff'
    }
  }
});


export default App;
