//modeled tests after https://gitlab.com/joswiatek/catch-me-outside/blob/master/frontend/src/App.test.js

import React from 'react';
import ReactDOM from 'react-dom';
import Enzyme, { configure, shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

import API from './services/api.js';
import AboutCard from './components/AboutCard.js';
import CountyCard from './components/CountyCard.js';
import Footer from './components/Footer.js';
import Header from './components/Header.js';
import LogoCard from './components/LogoCard.js';
import OrganizationCard from './components/OrganizationCard.js';
import PhScale from './components/PhScale.js';
import SplashCard from './components/SplashCard.js';
import StateCard from './components/AboutCard.js';

import About from './pages/About.js';
import Counties from './pages/Counties.js';
import CountyInstance from './pages/CountyInstance.js';
import Organizations from './pages/Organizations.js';
import OrganizationInstance from './pages/OrganizationInstance.js';
import SearchPage from './pages/SearchPage.js';
import Splash from './pages/Splash.js';
import States from './pages/States.js';
import StateInstance from './pages/StateInstance.js';


Enzyme.configure({ adapter: new Adapter() });

var assert = require('assert');

// describe('getStates', function() {
// 	describe('getStates(page)', function() {
// 		it('returns a list of the states', function(){
// 			assert.equal(-1, [1,2,3].indexOf(4));
// 		});
// 	});
// });

// describe('getCounties', function() {
// 	describe('getCounties(page)', function() {
// 		it('check return status', function(){
// 			assert.equal(-1, [1,2,3].indexOf(4));
// 		});
// 	});
// });

// describe('getOrganizations', function() {
// 	describe('getOrganizations(page)', function() {
// 		it('returns a list of the organizations', function(){
// 			assert.equal(-1, [1,2,3].indexOf(4));
// 		});
// 	});
// });

describe('Page Tests', function() {
	describe('About', function() {
		it('About tests', function() {
			let v = new About();
			expect(!(typeof v.state === 'undefined'));
			expect((typeof v.countCommits === 'function'));
			expect((typeof v.countIssues === 'function'));
			expect((typeof v.componentDidMount === 'function'));
			expect((typeof v.render === 'function'));
		});
	});
	describe('Counties', function() {
		it('Counties tests', function() {
			let v = new Counties();
			expect(!(typeof v.state === 'undefined'));
			expect((typeof v.loadData === 'function'));
			expect((typeof v.render === 'function'));
		});
	});
	// describe('CountyInstance', function() {
	// 	it('CountyInstance tests', function() {
	// 		let v = new CountyInstance();
	// 		expect(!(typeof v.state === 'undefined'));
	// 		expect((typeof v.loadData === 'function'));
	// 		expect((typeof v.genOrgLinks === 'function'));
	// 		expect((typeof v.genStateLinks === 'function'));
	// 		expect((typeof v.render === 'function'));
	// 	});
	// });
	describe('Organizations', function() {
		it('Organizations tests', function() {
			let v = new Organizations();
			expect(!(typeof v.state === 'undefined'));
			expect((typeof v.loadData === 'function'));
			expect((typeof v.randomImage === 'function'));
			expect((typeof v.render === 'function'));
		});
	});
	// describe('OrganizationInstance', function() {
	// 	it('OrganizationInstance tests', function() {
	// 		let v = new OrganizationInstance();
	// 		expect(!(typeof v.state === 'undefined'));
	// 		expect((typeof v.loadData === 'function'));
	// 		expect((typeof v.genCountyLinks === 'function'));
	// 		expect((typeof v.genStateLinks === 'function'));
	// 		expect((typeof v.randomImage === 'function'));
	// 		expect((typeof v.render === 'function'));
	// 	});
	// });
	describe('SearchPage', function() {
		it('SearchPage tests', function() {
			let v = new SearchPage();
			expect(!(typeof v.state === 'undefined'));
			expect((typeof v.loadData === 'function'));
			expect((typeof v.randomImage === 'function'));
			expect((typeof v.render === 'function'));
		});
	});
	describe('Splash', function() {
		it('Splash tests', function() {
			let v = new Splash();
			expect((typeof v.render === 'function'));
		});
	});
	describe('States', function() {
		it('States tests', function() {
			let v = new States();
			expect(!(typeof v.state === 'undefined'));
			expect((typeof v.loadData === 'function'));
			expect((typeof v.render === 'function'));
		});
	});
	// describe('StateInstance', function() {
	// 	it('StateInstance tests', function() {
	// 		let v = new StateInstance();
	// 		expect(!(typeof v.state === 'undefined'));
	// 		expect((typeof v.loadData === 'function'));
	// 		expect((typeof v.genCountyLinks === 'function'));
	// 		expect((typeof v.genOrgLinks === 'function'));
	// 		expect((typeof v.render === 'function'));
	// 	});
	// });
}); 

describe('Counties.js Tests', function() {
	describe('Counties', function() {
		it('Counties state', function() {
			let v = new Counties();
			expect(!(typeof v.state === 'undefined'));
		});
	});
	describe('Counties', function() {
		it('Counties loadData', function() {
			let v = new Counties();
			expect((typeof v.state === 'function'));
		});
	});
}); 

describe('Component Tests', function() {
	describe('<AboutCard>', () => {
	    it('component renders', () => {
	      const wrapper = shallow(<AboutCard />);
	      expect(wrapper.exists()).toBe(true);
	      assert(AboutCard("") != null);
	    });
	});
	// describe('<CountyCard>', () => {
	//     it('component renders', () => {
	//       const wrapper = shallow(<CountyCard />);
	//       expect(wrapper.exists()).toBe(true);
	//       assert(CountyCard("") != null);
	//     });
	// });
	describe('<Footer>', () => {
	    it('component renders', () => {
	      const wrapper = shallow(<Footer />);
	      expect(wrapper.exists()).toBe(true);
	      assert(Footer("") != null);
	    });
	});
	describe('<Header>', () => {
	    it('component renders', () => {
	      const wrapper = shallow(<Header />);
	      expect(wrapper.exists()).toBe(true);
	      let v = new Header();
	      assert(v != null);
	      assert(v.render() != null)
	    });
	});
	describe('<LogoCard>', () => {
	    it('component renders', () => {
	      const wrapper = shallow(<LogoCard />);
	      expect(wrapper.exists()).toBe(true);
	      assert(LogoCard("") != null);
	    });
	});
	describe('<OrganizationCard>', () => {
	    it('component renders', () => {
	      const wrapper = shallow(<OrganizationCard />);
	      expect(wrapper.exists()).toBe(true);
	      assert(OrganizationCard("") != null);
	    });
	});
	// describe('<PhScale>', () => {
	//     it('component renders', () => {
	//       const wrapper = shallow(<PhScale />);
	//       expect(wrapper.exists()).toBe(true);
	//     });
	// });
	describe('<SplashCard>', () => {
	    it('component renders', () => {
	      const wrapper = shallow(<SplashCard />);
	      expect(wrapper.exists()).toBe(true);
	      assert(SplashCard("") != null);
	    });
	});
	describe('<StateCard>', () => {
	    it('component renders', () => {
	      const wrapper = shallow(<StateCard />);
	      expect(wrapper.exists()).toBe(true);
	      assert(StateCard("") != null);
	    });
	});
});

describe('Page Tests', function() {
	describe('<About>', () => {
		it('component renders', () => {
			const wrapper = shallow(<About />);
			expect(wrapper.exists()).toBe(true);
		});
	});
	describe('<Counties>', () => {
		it('component renders', () => {
			const wrapper = shallow(<Counties />);
			expect(wrapper.exists()).toBe(true);
		});
	});
	// describe('<CountyInstance>', () => {
	// 	it('component renders', () => {
	// 		const wrapper = shallow(<CountyInstance />);
	// 		expect(wrapper.exists()).toBe(true);
	// 	});
	// });
	// describe('<Organizations>', () => {
	// 	it('component renders', () => {
	// 		const wrapper = shallow(<OrganizationInstance />);
	// 		expect(wrapper.exists()).toBe(true);
	// 	});
	// });
	// describe('<OrganizationInstance>', () => {
	// 	it('component renders', () => {
	// 		const wrapper = shallow(<OrganizationInstance />);
	// 		expect(wrapper.exists()).toBe(true);
	// 	});
	// });
	describe('<Splash>', () => {
		it('component renders', () => {
			const wrapper = shallow(<Splash />);
			expect(wrapper.exists()).toBe(true);
		});
	});
	describe('<States>', () => {
		it('component renders', () => {
			const wrapper = shallow(<States />);
			expect(wrapper.exists()).toBe(true);
		});
	});
	// describe('<StateInstance>', () => {
	// 	it('component renders', () => {
	// 		const wrapper = shallow(<StateInstance />);
	// 		expect(wrapper.exists()).toBe(true);
	// 	});
	// });
});



