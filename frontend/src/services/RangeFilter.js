import Filter from "./Filter.js"

/*
  * Constructs a Filter object which represents filtering by
  * a specific field for values in the given closed interval.
  *
  * field - the name of the field, as a string.
  *
  * left - a numeric value representing the left endpoint of
  *     the interval. (inclusive)
  *
  * right - a numeric value representing the right endpoint
  *     of the interval. (inclusive)
  *
  */
 class RangeFilter extends Filter {
    constructor(field, left, right) {
        super();
        this.field = field;
        this.left = left;
        this.right = right;
        return this;
    }

    getFilterString() {
        var res = String.raw`{ "range": { "` + this.field + String.raw`": {`;
        
        if(this.left != null) {
            res += String.raw`"gte": ` + this.left;
        }

        if(this.right != null) {
            if(this.left != null) {
                res += String.raw`,`;
            }
            res += String.raw`"lte": ` + this.right;
        }
        
        res += String.raw`}}}`;
        return res;
    }

    getNotFilterString(){
        return String.raw`"not": {
            ` + this.getFilterString() + String.raw`
        }`;
    }
}

export default RangeFilter;
