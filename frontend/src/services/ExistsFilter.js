import Filter from "./Filter.js"

class ExistsFilter extends Filter {
    constructor(field) {
        super();
        this.field = field;
        return this;
    }

    getFilterString() {
        return String.raw`{
            "wildcard": {
                "` + this.field + String.raw`": "?*"
            }
        }`;
    }

    getNotFilterString() {
        return String.raw`"not": {
            ` + this.getFilterString() + String.raw`
        }`;
    }
}

export default ExistsFilter;
