const API_LINK = "https://api.waterwedoing.me/";// "https://d32m6g3mr3e1a9.cloudfront.net/";

class API {
static getCounties(page) {

    var result = [];

    var call = API_LINK + String.raw`counties?page=` + page + String.raw`&q={"field": "county_name", "direction": "asc"}`;

    console.log("API CALL: " + call);

    return fetch(call)
    .then((resp) => resp.text())
    .then(function(resp) {
      return resp.replace(/NaN/g, "null");
    })
    .then(function(resp) {
      return JSON.parse(resp);
    })
    .then(function(jsone) {

    var instancelist = jsone.objects;

    var i;
    for(i = 0; i < instancelist.length; i++) {
      var org = instancelist[i];
      var res = new Object();

      if(org.organizations != null) {
        res.organizations = JSON.parse(org.organizations.replace(/\$/g, "\""));
      }

      res.name = org.county_name;
      res.svg_url = org.county_svg_url;
      res.thumb_url = org.county_thumb_url;
      if(res.thumb_url != null) {
      	var lastSlash = res.thumb_url.lastIndexOf("/");
      	res.thumb_url = res.thumb_url.replace("commons", "commons/thumb") + "/320px-" + res.thumb_url.substring(lastSlash+1) + ".png";
      }
      res.newest_date = org.new_date;
      res.nitrate = org.nitrate;
      res.oldest_date = org.old_date;
      res.ph = org.ph;
      res.sodium = org.sodium;
      res.turbidity = org.turbidity;

      result.push(res);
    }

    console.log("API RETURN: " + result);

    return result;

    });

  }

static getStates(page) {

  var result = [];

  var call = API_LINK + String.raw`states?page=` + page + String.raw`&q={"field": "name", "direction": "asc"}`;

  console.log("API CALL: " + call);

  return fetch(call)
  .then((resp) => resp.text())
  .then(function(resp) {
    return resp.replace(/NaN/g, "null");
  })
  .then(function(resp) {
    return JSON.parse(resp);
  })
  .then(function(jsone) {

  var instancelist = jsone.objects;

  var i;
  for(i = 0; i < instancelist.length; i++) {
    var state = instancelist[i];
    var res = new Object();

    console.log(state);

    if(state.counties != null) {
      res.counties = JSON.parse(state.counties.replace(/\$/g, "\""));
    }
    else {
      res.counties = state.counties;
    }

    res.organizations = state.organizations;
    if(state.organizations != null) {
      res.organizations = JSON.parse(state.organizations.replace(/\$/g, "\""));
    }

    //res.counties = state.counties;
    res.nitrate = state.nitrate;
    res.ph = state.ph;
    res.population = state.population;
    res.sodium = state.sodium;
    res.flag_url = state.state_flag_url.trim();
    res.name = state.state_name;
    res.svg_url = state.state_svg_url;
    res.turbidity = state.turbidity;

    result.push(res);
  }

  console.log("API RETURN: " + result);

  return result;

  }); // end of promise chain

}

static getOrganizations(page) {

  var result = [];

  var call = API_LINK + String.raw`organizations?page=` + page + String.raw`&q={"field": "name", "direction": "asc"}`;

  console.log("API CALL: " + call);

  return fetch(call)
  .then((resp) => resp.text())
  .then(function(resp) {
    return resp.replace(/NaN/g, "null");
  })
  .then(function(resp) {
    return JSON.parse(resp);
  })
  .then(function(jsone) {

  var instancelist = jsone.objects;

  var i;
  for(i = 0; i < instancelist.length; i++) {
    var org = instancelist[i];
    var res = new Object();

    if(org.counties != null) {
      res.counties = JSON.parse(org.counties.replace(/\$/g, "\""));
    }

    res.states = org.states;
    if(org.states != null) {
      res.states = JSON.parse(org.states.replace(/\$/g, "\""));
    }

    res.address = org.address;
    res.city = org.city;
    res.email = org.email;
    res.id = org.org_identifier;
    res.img_url = org.org_image_url;
    res.name = org.org_name;
    res.type = org.org_type;
    res.phone = org.phone;

    result.push(res);
  }

  console.log("API RETURN: " + result);

    return result;

  }); // end of promise chain

}

static getStateVerbose(keySent) {

  var result = [];

  var call = API_LINK + String.raw`
states?page=1&q={"filters":[{"name":"state_name","op":"eq","val":"` + keySent + String.raw`"}]}`;

  console.log("API CALL: " + call);

  return fetch(call)
  .then((resp) => resp.text())
  .then(function(resp) {
    return resp.replace(/NaN/g, "null");
  })
  .then(function(resp) {
    return JSON.parse(resp);
  })
  .then(function(jsone) {

  var instancelist = jsone.objects;

  var i;
  for(i = 0; i < instancelist.length; i++) {
    var state = instancelist[i];
    var res = new Object();

    if(state.counties != null) {
      res.counties = JSON.parse(state.counties.replace(/\$/g, "\""));
    }

    res.organizations = state.organizations;
    if(state.organizations != null) {
      res.organizations = JSON.parse(state.organizations.replace(/\$/g, "\""));
    }

    //res.counties = state.counties;
    res.nitrate = state.nitrate;
    res.ph = state.ph;
    res.population = state.population;
    res.sodium = state.sodium;
    res.flag_url = state.state_flag_url.trim();
    res.name = state.state_name;
    res.svg_url = state.state_svg_url;
    res.turbidity = state.turbidity;

    result.push(res);
  }

  console.log("API RESULT: " + result);

  return result;

  }); // end of promise chain

}

static getOrganizationVerbose(keySent) {

  var result = [];

  var call = API_LINK + String.raw`
organizations?page=1&q={"filters":[{"name":"org_identifier","op":"eq","val":"` + keySent + String.raw`"}]}`;

  console.log("API CALL: " + call);

  return fetch(call)
  .then((resp) => resp.text())
  .then(function(resp) {
    return resp.replace(/NaN/g, "null");
  })
  .then(function(resp) {
    return JSON.parse(resp);
  })
  .then(function(jsone) {

  var instancelist = jsone.objects;

  var i;
  for(i = 0; i < instancelist.length; i++) {
    var org = instancelist[i];
    var res = new Object();

    if(org.counties != null) {
      res.counties = JSON.parse(org.counties.replace(/\$/g, "\""));
    }

    res.states = org.states;
    if(org.states != null) {
      res.states = JSON.parse(org.states.replace(/\$/g, "\""));
    }

    res.address = org.address;
    res.city = org.city;
    res.email = org.email;
    res.id = org.org_identifier;
    res.img_url = org.org_image_url;
    res.name = org.org_name;
    res.type = org.org_type;
    res.phone = org.phone;

    result.push(res);

  }

  console.log("API RETURN: " + result);

  return result;

  }); // end of promise chain

}

static getCountyVerbose(keySent) {

    var result = [];

    var call = API_LINK + String.raw`
counties?page=1&q={"filters":[{"name":"county_name","op":"eq","val":"` + keySent + String.raw`"}]}`;

    console.log("API CALL: " + call);

    return fetch(call)
    .then((resp) => resp.text())
    .then(function(resp) {
      return resp.replace(/NaN/g, "null");
    })
    .then(function(resp) {
      return JSON.parse(resp);
    })
    .then(function(jsone) {

    var instancelist = jsone.objects;

    var i;
    for(i = 0; i < instancelist.length; i++) {
      var org = instancelist[i];
      var res = new Object();

      if(org.organizations != null) {
        res.organizations = JSON.parse(org.organizations.replace(/\$/g, "\""));
      }

      res.name = org.county_name;
      res.svg_url = org.county_svg_url;
      res.thumb_url = org.county_thumb_url;
      if(res.thumb_url != null) {
      	var lastSlash = res.thumb_url.lastIndexOf("/");
      	res.thumb_url = res.thumb_url.replace("commons", "commons/thumb") + "/320px-" + res.thumb_url.substring(lastSlash+1) + ".png";
      }
      res.newest_date = org.new_date;
      res.nitrate = org.nitrate;
      res.oldest_date = org.old_date;
      res.ph = org.ph;
      res.sodium = org.sodium;
      res.turbidity = org.turbidity;
      res.state = org.state;

      result.push(res);
    }

    console.log("API RETURN: " + result);

    return result;

    }); // end of promise chain

  }

  /****************************************************************************/
  /****************************************************************************/
  /**                                                                        **/
  /**       000000    0000000     00000     000000      000000     00    00  **/
  /**     000    00  00         00   00    00    00   000    00   00    00   **/
  /**       000     00000     000000000   000000     00          00000000    **/
  /**  000   000   00        00     00   00    00   000    00   00    00     **/
  /**   000000    0000000   00     00   00    00     000000    00    00      **/
  /**                                                                        **/
  /****************************************************************************/
  /****************************************************************************/

  static str(vari) {
    if(vari == null) {
      return null;
    } else {
      return vari.toString();
    }
  }

  static parseHits(searchterm, resjson) {

    console.log("parsehits");
    console.log("resjoin");
    console.log(resjson);

    var hitlist = resjson['hits']['hits'];

    var results = [];

    for(var i = 0; i < hitlist.length; i++) {

      console.log("hit " + i);

      var hit = hitlist[i];
      var res = new Object();
      res.es_type = hit._type;
      res.es_id = hit._id; // id to be used for es.index()
      res.es_score = hit._score;

      switch(res.es_type) {
        case "county_instance":
          console.log("counties index");
          console.log(hit._source.county_name);

          res.name = hit._source.county_name;
          res.svg_url = hit._source.county_svg_url;

          res.thumb_url = null;
          if(hit._source.county_thumb_url != null) {
          	var lastSlash = hit._source.county_thumb_url.lastIndexOf("/");
          	res.thumb_url = hit._source.county_thumb_url.replace("commons", "commons/thumb") + "/320px-" + hit._source.county_thumb_url.substring(lastSlash+1) + ".png";
          }

          res.newest_date = hit._source.new_date;
          res.oldest_date = hit._source.old_date;
          res.nitrate = API.str(hit._source.nitrate);
          res.organizations = null;
          if(hit._source.organizations != null) {
            console.log("PARSED JSON: county, orgs list");
            res.organizations = JSON.parse(hit._source.organizations.replace(/\$/g, "\""));
          }
          res.ph = API.str(hit._source.ph);
          res.sodium = API.str(hit._source.sodium);
          res.state = API.str(hit._source.state);
          res.turbidity = API.str(hit._source.turbidity);
          console.log(res);
          results.push(res);
          break;
        case "organization_instance":
          console.log("organizations index");
          console.log(hit);
          res.id = hit._source.org_identifier;
          res.name = hit._source.org_name;
          res.type = hit._source.org_type;
          res.city = hit._source.city;
          res.states = null;
          if(hit._source.states != null) {
            console.log("PARSED JSON: org, states list");
            res.states = JSON.parse(hit._source.states.replace(/\$/g, "\""));
          }
          res.counties = null;
          if(hit._source.counties) {
            console.log("PARSED JSON: org, counties list");
            res.counties = JSON.parse(hit._source.counties.replace(/\$/g, "\""));
          }
          res.counties = hit._source.counties;
          res.email = hit._source.email;
          res.phone = hit._source.phone;
          res.address = hit._source.address;
          res.image_url = hit._source.org_image_url;
          results.push(res);
          break;
        case "state_instance":
          console.log("states index");
          console.log(hit);
          res.nitrate = API.str(hit._source.nitrate);
          res.ph = API.str(hit._source.ph);
          res.population = API.str(hit._source.population);
          res.sodium = API.str(hit._source.sodium);
          res.turbidity = API.str(hit._source.turbidity);
          res.flag_url = hit._source.state_flag_url;
          res.name = hit._source.state_name;
          res.svg_url = hit._source.state_svg_url;
          res.counties = null;
          if(hit._source.counties != null) {
            console.log("PARSED JSON: state, counties list");
            res.counties = JSON.parse(hit._source.counties.replace(/\$/g, "\""));
          }
          res.organizations = null;
          if(hit._source.organizations != null) {
            console.log("PARSED JSON: state, orgs list");
            res.organizations = JSON.parse(hit._source.organizations.replace(/\$/g, "\""));
          }

          console.log("CHECKING SEARCH");
          if(searchterm != null) {

            console.log("SEARCH TERM IS NOT NULL");
          //   var open = "<span id=\"highlight\">";
          //   var close = "</span>";
          //
          //   var openmark = "**";
          //   var closemark = "^^";
          //
          //   console.log("SEARCH TERM: " + searchterm);
          //   var tokens = searchterm.split(" ");
          //   console.log("INTERATING OVER TOKENS");
          //   for(var k = 0; k < tokens.length; k++) {
          //     var token = tokens[k];
          //     console.log("TOKEN " + token);
          //     for(var entry in res) {
          //       console.log("SEARCHING IN " + entry + " VALUE IS " + res[entry]);
          //       if(typeof entry === 'string' || entry instanceof String) {
          //         var idx = entry.indexOf(token);
          //         if(idx != -1) {
          //           entry = entry.substring(0, idx) + openmark + entry.substring(idx, idx + token.length()) + closemark + entry.substring(idx + token.length());
          //           console.log("FOUND");
          //         }
          //       }
          //     }
          //   }
          //
          //   for(var entry in res) {
          //     var intervals = [];
          //     var start = -1;
          //     var end = -1;
          //     for(var i = 0; i < entry.length-1; i++) {
          //       if (entry.charAt(i) == '*' && entry.charAt(i+1) == '*') {
          //         if(start == -1) {
          //           start = i;
          //         }
          //       }
          //       if(entry.charAt(i) == '^' && entry.charAt(i+1) == '^') {
          //         if(start != -1) {
          //           end = i;
          //           intervals.push(start);
          //           intervals.push(end);
          //           start = -1;
          //           end = -1;
          //         }
          //       }
          //       for(var j = 0; j < intervals.length; j+=2) {
          //         var s = intervals[j];
          //         var e = intervals[j+1];
          //         entry = entry.substring(0, s) + open + entry.substring(s, e) + close + entry.substring(e);
          //       }
          //       entry = entry.replace(/\*/g, "");
          //       entry = entry.replace(/\^/g, "");
          //     }
          //   }
          // }

          // break;
      }

      results.push(res);
      break;
      }

      console.log("ENTERING THE TOKEN FINDER");
      // if(searchterm != null && searchterm != "") {
      //   var tokens = searchterm.split(" ");
      //   var killct = 4000;
      //   for(var k = 0; k < tokens.length; k++) {
      //     console.log("TOKEN " + tokens[k]);
      //     for(var entry in res) {
      //       if(res.hasOwnProperty(entry)) {
      //         console.log("ENTRY " + entry + "WITH VALUE " + res[entry]);
      //         console.log(typeof entry);
      //         if(res[entry] != null && entry.indexOf("url") == -1) {
      //           console.log("NOT NULL");
      //            if(typeof res[entry] === 'string' || res[entry] instanceof String) {
      //             //var first_idx = res[entry].indexOf(tokens[k]);
      //             var idxs = [];
      //             // for(var i = first_idx; i != -1; i = res[entry].indexOf(tokens[k], i))
      //             //   idxs.push(i);
      //             var i = 0;
      //             while(i < res[entry].length){
      //               var idx = res[entry].indexOf(tokens[k], i);
      //               if (idx == -1) {
      //                 break;
      //               } else {
      //                 i = idx + tokens[k].length;
      //                 idxs.push(idx);
      //               }
      //             }
      //             // for(var idx in idxs) {
      //             //   console.log("FOUND AT " + idx);
      //             //   res[entry] = res[entry].substring(0, idx) + '**' + res[entry].substring(idx, idx + tokens[k].length) + String.raw`**` + res[entry].substring(idx + tokens[k].length);
      //             // }
      //            }
      //         }
      //       }
      //     }
      //   }
      // }
    }
    console.log("RESULTS:");
    console.log(results);
    return results;
  }

  static doSearchWithResultCount(index, page, search, sort, sort_direction, filters) {
    // do fetch

    console.log("page:    " + page);
    console.log("search:  " + search);
    console.log("sort:    " + sort);
    console.log("filters: ");
    console.log(filters);

    const resct = 9;

    var from = (resct * (page-1));

    if (!sort) {
      if (!search) {
        if (!index) {
          sort = String.raw`[
            "_score"
          ]`;
        }
        else if (index == "states") {
          sort = "state_name.raw";
          sort = String.raw`[
            {"` + sort + String.raw`": {"order": "` + sort_direction + String.raw`"}
        }
      ]`;
        } else if (index == "counties") {
          sort = "county_name.raw";
          sort = String.raw`[
            {"` + sort + String.raw`": {"order": "` + sort_direction + String.raw`"}
        }
      ]`;
        } else if (index == "organizations") {
          sort = "org_name.raw";
          sort = String.raw`[
            {"` + sort + String.raw`": {"order": "` + sort_direction + String.raw`"}
        }
      ]`;
        }
      } else {
        sort = String.raw`[
          "_score"
        ]`;
      }
    } else {
        sort = String.raw`[
          {"` + sort + String.raw`": {"order": "` + sort_direction + String.raw`"}
      }
    ]`;
    }

    var filterString = "";

    if(filters != null && filters.length != 0) {
      filterString = String.raw`
        "filter": [
        `;

      for (var i = 0; i < filters.length; i++) {
        if(filters[i] != null) {
          filterString += filters[i].getFilterString();
          
          for (var check_index = i + 1; check_index < filters.length; check_index++) {
            if (filters[check_index] != null) {
              filterString += String.raw`,
              `;
              break;
            }
          }
        }
      }
      filterString += String.raw`
      ]`;
    } else {
      filterString = String.raw`"filter": []`;
    }

    var query = "";

    query += String.raw`"query": {
      "bool": {`;

    if(search==null || search == "") {
        query += String.raw`
        "must": {
          "match_all": {}
        },`;
        query += filterString;
        query += String.raw`
      }}`;
    } else {
          query += String.raw`"must": {
            "query_string" : {`;

            // if (["ph", "sodium", "turbidity", "nitrate"].includes(search)) {
            //   search += ".raw";
            // }
            search = search.toString();
            var searchList = search.replace(/[^A-Za-z0-9\.]+/, " ");
            searchList = searchList.split(" ");
            for (var i = 0; i < searchList.length - 1; i++) {
              searchList[i] = "*"+searchList[i]+"* ";
            }
            searchList[searchList.length-1] = "*"+searchList[searchList.length-1]+"*"
            var finalSearch = "";
            for (var i = 0; i < searchList.length; i++) {
              finalSearch = finalSearch + searchList[i];
            }

            query += String.raw`"query": "` + finalSearch + String.raw`",
              "fields": ["state_name.raw^4", "county_name.raw^3", "org_name.raw^3", "state.raw^0.5", "nitrate.raw^1.5", "population.raw^1.5", "sodium.raw^1.5", "turbidity.raw^1.5", "ph.raw^1.5", "address.raw^1.0", "city.raw^1.0", "counties.raw^1.0", "email.raw^1.0", "org_identifier.raw^1.0", "org_type.raw^1.0", "phone.raw^1.0"]
            }
          },
          `;
          query += filterString;
        query += String.raw`
      }}`;
    }

    var body = String.raw`{
      "from": ` + from  + String.raw`,
      "size": ` + resct + String.raw`,
      ` + query         + String.raw`,
      "sort": ` + sort  + String.raw`
    }`;

    body = body.toLowerCase();

    console.log("BODY:");
    console.log(body);
    console.log("FETCHING:");
    if(index == null || index == "all") {
      index = "";
    } else {
      index = index + "/";
    }

    var fetchurl = API_LINK + index + "search?q=%20" + body.replace(/\s+/g,"").replace(/\"/g,"%22");
    console.log(fetchurl);
    return fetch(fetchurl)
    .then((resp) => resp.text())
    .then(function(resp) {
      console.log("PARSING FETCHED CONTENT");
      console.log(resp);
      return resp;
    })
    .then((resp) => JSON.parse(resp))
    .then(function(esjson) {
      console.log("PARSED JSON:");
      console.log(esjson);
      var parsed = API.parseHits(search, esjson);
      console.log("PARSED CONTENT:");
      parsed.push(Math.ceil(esjson['hits'].total / resct));
      parsed.push(esjson['hits'].total);
      console.log(parsed);
      return parsed;
    });

  }

  /*
  * Creates an elastic search query from the input parameters, sends the query,
  * receives the response, and formats the result into an array of instance
  * objects.
  *
  * index - a string representing which model to search on "all" to search all
  *     models, and "states", "organizations", or "counties".
  *
  * page - the page to display, as a numeric type.
  *
  * search - the search string that the user types into the search bar. Can
  *     also be null or "" to not match on any string.
  *
  * sort - the name of the field to sort by. Doesn't have to exist in every
  *     instance.
  *
  * sort_direction - either "asc" or "desc" to sort in ascending or descending
  *     order respectively.
  *
  * filters - a list of Filter child objects of varying types (see imports)
  *
  *
  * RETURNS - a list of generic Objects with the correct attributes of the
  *     instances they represent. To tell which model they belong to, check the
  *     "type" attribute which should be one of the following strings:
  *     "state_instance", "county_instance", "organization_instance"
  */
  static doSearch(index, page, search, sort, sort_direction, filters) {
    return API.doSearchWithResultCount(index, page, search, sort, sort_direction, filters)
    .then(function(res) {
      res.pop();
      return res;
    });
  }

}

export {API};
