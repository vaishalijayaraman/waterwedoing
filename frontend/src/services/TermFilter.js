import Filter from "./Filter.js"

/*
  * A Filter object which represents filtering by
  * a specific field for the exact value requested.
  *
  * field - the name of the field, as a string.
  *
  * value - a string representing the exact value requested.
  *
  */
class TermFilter extends Filter {
    constructor(field, value) {
        super();
        this.field = field;
        this.value = value;
        return this;
    }

    getFilterString() {
        return String.raw`{ "term": { "` + this.field + 
            String.raw`": "` + this.value + String.raw`" }}`;
    }

    getNotFilterString(){
        return String.raw`"not": {
            ` + this.getFilterString() + String.raw`
        }`;
    }
}

export default TermFilter;
