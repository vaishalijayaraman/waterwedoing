/*
 * @author Andrew Hill
 *
 */
class Organization {
    constructor(identifier, org_name, org_type, email, phone_number, street_address, city, state) {
        this.identifier = identifier;
        this.name = org_name;
        this.org_type = org_type; // Can be private, local, state, federal
        this.email = email;
        this.phone_number = phone_number;
        this.street_address = street_address;
        this.city = city;
        this.state = state;
    }
}
