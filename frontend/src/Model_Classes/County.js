/*
 * @author Andrew Hill
 *
 */

class County extends WaterData {
    constructor(county_name, state_name, ph_level, sodium_level, turbidity, nitrate_level, sample_oldest_date, sample_newest_date) {
        super(ph_level, sodium_level, turbidity, nitrate_level);
        this.name = county_name;
        this.state = state_name;
        this.sample_oldest_date = sample_oldest_date;
        this.sample_newest_date = sample_newest_date;
    }
}
