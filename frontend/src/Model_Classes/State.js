/*
 * @author Andrew Hill
 *
 */

class State extends WaterData {
    constructor(state_name, state_abbv, county_list, public_LRP_usability) {
        super(county_list.average_ph_level, county_list.average_sodium_level, county_list.average_turbidity, county_list.average_nitrate_level);
        this.state_name = state_name;
        this.counties = county_list;
        this.state_abbv = state_abbv;
        this.public_LRP_usability = public_LRP_usability;
    }
}
