/*
 * @author Andrew Hill
 *
 */
class Site extends WaterData {
    constructor(county_code, ph_level, sodium_level, turbidity, nitrate_level) {
        super(ph_level, sodium_level, turbidity, nitrate_level);
        this.county_code = county_code;
    }
}