/*
 * @author Andrew Hill
 *
 */
class CountyList {
    constructor() {
        this.county_list = [];
        this.length = 0;
    }

    add(new_county) {
        this.county_list.push(new_county);
        length++;
    }

    length() {
        return this.length;
    }

    pop(){
        length--;
        return this.county_list.pop();
    }

    set_all_average_water_data() {
        this.set_average_ph_level();
        this.set_average_sodium_level();
        this.set_average_turbidity();
        this.set_average_nitrate_level();
    }

    set_average_ph_level() {
        this.average_ph_level = 0;

        for(var i = 0; i < this.county_list.length; i++) {
            this.average_ph_level += this.county_list[i];
        }

        this.average_ph_level /= this.county_list.length;
        return this.average_ph_level;
    }

    set_average_sodium_level() {
        this.average_sodium_level = 0;

        for(var i = 0; i < this.county_list.length; i++) {
            this.average_sodium_level += this.county_list[i];
        }

        this.average_sodium_level /= this.county_list.length;
        return this.average_sodium_level;
    }

    set_average_turbidity() {
        this.average_turbidity = 0;

        for(var i = 0; i < this.county_list.length; i++) {
            this.average_turbidity += this.county_list[i];
        }

        this.average_turbidity /= this.county_list.length;
        return this.average_turbidity;
    }

    set_average_nitrate_level() {
        this.average_nitrate_level = 0;

        for(var i = 0; i < this.county_list.length; i++) {
            this.average_nitrate_level += this.county_list[i];
        }

        this.average_nitrate_level /= this.county_list.length;
        return this.average_nitrate_level;
    }
}
