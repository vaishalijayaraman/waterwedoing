/*
 * @author Andrew Hill
 *
 */

// Build organization instances
org_list = [];
org_list[0] = new Organization("21TXBCH", "Texas General Land Office", "State/US Government", "craig.davis@glo.texas.gov", "512-463-8126", "P. O. Box 12873", "Austin", "TX");
org_list[1] = new Organization("RIVERKEEPER", "Riverkeeper", "Volunteer", "jepstein@riverkeeper.org", "914-478-501", "20 Secor Road", "Obssining", "NY");
org_list[2] = new Organization("CEDEN", "California State Water Resources Control Board", "State/US Government", "ceden@waterboards.ca.gov", "714-755-3220", "1001 I Street", "Sacramento", "CA");

// Build county instances
let county_list = new CountyList();
county_list.add(new County("Aransas", "Texas", 8.137671518, 2066.529665, 65.7322449, 0.003257294018, new Date('March 2, 1939'), new Date('January 1, 2019')));
county_list.add(new County("New York", "New York", 7.111721854, 4.48055555556, 4.8, 0.870530612245, new Date('November 10, 1970'), new Date('October 30, 2017')));
county_list.add(new County("Los Angeles", "California", 7.9898789, 100.7625328, 1.24943859593047, 7120.162734, new Date('January 1, 1901'), new Date('March 17, 2019')));

// Build State instances
state_list = [];

let tex_counties = new CountyList();
tex_counties.add(county_list[0]);
tex_counties.set_all_average_water_data();

state_list[0] = new State("Texas", "TX", tex_counties, 100.0);

let new_york_counties = new CountyList();
new_york_counties.add(county_list[1]);

state_list[1] = new State("New York", "NY", new_york_counties, 43.2);

let california_counties = new CountyList();
california_counties.add(county_list[2]);

state_list[2] = new State("California", "CA", california_counties, 41.8);
