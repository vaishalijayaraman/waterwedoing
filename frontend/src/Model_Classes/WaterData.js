/*
 * @author Andrew Hill
 *
 */
class WaterData {
    constructor(ph_level, sodium_level, turbidity, nitrate_level) {
        this.ph_level = ph_level;
        this.sodium_level = sodium_level;
        this.turbidity = turbidity;
        this.nitrate_level = nitrate_level;
    }
}