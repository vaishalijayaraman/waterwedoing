class SiteList {
    constructor() {
        this.site_list = [];
    }

    add(new_site) {
        site_list.add(new_site);
    }

    set_all_average_water_data() {
        this.set_average_ph_level();
        this.set_average_sodium_level();
        this.set_average_turbidity();
        this.set_average_nitrate_level();
    }

    set_average_ph_level() {
        this.average_ph_level = 0;

        site_list.forEach(function(site) {
            this.average_ph_level += site.ph_level;
        });

        this.average_ph_level /= site_list.length();
        return this.average_ph_level;
    }

    set_average_sodium_level() {
        this.average_sodium_level = 0;

        site_list.forEach(function(site) {
            this.average_sodium_level += site.sodium_level;
        });
        this.average_sodium_level /= site_list.length();
        return this.average_sodium_level;
    }

    set_average_turbidity() {
        this.average_turbidity = 0;

        site_list.forEach(function(site) {
            this.average_turbidity += site.turbidity;
        });

        this.average_turbidity /= site_list.length();
        return this.average_turbidity;
    }

    set_average_nitrate_level() {
        this.average_nitrate_level = 0;

        site_list.forEach(function(site) {
            this.average_nitrate_level += site.nitrate_level;
        });

        this.average_nitrate_level /= site_list.length();
        return this.average_nitrate_level;
    }
}