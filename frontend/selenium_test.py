# Selenium
from selenium import webdriver
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as ec
from selenium.webdriver.common.keys import Keys

# Python
import unittest
import time

# Variables
url = "https://www.waterwedoing.com/"
url = "http://localhost:3000/"

# Test Class
class SeleniumTest(unittest.TestCase):

	def setUp(self): #unittest construct
		webdriver.ChromeOptions().add_argument('--no-sandbox')
		self.driver = webdriver.Chrome(options=webdriver.ChromeOptions())
		self.url = url

	def tearDown(self): #unittest destruct
		self.driver.quit()

	''' TESTING SITE HEADER FUNCTIONALITY '''

	def test_header_splash_page(self): #check if splash page loads from header
		try:
			self.driver.get(self.url)
			header_splash = self.driver.find_element_by_class_name("splash-button")
			header_splash.click()
			self.assertEqual(self.driver.current_url, self.url+"")
		except NoSuchElementException as err:
			self.fail(err.msg)

	def test_header_about_page(self): #check if about page loads from header
		try:
			self.driver.get(self.url)
			header_about = self.driver.find_element_by_class_name("about-button")
			header_about.click()
			self.assertEqual(self.driver.current_url, self.url+"about")
		except NoSuchElementException as err:
			self.fail(err.msg)

	def test_header_visualizations_page(self): #check if visualizations page loads from header
		try:
			self.driver.get(self.url)
			header_about = self.driver.find_element_by_class_name("viz-button")
			header_about.click()
			self.assertEqual(self.driver.current_url, self.url+"visualizations")
		except NoSuchElementException as err:
			self.fail(err.msg)

	def test_header_search_page(self): #check if search page loads from header
		try:
			self.driver.get(self.url)
			header_state = self.driver.find_element_by_class_name("search-button")
			header_state.click()
			self.assertEqual(self.driver.current_url, self.url+"search")
		except NoSuchElementException as err:
			self.fail(err.msg)

	def test_header_states_page(self): #check if states page loads from header
		try:
			self.driver.get(self.url)
			header_state = self.driver.find_element_by_class_name("state-button")
			header_state.click()
			self.assertEqual(self.driver.current_url, self.url+"states")
		except NoSuchElementException as err:
			self.fail(err.msg)

	def test_header_counties_page(self): #check if counties page loads from header
		try:
			self.driver.get(self.url)
			header_county = self.driver.find_element_by_class_name("county-button")
			header_county.click()
			self.assertEqual(self.driver.current_url, self.url+"counties")
		except NoSuchElementException as err:
			self.fail(err.msg)

	def test_header_organizations_page(self): #check if organizations page loads from header
		try:
			self.driver.get(self.url)
			header_org = self.driver.find_element_by_class_name("org-button")
			header_org.click()
			self.assertEqual(self.driver.current_url, self.url+"organizations")
		except NoSuchElementException as err:
			self.fail(err.msg)

	''' TESTING SPLASH CARD FUNCTIONALITY '''

	def test_model_page_card_states(self): #check if state model page loads from model card
		try:
			self.driver.get(self.url)
			sp_state = self.driver.find_element_by_class_name("splash-states-card")
			sp_state.click()
			self.assertEqual(self.driver.current_url, self.url+"states")
		except NoSuchElementException as err:
			self.fail(err.msg)

	def test_model_page_card_counties(self): #check if counties model page loads from model card
		try:
			self.driver.get(self.url)
			sp_county = self.driver.find_element_by_class_name("splash-counties-card")
			sp_county.click()
			self.assertEqual(self.driver.current_url, self.url+"counties")
		except NoSuchElementException as err:
			self.fail(err.msg)

	def test_model_page_card_organizations(self): #check if organizations model page loads from model card
		try:
			self.driver.get(self.url)
			sp_org = self.driver.find_element_by_class_name("splash-organizations-card")
			sp_org.click()
			self.assertEqual(self.driver.current_url, self.url+"organizations")
		except NoSuchElementException as err:
			self.fail(err.msg)


# Main Method
if __name__ == '__main__':

    tests = unittest.TestLoader().loadTestsFromTestCase(SeleniumTest)
    assessment = unittest.TextTestRunner(verbosity=2).run(tests)

    print("FAIL") if (len(assessment.failures) + len(assessment.errors) > 0) else print("PASS")
