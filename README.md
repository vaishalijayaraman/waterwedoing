# Water We Doing

 Provides information about water conservation efforts and water quality in the US at a state and county level.

### Website
The project website is www.waterwedoing.me.

## Group Members
| Name  | EID | GitLab Username | 
| ------------- | ------------- | ------------- |
| Andrew Hill  | awh772  | WesternBit | 
| Andrew Kim  | hjk545  | nimbus22  | 
| Antonio De Cesare  | ad42644  | antdece  | 
| Benjamin Carter | blc2858  | bencarter87 | 
| Shane Anderson | sda867 | shanders  | 
| Vaishali Jayaraman | jv27995 | vaishalijayaraman  | 

## Git SHA
| Phase | Git SHA |
| ------------- | ------------- |
| Phase I | e0851ac4f39413135d5dd4b46b5bcc2a65fb385c |
| Phase II | 85557f2a34c2c6dd167a9a228bbf549016a48c95 |
| Phase III | 9d1ff4934e9dcc2cce106622caaa2fba9edef557 |
| Phase IV | ba14a40d33f2005fc2c77ce6feb5cd7e4647aac8 |

## GitLab Pipelines

The GitLab pipelies of the project can be found [here](https://gitlab.com/vaishalijayaraman/waterwedoing/pipelines).

## Completion Times (per member)
| Phase | Estimated Completion Time | Actual Completion Time |
| ------------- | ------------- | ------------- |
| I | 25 hours | 36 hours |
| II | 35 hours | 70 hours |
| III | 30 hours | 60 hours |
| IV | 25 hours | 40 hours |

## Notes
- How to create an HTTP request through Javascript was learned from [here](https://gitlab.com/LuculentDig/energiziousness/blob/master/client/src/pages/about.js).
- How to create cards, how to stylize footer and how to integrate GitLab CI with Docker (.yaml file creation) was learned from [here](https://gitlab.com/joswiatek/catch-me-outside).
- How to create dynamic content was learned from [here](https://medium.com/@schlunzk/creating-a-dynamic-card-layout-in-jsx-react-c366c3b2ecde).
- How to use mocha for JS testing was learned from [here](https://medium.com/@compatt84/how-to-setup-unit-tests-with-create-react-app-mocha-and-visual-studio-code-mocha-side-bar-eb4f75d8de5a) and [here](https://gitlab.com/joswiatek/catch-me-outside/blob/master/frontend/src/App.test.js).
- How to design unittests was learned from [here](https://gitlab.com/CameronEgger/inneedofsoup/blob/master/backend/tests.py).
- How to make Flask application was learned from [here](https://docs.aws.amazon.com/elasticbeanstalk/latest/dg/create-deploy-python-flask.html).
- How to add page buttons from modeled from [here](https://gitlab.com/joswiatek/catch-me-outside/blob/master/frontend/src/App.css).
- How to create and design search, sort and filter components was modeled from [here](https://gitlab.com/joswiatek/catch-me-outside).
- How to use Bar Chart in React with D3 was modeled from [here](frontend/src/pages/Visualizations.js).
- How to use D3 Word Cloud in React was modeled from [here](https://codepen.io/znak/pen/rOgXoV?editors=0010).
- How to create a Heat Map with D3 was modeled from [here](https://codepen.io/trey-davis/pen/pwgBYo?editors=0110). 