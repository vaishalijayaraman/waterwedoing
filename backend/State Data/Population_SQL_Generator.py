import io, os, csv

with open("Population_SQL_Commands.txt", "w") as sql_file:
	with open("CensusData.csv", "r") as census_file:
		reader = csv.DictReader(census_file, delimiter=',')

		for row in reader:
			sql_file.write("UPDATE States\n\tWHERE state_name = '" + row["State"]+ "'\n\tSET population = " + row["Populations"] + ";\n")