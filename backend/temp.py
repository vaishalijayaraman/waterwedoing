import requests, zipfile, io, csv, os
from datetime import datetime
from app import db
from flask import Flask
from config import Config
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate
import numpy as np
from app import app
from app.models import *
import math
import re

counties_in_orgs = {}
states_in_orgs = {}
with open("phone_sql.txt", "w") as file:

	def phone_sql(org_id, phone):
		file.write("Update organizations\n\tset phone = '" + phone + "'\n\t where org_identifier = '" + org_id + "';\n")

	def ph_vals_state(ph, state_name):
		file_1.write("Update States\n\tSET ph = '" + str(ph) + "'\n\tWHERE state_name = '" + str(state_name) + "';\n")

	def ph_vals_county(ph, county_name):
		file_2.write("Update Counties\n\tSET ph = '" + str(ph) + "'\n\tWHERE county_name = '" + str(county_name) + "';\n")

	def counties_in_states_sql(counties_in_states, state_name):
		file.write("Update States\n\tSET counties = '" + str(counties_in_states).replace("'$", '$').replace("$'", "$").replace('"$', '$').replace('$"', '$').replace("$, ", "$,").replace("'", "''") + "'\n\tWHERE state_name = '" + str(state_name) + "';\n")

	def states_in_orgs_sql(states_in_orgs):
		for org, states in states_in_orgs.items():
			file.write("UPDATE Organizations\n\tSET states = '" + str(states).replace('{', '[').replace('}', ']').replace("'$", '$').replace("$'", "$").replace('"$', '$').replace('$"', '$').replace("$, ", "$,").replace("'", "''") + "'\n\tWHERE org_identifier = '" + str(org) + "';\n")

	def counties_in_orgs_sql(counties_in_orgs):
		for org, counties in counties_in_orgs.items():
			file.write("UPDATE Organizations\n\tSET counties = '" + str(counties).replace('{', '[').replace('}', ']').replace("'$", '$').replace("$'", "$").replace('"$', '$').replace('$"', '$').replace("$, ", "$,").replace("'", "''") + "'\n\tWHERE org_identifier = '" + str(org) + "';\n")

	def turbidity_in_county(county_name, turbidity_county):
		turb_county.write("Update Counties\n\tSET turbidity = '" + str(turbidity_county) + "'\n\tWHERE county_name = '" + str(county_name) + "';\n")

	def turbidity_in_state(state_name, turb):
		turb_state.write("Update States\n\tSET turbidity = '" + str(turb) + "'\n\tWHERE state_name = '" + str(state_name) + "';\n")

	def generate_orgs_in_state_SQL(orgs_in_state, state_name):
		orgs_in_state_SQL_commands.write("Update States\n\tSET organizations = '" + str(orgs_in_state).replace('{', '[').replace('}', ']').replace("'$", '[$').replace("$'", "$]").replace('"$', '[$').replace('$"', '$]').replace("], ", "],").replace("'", "''") + "'\n\tWHERE state_name = '" + state_name + "';\n")

	states_in_org = dict()
	def orgs_in_counties(county_orgs, county_name):
		file.write("UPDATE Counties\n\tSET organizations = '" + str(county_orgs).replace('{', '[').replace('}', ']').replace("'$", '[$').replace("$'", "$]").replace('"$', '[$').replace('$"', '$]').replace("], ", "],").replace("'", "''") + "'\n\tWHERE county_name = '" + county_name + "';\n")

	with open("State_and_County_Codes/FIPS_State_Codes.csv", "r") as states_csv, open("State_and_County_Codes/FIPS_County_Codes.csv", "r") as county_csv:
		states_file = csv.DictReader(states_csv, delimiter=',')
		states_names = []
		states_codes = []
		states_abbrevs = []
		for states in states_file:
			states_names.append(states["Name"])
			states_codes.append(states["FIPS"])
			states_abbrevs.append(states["State"])
		county_file = csv.DictReader(county_csv, delimiter=',')
		county_iter = iter(county_file)
		all_orgs = set()
		

		for x in range(0, 50):
			state_code = states_codes[x]			
			abbreviation = states_abbrevs[x]
			state_name = states_names[x]
			print(state_name)
			turbs = []
			orgs_in_state = set()
			counties_in_state = []
			turbidity_total = 0
			turbidity_count = 0
			ph_total = 0
			ph_count = 0


			i = 0
			for county in county_iter:
				if i < 10 and str(county["State"]) == str(abbreviation):
					county_code = int(county["FIPS"]) % 1000
					county_tracker = int(county["FIPS"])
					county_name = county["Name"] + ', ' + state_name
					print(county_name)
					counties_in_state.append( '$' + county_name + '$')
					with open('county_data/' +str(county_tracker)+ '_result.csv', "r") as csv_result:
						final_csv = csv.DictReader(csv_result, delimiter=',')
						turbs_in_counties = []
						organizations = set()
						county_turbidity_total = 0
						county_turbidity_count = 0
						county_ph_total = 0
						county_ph_count = 0

						for row in final_csv:
							organizations.add(row["OrganizationIdentifier"])

							if row["CharacteristicName"].upper() == 'PH':
								try:
									curr_ph = float(row["ResultMeasureValue"])
									if 0.0 <= curr_ph <= 14.0:
										county_ph_total += float(row["ResultMeasureValue"])
										ph_total += float(row["ResultMeasureValue"])
										county_ph_count += 1
										ph_count += 1
								except ValueError:
									pass

							if row["CharacteristicName"] == "Turbidity" and (row["ResultMeasure/MeasureUnitCode"].upper() == "NTU" or row["ResultMeasure/MeasureUnitCode"].upper() == "JTU" or row["ResultMeasure/MeasureUnitCode"].upper() == "FTU"):
								try:
									curr_turb = float(row["ResultMeasureValue"])
									if not math.isnan(curr_turb):
										if curr_turb < 0.0:
											curr_turb = 0.0
										turbs_in_counties.append(curr_turb)
								except ValueError:
									pass
							if row["OrganizationIdentifier"] not in states_in_org:
								states_in_org[row["OrganizationIdentifier"]] = set([state_name])
							else:
								states_in_org[row["OrganizationIdentifier"]] |= set([state_name])	
						
						turbidity_county = -1
						try:
							if len(turbs_in_counties) != 0:
								turbidity_county = np.mean(turbs_in_counties, dtype=np.float64)
								turbs.append(turbidity_county)
						except:
							pass

						ph_county = -1
						try:
							ph_county = county_ph_total/county_ph_count
						except:
							pass

						#ph_vals_county(ph_county, county_name)

						county_orgs = set()
						counties_in_org = set()
						with open("org_data/" +str(county_tracker)+"_organization.csv", "r") as org_csv:
							org_data = csv.DictReader(org_csv, delimiter=',')
							line_count = 0
							for org in org_data:
								if org["OrganizationIdentifier"] in organizations:
									print(org["OrganizationFormalName"])
									if org["OrganizationIdentifier"] in all_orgs:
										#county_orgs.append(str(org["OrganizationFormalName"] + ', ' + org["OrganizationIdentifier"]))
										county_orgs.add('$' + org["OrganizationFormalName"] + '$,$' + org["OrganizationIdentifier"] + '$')
										curr_states = states_in_orgs[org["OrganizationIdentifier"]]
										curr_states.add('$'+state_name+'$')
										states_in_orgs[org["OrganizationIdentifier"]] = curr_states
										old_set = counties_in_orgs[org["OrganizationIdentifier"]]
										old_set.add( '$'+county_name+'$')
										counties_in_orgs[org["OrganizationIdentifier"]] = old_set
										orgs_in_state.add('$' + org["OrganizationFormalName"] + '$,$' + org["OrganizationIdentifier"] + '$')
										
									else:
										all_orgs.add(org["OrganizationIdentifier"])
										phone_str = ''
										try:
											phone_str = org["Telephonic"]
											phone_list = re.split('x', phone_str)
											phone_str = phone_list[0]
											phone_str = re.sub("\D", '', phone_str)
											if len(phone_str) > 10:
												phone_str = phone_str[-10:]
										except:
											pass

										phone_sql(org["OrganizationIdentifier"], phone_str)
										
										orgs_in_state.add('$' + org["OrganizationFormalName"] + '$,$' + org["OrganizationIdentifier"] + '$')
										
										org_state = org["OrganizationAddress/StateCode_1"]

										for state_org in states_file:
											if org_state == state_org["State"]:
												org_state = state_org["Name"]
												break
										else:
											org_state = "not found"
										curr_state = set()
										curr_state.add('$' + state_name + '$')
										states_in_orgs.update({org["OrganizationIdentifier"] : curr_state})
										county_in_org = set()
										county_in_org.add('$' + county_name + '$')
										counties_in_orgs.update({org["OrganizationIdentifier"] : county_in_org})
										county_orgs.add('$' + org["OrganizationFormalName"] + '$,$' + org["OrganizationIdentifier"] + '$')
							
							#turbidity_in_county(county_name, turbidity_county)
							#orgs_in_counties(county_orgs, county_name)

				else:
					break
				i+=1

			ph_val = ph_total/ph_count
			#ph_vals_state(ph_val, state_name)
			#counties_in_states_sql(counties_in_state, state_name)
			#add_county_for_state(state_name, counties_in_state)
			turb = np.mean(turbs, dtype=np.float64)
			#turbidity_in_state(state_name, turb)
			#generate_orgs_in_state_SQL(orgs_in_state, state_name)						
			
			while str(next(county_iter)["State"]) == str(abbreviation):
				continue

	#states_in_orgs_sql(states_in_orgs)
	#counties_in_orgs_sql(counties_in_orgs)
#This shouldn't actually do anything because edit is supposed to be instantaneous. In theory.
#db.session.commit()
