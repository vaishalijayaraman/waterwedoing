import requests

def update_date_code(date_code):
    number_form = int(date_code, 10)
    number_form -= 1

    if number_form < 0:
        raise UnboundLocalError
    elif number_form < 10:
        date_code = "0" + str(number_form)
    else:
        date_code = str(number_form)

    return date_code

def twodig(num):
    if num < 0:
        raise UnboundLocalError
    elif num < 10:
        num = "0" + str(num)
    else:
        num = str(num)

    return num

"""
test_req_str = "https://api.census.gov/data/2018/pep/charagegroups?get=POP,GEONAME&for=state:08&DATE_CODE=10"
results = requests.get(test_req_str)

print(results)
print(type(results))
print(results.json())
print(results.json()[1][0])
"""

print("State, Populations")

exclusions = [3, 7, 14, 43, 52]

for state_code in range(1, 57):
    if state_code not in exclusions:
        results = None
        date_code = '13'

        while(results == None and (str(date_code) != "00")):
            request_string = "https://api.census.gov/data/2018/pep/charagegroups?get=POP,GEONAME&for=state:" + twodig(state_code) + "&DATE_CODE=" + date_code
            #print(request_string)
            date_code = update_date_code(date_code)
            results = requests.get(request_string)
            
            try:
                results = results.json()
            except:
                results = None

            #print(results)
        print(results[1][1] +"," + results[1][0])
        


