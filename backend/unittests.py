# we modeled these tests after https://gitlab.com/CameronEgger/inneedofsoup/blob/master/backend/tests.py

import unittest
import application
from application import models
import requests

class WaterWeDoingUnitTests(unittest.TestCase):

	def test_state_model(self): 
		s = models.States
		assert hasattr(s, "__dict__")
		db_info = s.__dict__
		assert "__tablename__" in db_info
		assert "__table__" in db_info
		db_table = db_info["__table__"]
		db_table = eval(str(db_table._columns))
		assert db_table == [
		"states.state_name",
		"states.population",
		"states.sodium",
		"states.turbidity",
		"states.nitrate",
		"states.ph",
		"states.state_flag_url",
		"states.state_svg_url",
		"states.organizations",
		"states.counties"]

	def test_county_model(self): 
		c = models.Counties
		assert hasattr(c, "__dict__")
		db_info = c.__dict__
		assert "__tablename__" in db_info
		assert "__table__" in db_info
		db_table = db_info["__table__"]
		db_table = eval(str(db_table._columns))
		assert db_table == [
		"counties.county_name",
		"counties.ph",
		"counties.sodium",
		"counties.turbidity",
		"counties.nitrate",
		"counties.old_date",
		"counties.new_date",
		"counties.state",
		"counties.county_svg_url",
		"counties.county_thumb_url",
		"counties.organizations"]

	def test_organization_model(self): 
		o = models.Organizations
		assert hasattr(o, "__dict__")
		db_info = o.__dict__
		assert "__tablename__" in db_info
		assert "__table__" in db_info
		db_table = db_info["__table__"]
		db_table = eval(str(db_table._columns))
		assert db_table == [
		"organizations.org_identifier",
		"organizations.org_name",
		"organizations.org_type",
		"organizations.city",
		"organizations.states",
		"organizations.counties",
		"organizations.email",
		"organizations.phone",
		"organizations.address",
		"organizations.org_image_url"]

	def test_add_cors_headers(self):  # test5
		func = application.add_cors_headers
		class temp:
			def __init__(self):
				self.headers = {}
		response = temp()
		assert "Access-Control-Allow-Origin" not in response.headers
		assert "Access-Control-Allow-Credentials" not in response.headers
		response = func(response)
		assert (
			"Access-Control-Allow-Origin" in response.headers
			and response.headers["Access-Control-Allow-Origin"] == "*"
		)
		assert (
			"Access-Control-Allow-Credentials" in response.headers
			and response.headers["Access-Control-Allow-Credentials"] == "true"
		)

	def test_api(self):
		r = requests.get("https://api.waterwedoing.me/debug")
		assert(r.json()["status"] == "success")

if __name__ == '__main__':
	unittest.main()
