import requests
import json

issue_counts = {"Andrew Hill":0, "Andrew Kim":0, "Antonio De Cesare":0, "Benjamin Carter":0, 
	"Shane Anders":0, "Vaishali Jayaraman":0}
commit_counts = {"Andrew Hill":0, "Andrew Kim":0, "Antonio De Cesare":0, 
	"Benjamin Carter":0, "Shane Anders":0, "Vaishali Jayaraman":0}
test_counts = {"Andrew Hill":0, "Andrew Kim":0, "Antonio De Cesare":0, 
	"Benjamin Carter":0, "Shane Anders":0, "Vaishali Jayaraman":0}
names = {"Andrew Westbrook Hill":"Andrew Hill", "WesternBit":"Andrew Hill", "Andrew Kim":"Andrew Kim", 
	"Hyun Kim":"Andrew Kim", "Antonio De Cesare":"Antonio De Cesare", "Benjamin Carter":"Benjamin Carter", 
	"bencarter87":"Benjamin Carter", "Shane Anders":"Shane Anders", "shanders":"Shane Anders", 
	"Vaishali Jayaraman":"Vaishali Jayaraman"}


def count_issues():
	url = "https://gitlab.com/api/v4/projects/12978758/issues?state=closed"
	response = requests.get(url)
	issues = json.loads(response.text)

	for issue in issues:
		closer = issue["closed_by"]["name"]
		name = names[closer]
		issue_counts[name] += 1
		


def count_commits():
	#have to get commits from each branch
	url_dev_commits = "https://gitlab.com/api/v4/projects/12978758/repository/commits?ref_name=development"
	url_master_commits = "https://gitlab.com/api/v4/projects/12978758/repository/commits"

	response_dev = requests.get(url_dev_commits)
	response_master = requests.get(url_master_commits)

	dev_commits = json.loads(response_dev.text)
	master_commits = json.loads(response_master.text)

	for commit in dev_commits:
		author = commit["author_name"]
		name = names[author]
		commit_counts[name] += 1
		

	for commit2 in master_commits:
		author2 = commit2["author_name"]
		name2 = names[author2]
		commit_counts[name2] += 1
	

if __name__ == "__main__" :
	count_issues()
	count_commits()
	f = open("GitLabDetails.txt", "w")
	# NAME
	# COMMITS
	# ISSUES
	# TESTS
	for name in issue_counts.keys():
		print(name)
		print(str(commit_counts[name]))
		print(str(issue_counts[name]))
		print(str(test_counts[name]))

