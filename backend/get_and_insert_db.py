import requests, zipfile, io, csv, os
from datetime import datetime
from app import db
from flask import Flask
from config import Config
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate
from app import app
from app.models import *

with open("State_and_County_Codes/FIPS_State_Codes.csv", "r") as states_csv, open("State_and_County_Codes/FIPS_County_Codes.csv", "r") as county_csv:
	states_file = csv.DictReader(states_csv, delimiter=',')
	states_names = []
	states_codes = []
	states_abbrevs = []
	for states in states_file:
		states_names.append(states["Name"])
		states_codes.append(states["FIPS"])
		states_abbrevs.append(states["State"])
	county_file = csv.DictReader(county_csv, delimiter=',')
	county_iter = iter(county_file)
	all_orgs = set()

	for x in range(0, 50):
		state_code = states_codes[x]			
		abbreviation = states_abbrevs[x]
		state_name = states_names[x]
		print(state_name)
		
		orgs_in_state = set()
		all_counties = set()
		states_in_org = dict()

		sodium_total = 0
		sodium_count = 0
		turbidity_total = 0
		turbidity_count = 0
		ph_total = 0
		ph_count = 0
		nitrate_total = 0
		nitrate_count = 0

		i = 0
		for county in county_iter:
			if i < 10 and str(county["State"]) == str(abbreviation):
				county_code = int(county["FIPS"]) % 1000
				county_tracker = int(county["FIPS"])
				county_name = county["Name"] + ', ' + state_name
				print(county_name)
				all_counties.add(county_name)
				result_zipped = requests.get(f"https://www.waterqualitydata.us/data/Result/search?countrycode=US&statecode=US%3A{str(state_code).zfill(2)}&countycode=US%3A{str(state_code).zfill(2)}%3A{str(county_code).zfill(3)}&characteristicName=pH&characteristicName=Turbidity&characteristicName=Nitrate&characteristicName=Sodium&mimeType=csv&zip=yes")
				try:
					result = zipfile.ZipFile(io.BytesIO(result_zipped.content))
					result.extractall()
				except:
					i -= 1
				else:
					os.rename("result.csv", "county_data/" +str(county_tracker)+ "_result.csv")

					with open('county_data/' +str(county_tracker)+ '_result.csv', "r") as csv_result:
						final_csv = csv.DictReader(csv_result, delimiter=',')

						organizations = set()
						old_date = datetime.max
						new_date = datetime.min
						county_pH_total = 0
						county_pH_count = 0
						county_turbidity_total = 0
						county_turbidity_count = 0
						county_sodium_total = 0
						county_sodium_count = 0
						county_nitrate_total = 0
						county_nitrate_count = 0

						for row in final_csv:
							organizations.add(row["OrganizationIdentifier"])

							if states_in_org[row["OrganizationIdentifier"]] == None:
								states_in_org[row["OrganizationIdentifier"]] = {state_name}
							else:
								states_in_org[row["OrganizationIdentifier"]] = states_in_org[row["OrganizationIdentifier"]] + ", " + state_name

							if row["ActivityStartDate"] != "########":
								try:
									curr_date = datetime.strptime(row["ActivityStartDate"], '%Y-%m-%d')
								except ValueError:
									pass
								else:
									if curr_date < old_date:
										old_date = curr_date
									if curr_date > new_date:
										new_date = curr_date
							if row["CharacteristicName"] == "pH":
								try:
									county_pH_total += float(row["ResultMeasureValue"])
									ph_total += float(row["ResultMeasureValue"])
								except ValueError:
									pass
								else:
									county_pH_count += 1
									ph_count += 1
							if row["CharacteristicName"] == "Turbidity" and (row["ResultMeasure/MeasureUnitCode"].upper() == "NTU" or row["ResultMeasure/MeasureUnitCode"].upper() == "JTU" or row["ResultMeasure/MeasureUnitCode"].upper() == "FTU"):
								try:
									county_turbidity_total += float(row["ResultMeasureValue"])
									turbidity_total += float(row["ResultMeasureValue"])
								except ValueError:
									pass
								else:
									county_turbidity_count += 1
									turbidity_count += 1
							if row["CharacteristicName"] == "Sodium" and row["ResultMeasure/MeasureUnitCode"] == "mg/l":
								try:
									county_sodium_total += float(row["ResultMeasureValue"])
									sodium_total += float(row["ResultMeasureValue"])
								except ValueError:
									pass
								else:
									county_sodium_count += 1
									sodium_count += 1
							if row["CharacteristicName"] == "Nitrate" and row["ResultMeasure/MeasureUnitCode"] == "mg/l as N" or row["ResultMeasure/MeasureUnitCode"] == "mg/l":
								try:
									county_nitrate_total += float(row["ResultMeasureValue"])
									nitrate_total += float(row["ResultMeasureValue"])
								except ValueError:
									pass
								else:
									county_nitrate_count += 1
									nitrate_count += 1

						ph_county = -1
						try:
							ph_county = county_pH_total/county_pH_count
						except:
							pass
						turbidity_county = -1
						try:
							turbidity_county = county_turbidity_total/county_turbidity_count
						except:
							pass
						nitrate_county = -1
						try:
							nitrate_county = county_nitrate_total/county_nitrate_count
						except:
							pass
						sodium_county = -1
						try:
							sodium_county = county_sodium_total/county_sodium_count
						except:
							pass

						new_county = Counties(county_name=county_name, ph=ph_county, sodium=sodium_county, turbidity=turbidity_county, nitrate=nitrate_county, old_date=old_date, new_date=new_date, state=state_name)
						db.session.add(new_county)
						county_orgs = []
						orgs_zipped = requests.get(f"https://www.waterqualitydata.us/data/Organization/search?countrycode=US&statecode=US%3A{str(state_code).zfill(2)}&countycode=US%3A{str(state_code).zfill(2)}%3A{str(county_code).zfill(3)}&characteristicName=pH&characteristicName=Turbidity&characteristicName=Nitrate&characteristicName=Sodium&mimeType=csv&zip=yes")
						try:
							orgs_closed = zipfile.ZipFile(io.BytesIO(orgs_zipped.content))
							orgs_closed.extractall()
						except:
							pass
						else:
							os.rename("organization.csv", "org_data/" +str(county_tracker)+"_organization.csv")

							with open("org_data/" +str(county_tracker)+"_organization.csv", "r") as org_csv:
								org_data = csv.DictReader(org_csv, delimiter=',')
								line_count = 0
								for org in org_data:
									if org["OrganizationIdentifier"] in organizations:
										print(org["OrganizationFormalName"])
										if org["OrganizationIdentifier"] in all_orgs:
											curr_org = Organizations.query.get(org["OrganizationIdentifier"])
											curr_org.counties += ", " + county_name
											county_orgs.append(str(org["OrganizationFormalName"] + ' ' + org["OrganizationIdentifier"]))
										else:
											all_orgs.add(org["OrganizationIdentifier"])
											orgs_in_state.add(str(org["OrganizationFormalName"] + ' ' + org["OrganizationIdentifier"]))
											formal_name = org["OrganizationFormalName"]
											identifier = org["OrganizationIdentifier"]
											org_type = org["OrganizationType"]
											email = org["ElectronicAddress"]
											phone = org["Telephonic"]
											address = org["OrganizationAddress/AddressText_1"]
											city = org["OrganizationAddress/LocalityName_1"]
											org_state = org["OrganizationAddress/StateCode_1"]

											for state_org in states_file:
												if org_state == state_org["State"]:
													org_state = state_org["Name"]
													break
											else:
												org_state = "not found"
											new_org = Organizations(org_identifier=identifier, org_name=formal_name, org_type=org_type, city=city, state=org_state, counties=county_name, email=email, phone=phone, address=address)
											db.session.add(new_org)
											county_orgs.append(org["OrganizationFormalName"] + org["OrganizationIdentifier"])

								new_county.organizations = str(county_orgs)


			else:
				break
			i+=1
		
		ph_state = -1
		try:
			ph_state = ph_total/ph_count
		except:
			pass
		turbidity_state = -1
		try:
			turbidity_state = turbidity_total/turbidity_count
		except:
			pass
		nitrate_state = -1
		try:
			nitrate_state = nitrate_total/nitrate_count
		except:
			pass
		sodium_state = -1
		try:
			sodium_state = sodium_total/sodium_count
		except:
			pass

		new_state = States(state_name=state_name, sodium=sodium_state, turbidity=turbidity_state, nitrate=nitrate_state, ph=ph_state, organizations=str(orgs_in_state))
		db.session.add(new_state)

		db.session.commit()
		
		while str(next(county_iter)["State"]) == str(abbreviation):
			continue


db.session.commit()

def generate_states_in_org_SQL(given_states_in_org):
	with open("states_in_organizations_SQL_commands.txt", "w") as states_in_org_SQL_commands:
		for org_identifier, states in given_states_in_org:
			states_in_org_SQL_commands.write("ALTER TABLE Organizations\n\tALTER {0}.state\n\tSET \'{1}\';\n".format(org_identifier, states))


def edit_states_in_org(given_states_in_org):
	
	for org_identifier, states in given_states_in_org:
		query = db.session.query(org_identifier).filter(Organizations.org_identifier == org_identifier)
		org_result = query.first()

		org_result.state = states


generate_states_in_org_SQL(states_in_org)

edit_states_in_org(states_in_org)

#This shouldn't actually do anything because edit is supposed to be instantaneous. In theory.
db.commit()
