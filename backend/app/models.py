from app import db

class States(db.Model):
	state_name = db.Column(db.Text, primary_key=True)
	population = db.Column(db.Float)
	sodium = db.Column(db.Float)
	turbidity = db.Column(db.Float)
	nitrate = db.Column(db.Float)
	ph = db.Column(db.Float)
	state_flag_url = db.Column(db.Text)
	state_svg_url = db.Column(db.Text)
	organizations = db.Column(db.Text)
	counties = db.Column(db.Text)
	
	def __repr__(self):
		return '<State {}>'.format(self.state_name)


class Counties(db.Model):
	county_name = db.Column(db.Text, primary_key=True)
	ph = db.Column(db.Float)
	sodium = db.Column(db.Float)
	turbidity = db.Column(db.Float)
	nitrate = db.Column(db.Float)
	old_date = db.Column(db.DateTime)
	new_date = db.Column(db.DateTime)
	state = db.Column(db.Text)
	county_svg_url = db.Column(db.Text)
	county_thumb_url = db.Column(db.Text)
	organizations = db.Column(db.Text)
	
	def __repr__(self):
		return '<County {}>'.format(self.county_name)


class Organizations(db.Model):
	org_identifier = db.Column(db.Text, primary_key=True)
	org_name = db.Column(db.Text)
	org_type = db.Column(db.Text)
	city = db.Column(db.Text)
	states = db.Column(db.Text)
	counties = db.Column(db.Text)
	email = db.Column(db.Text)
	phone = db.Column(db.Text)
	address = db.Column(db.Text)
	org_image_url = db.Column(db.Text)

	def __repr__(self):
		return '<Organization {}>'.format(self.org_name)
