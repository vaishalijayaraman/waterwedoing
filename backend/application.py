# template taken from https://docs.aws.amazon.com/elasticbeanstalk/latest/dg/create-deploy-python-flask.html

from flask import Flask, jsonify, request, render_template
import requests
import json
import os
import time
import sys
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate
from config import Config
import flask_restless
from elasticsearch import Elasticsearch, exceptions, helpers

application = Flask(__name__)
application.config.from_object(Config)
db = SQLAlchemy(application)
migrate = Migrate(application, db)

es = Elasticsearch(host="es")


class models:
    class States(db.Model):
        state_name = db.Column(db.Text, primary_key=True)
        population = db.Column(db.Float)
        sodium = db.Column(db.Float)
        turbidity = db.Column(db.Float)
        nitrate = db.Column(db.Float)
        ph = db.Column(db.Float)
        state_flag_url = db.Column(db.Text)
        state_svg_url = db.Column(db.Text)
        organizations = db.Column(db.Text)
        counties = db.Column(db.Text)

        def __repr__(self):
            return "<State {}>".format(self.state_name)

    class Counties(db.Model):
        county_name = db.Column(db.Text, primary_key=True)
        ph = db.Column(db.Float)
        sodium = db.Column(db.Float)
        turbidity = db.Column(db.Float)
        nitrate = db.Column(db.Float)
        old_date = db.Column(db.DateTime)
        new_date = db.Column(db.DateTime)
        state = db.Column(db.Text)
        county_svg_url = db.Column(db.Text)
        county_thumb_url = db.Column(db.Text)
        organizations = db.Column(db.Text)

        def __repr__(self):
            return "<County {}>".format(self.county_name)

    class Organizations(db.Model):
        org_identifier = db.Column(db.Text, primary_key=True)
        org_name = db.Column(db.Text)
        org_type = db.Column(db.Text)
        city = db.Column(db.Text)
        states = db.Column(db.Text)
        counties = db.Column(db.Text)
        email = db.Column(db.Text)
        phone = db.Column(db.Text)
        address = db.Column(db.Text)
        org_image_url = db.Column(db.Text)

        def __repr__(self):
            return "<Organization {}>".format(self.org_name)


def add_cors_headers(response):
    response.headers["Access-Control-Allow-Origin"] = "*"
    response.headers["Access-Control-Allow-Credentials"] = "true"
    response.headers["Access-Control-Allow-Headers"] = "Content-Type"
    return response


application.after_request(add_cors_headers)

manager = flask_restless.APIManager(application, flask_sqlalchemy_db=db)

manager.create_api(
    models.States,
    methods=["GET"],
    allow_delete_many=True,
    max_results_per_page=500,
    results_per_page=9,
    url_prefix="/",
)
manager.create_api(
    models.Counties,
    methods=["GET"],
    allow_delete_many=True,
    max_results_per_page=500,
    results_per_page=9,
    url_prefix="/",
)
manager.create_api(
    models.Organizations,
    methods=["GET"],
    allow_delete_many=True,
    max_results_per_page=500,
    results_per_page=9,
    url_prefix="/",
)


def load_data_in_es():
    """ creates an index in elasticsearch """
    models = ["states", "counties", "organizations"]
    doc_type = {
        "states": "state_instance",
        "counties": "county_instance",
        "organizations": "organization_instance",
    }

    for model in models:
        url = "{}{}{}".format(
            "https://api.waterwedoing.me/", model, "?results_per_page=500"
        )
        r = requests.get(url)
        data = r.json()["objects"]
        for id, instance in enumerate(data):
            res = es.index(index=model, doc_type=doc_type[model], id=id, body=instance)
        print("Total {} loaded: ".format(model), len(data))


def safe_check_index(index, retry=3):
    """ connect to ES with retry """
    if not retry:
        print("Out of retries. Bailing out...")
        sys.exit(1)
    try:
        status = es.indices.exists(index)
        return status
    except exceptions.ConnectionError as e:
        print("Unable to connect to ES. Retrying in 5 secs...")
        time.sleep(5)
        safe_check_index(index, retry - 1)


@application.route("/search")
def search():
    customquery = request.args.get("q")

    if not customquery:
        return jsonify({"status": "failure", "msg": "No query provided"})
    try:
        search_result = es.search(index="_all", body=json.loads(customquery))
        return str(json.dumps(search_result))
    except Exception as e:
        return jsonify(
            {
                "status": "failure",
                "msg": str(
                    "error in reaching elasticsearch: "
                    + str(e)
                    + ", custom query was: "
                    + customquery
                ),
            }
        )


@application.route("/organizations/search")
def organizations_search():
    customquery = request.args.get("q")

    if not customquery:
        return jsonify({"status": "failure", "msg": "No query provided"})
    try:
        search_result = es.search(index="organizations", body=json.loads(customquery))
        return str(json.dumps(search_result))
    except Exception as e:
        return jsonify(
            {
                "status": "failure",
                "msg": str(
                    "error in reaching elasticsearch: "
                    + str(e)
                    + ", custom query was: "
                    + customquery
                ),
            }
        )


@application.route("/counties/search")
def counties_search():
    customquery = request.args.get("q")

    if not customquery:
        return jsonify({"status": "failure", "msg": "No query provided"})
    try:
        search_result = es.search(index="counties", body=json.loads(customquery))
        return str(json.dumps(search_result))
    except Exception as e:
        return jsonify(
            {
                "status": "failure",
                "msg": str(
                    "error in reaching elasticsearch: "
                    + str(e)
                    + ", custom query was: "
                    + customquery
                ),
            }
        )


@application.route("/states/search")
def states_search():
    customquery = request.args.get("q")

    if not customquery:
        return jsonify({"status": "failure", "msg": "No query provided"})
    try:
        search_result = es.search(index="states", body=json.loads(customquery))
        return str(json.dumps(search_result))
    except Exception as e:
        return jsonify(
            {
                "status": "failure",
                "msg": str(
                    "error in reaching elasticsearch: "
                    + str(e)
                    + ", custom query was: "
                    + customquery
                ),
            }
        )


def check_and_load_index():
    """ checks if index exits and loads the data accordingly """
    if (
        not safe_check_index("states")
        or not safe_check_index("counties")
        or not safe_check_index("organizations")
    ):
        print("Index not found...")
        load_data_in_es()


@application.route("/")
def index():
    return render_template("index.html")


@application.route("/debug")
def test_es():
    resp = {}
    try:
        msg = es.cat.indices()
        resp["msg"] = msg
        resp["status"] = "success"
    except:
        resp["status"] = "failure"
        resp["msg"] = "Unable to reach ES"
    return jsonify(resp)


# run the app.
if __name__ == "__main__":
    # Setting debug to True enables debug output. This line should be
    # removed before deploying a production app.
    application.debug = False
    check_and_load_index()

    port = int(os.environ.get("PORT", 5000))
    application.run(host="0.0.0.0", port=port)
