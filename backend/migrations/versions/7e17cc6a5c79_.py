"""empty message

Revision ID: 7e17cc6a5c79
Revises: 
Create Date: 2019-07-06 16:27:37.729981

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '7e17cc6a5c79'
down_revision = None
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.create_table('state',
    sa.Column('state_name', sa.Text(), nullable=False),
    sa.Column('public_lake', sa.Float(), nullable=True),
    sa.Column('sodium', sa.Float(), nullable=True),
    sa.Column('turbidity', sa.Float(), nullable=True),
    sa.Column('nitrate', sa.Float(), nullable=True),
    sa.Column('ph', sa.Float(), nullable=True),
    sa.Column('state_flag_url', sa.Text(), nullable=True),
    sa.Column('state_svg_url', sa.Text(), nullable=True),
    sa.PrimaryKeyConstraint('state_name')
    )
    op.create_table('county',
    sa.Column('county_name', sa.Text(), nullable=False),
    sa.Column('ph', sa.Float(), nullable=True),
    sa.Column('sodium', sa.Float(), nullable=True),
    sa.Column('turbidity', sa.Float(), nullable=True),
    sa.Column('nitrate', sa.Float(), nullable=True),
    sa.Column('old_date', sa.DateTime(), nullable=True),
    sa.Column('new_date', sa.DateTime(), nullable=True),
    sa.Column('state', sa.Text(), nullable=True),
    sa.Column('county_svg_url', sa.Text(), nullable=True),
    sa.ForeignKeyConstraint(['state'], ['state.state_name'], ),
    sa.PrimaryKeyConstraint('county_name')
    )
    op.create_table('organization',
    sa.Column('org_identifier', sa.Text(), nullable=False),
    sa.Column('org_name', sa.Text(), nullable=True),
    sa.Column('org_type', sa.Text(), nullable=True),
    sa.Column('city', sa.Text(), nullable=True),
    sa.Column('state', sa.Text(), nullable=True),
    sa.Column('email', sa.Text(), nullable=True),
    sa.Column('phone', sa.Text(), nullable=True),
    sa.Column('address', sa.Text(), nullable=True),
    sa.Column('org_image_url', sa.Text(), nullable=True),
    sa.ForeignKeyConstraint(['state'], ['state.state_name'], ),
    sa.PrimaryKeyConstraint('org_identifier'),
    sa.UniqueConstraint('address'),
    sa.UniqueConstraint('email'),
    sa.UniqueConstraint('phone')
    )
    op.create_foreign_key(None, 'counties', 'county', ['county_name'], ['county_name'])
    op.create_foreign_key(None, 'counties', 'organization', ['org_identifier'], ['org_identifier'])
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_constraint(None, 'counties', type_='foreignkey')
    op.drop_constraint(None, 'counties', type_='foreignkey')
    op.drop_table('organization')
    op.drop_table('county')
    op.drop_table('state')
    # ### end Alembic commands ###
