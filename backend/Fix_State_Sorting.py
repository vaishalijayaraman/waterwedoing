import requests
import json
import os
import sys

def run_fix_state_sorting():
    
    #API_LINK = "0.0.0.0:9200"
    API_LINK2 = "es"

    query_data = {
        "mappings": {
            "states": {
                "properties": {
                    "state_name": { 
                        "type": "text",
                        "fields": {
                            "raw": { 
                                "type":     "text",
                                "index":	"not_analyzed"
                            }
                        }
                    }
                }
            }
        }
    }

    try:
        #r = requests.put(API_LINK, data = query_data)
        r2 = requests.put(API_LINK2, data = query_data)
    except Exception as e:
        print("failure:\t" + str(e))
    else:
        print("status_code" + str(r2.status_code))
        print("history:\t" + str(r2.history))
        print(str(json.dumps(r2)))

    
