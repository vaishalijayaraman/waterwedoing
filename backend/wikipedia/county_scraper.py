import io, csv

with open('countythumbsql.txt', 'w') as sql_file:
	with open('countythumbs.csv', 'r') as thumbs_csv:
		thumb = csv.DictReader(thumbs_csv, delimiter=',')
		for row in thumb:
			sql_file.write("UPDATE Counties\n\tSET county_thumb_url = '" + row["URL"] + "'\n\tWHERE county_name = '" + row["County"] + ',' + row["State"] + "';\n")
