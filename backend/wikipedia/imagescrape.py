#!/usr/bin/python3

import wikipedia
import fnmatch
import random

def main() :

    countynames = open("counties.csv", "rt")
    wildstart = "https://upload.wikimedia.org/wikipedia/commons/?/??/Map_of_"
    wildmid = "_highlighting_"
    wildend = "_*.*"

    urls = []

    for line in countynames :

        try:

            print(line)

            tokens = line.split(",")

            wikipage = None
            try:
                wikipage = wikipedia.page(tokens[0] + " County, " + tokens[1])
            except wikipedia.DisambiguationError as e:
                print("Was ambiguous!!!")
                s = None
                for option in e.options:
                    if tokens[1] in option:
                        s = option
                        print("Chose " + s)
                        break
                if(s is None):
                    print("No good match, choosing randomly...")
                    s = random.choice(e.options)
                    print("Chose " + s)

                wikipage = wikipedia.page(s)

            wildurl = wildstart + tokens[1].strip().replace(" ", "_") + wildmid + tokens[0].strip().replace(" ", "_") + wildend

            print("Match string: " + wildurl)

            for s in wikipage.images :
                print("image: " + s)
                slist = []
                slist.append(s)
                if len(fnmatch.filter(slist, wildurl)) == 1 :
                    print("match!")
                    urls.append(line + ", " + s)
                    break

        except:
            print("PASSED " + line)

    with open("outputurls.txt", "w") as outputurls:

        for u in urls :
            outputurls.write(u)
            outputurls.write("\n")

if __name__ == "__main__":
    main()
