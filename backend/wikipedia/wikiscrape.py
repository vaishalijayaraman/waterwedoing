#!/usr/bin/python3

import wikipedia
import fnmatch

def main() :
    
    wikipage = wikipedia.page('Flags of the U.S. states and territories')

    stnames = open("states.txt", "rt")
    wildstart = "https://upload.wikimedia.org/wikipedia/commons/?/??/Flag_of_"
    wildend = "*.svg"
    wilds = []
    for line in stnames :
        wilds.append( ( line.strip(), wildstart + line.strip().replace(" ", "_") + wildend ) )

    urls = []
    for s in wikipage.images :
        match = False
        for wild in wilds :
            ls = []
            ls.append(s)
            if len(fnmatch.filter(ls, wild[1])) == 1 :
                urls.append((wild[0], s))
                match = True
                wilds.remove(wild)
                break

    for s in wilds:
        print( "No match for %s" % s[0])

    for u in urls :
        print( u[0] + ", " + u[1] )

if __name__ == "__main__":
    main()
