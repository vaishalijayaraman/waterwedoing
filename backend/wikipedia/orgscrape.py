

import wikipedia
import random

def main() :

	orgnames = open("Organization Names List.txt", "rt")

	urls = []

	for line in orgnames :

		curline = line
		finding = True

		while(finding) :

			finding = False
			print(line)
			wikipage = wikipedia.search(line)
			if len(wikipage) != 0:
				page = None
				try:
					page = wikipedia.page(wikipage[0])
				except wikipedia.DisambiguationError as e:
					print("Was ambiguous!!! choosing randomly...")
					s = None
					s = e.options[0]
					print("Chose " + s)
					try:
						page = wikipedia.page(s.title)
					except:
						continue
				print(page)
				print(page.title)
				if len(page.images) != 0:
					finding = False
					s = page.images[0]
					print("url = " + s)
					urls.append(line.replace('\n', "") + "," + s)
				else:
					print("Removing word from title...")
					finding = True
					idx = curline.rfind(" ")
					if (idx == -1):
						finding = False
					else:
						curline = curline[0 : idx]
						print("New title: " + curline)

		with open("orgurls.txt", "w") as outputurls:

			for u in urls :
				print(u)
				outputurls.write(u)
				outputurls.write("\n")

if __name__ == "__main__":
	main()
