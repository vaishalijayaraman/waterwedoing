UPDATE Organizations
	SET org_image_url = 'https://upload.wikimedia.org/wikipedia/commons/0/07/AlapahaRiver2002.jpg'
	WHERE org_name = 'USGS Mississippi Water Science Center';
UPDATE Organizations
	SET org_image_url = 'https://upload.wikimedia.org/wikipedia/commons/1/1b/Erythemal_action_spectrum.svg'
	WHERE org_name = 'FL Dept. of Environmental Protection';
UPDATE Organizations
	SET org_image_url = 'https://upload.wikimedia.org/wikipedia/commons/a/a4/Broken_Bow%2C_Nebraska_bandstand_from_E.JPG'
	WHERE org_name = 'Nebraska Department of Environmental Quality';
UPDATE Organizations
	SET org_image_url = 'https://upload.wikimedia.org/wikipedia/commons/5/51/Noizy-4.jpg'
	WHERE org_name = 'GBMc & Associates';
UPDATE Organizations
	SET org_image_url = 'https://upload.wikimedia.org/wikipedia/commons/7/75/Bureau_of_Reclamation_regions.png'
	WHERE org_name = 'Bureau of Reclamation';
UPDATE Organizations
	SET org_image_url = 'https://upload.wikimedia.org/wikipedia/commons/0/05/A_BULLDOZER_PILES_BOULDERS_IN_AN_ATTEMPT_TO_PREVENT_LAKE_SHORE_EROSION_-_NARA_-_547031.jpg'
	WHERE org_name = 'EPA R7';
UPDATE Organizations
	SET org_image_url = 'https://upload.wikimedia.org/wikipedia/commons/e/ee/Elkhart-indiana-st-joe-river.jpg'
	WHERE org_name = 'USGS Indiana Water Science Center';
UPDATE Organizations
	SET org_image_url = 'https://upload.wikimedia.org/wikipedia/commons/1/14/2008_Farm_Bill_logo_%28USA%29.jpg'
	WHERE org_name = 'Alaska Soil and Water Conservation District';
UPDATE Organizations
	SET org_image_url = 'https://upload.wikimedia.org/wikipedia/commons/e/e6/Flag_of_Alaska.svg'
	WHERE org_name = 'Georgetown Tribal Council';
UPDATE Organizations
	SET org_image_url = 'https://upload.wikimedia.org/wikipedia/commons/c/ca/2_USGA_ANWR_Oil.png'
	WHERE org_name = 'Alaska Department of Environmental Conservation (BEACH)';
UPDATE Organizations
	SET org_image_url = 'https://upload.wikimedia.org/wikipedia/commons/e/e7/California_map_showing_Del_Norte_County.png'
	WHERE org_name = 'Elk Valley Rancheria';
UPDATE Organizations
	SET org_image_url = 'https://upload.wikimedia.org/wikipedia/commons/f/ff/1st_Army.svg'
	WHERE org_name = 'Georgia Environmental Protection Division';
UPDATE Organizations
	SET org_image_url = 'https://upload.wikimedia.org/wikipedia/commons/9/93/1732_map_of_Maryland.jpg'
	WHERE org_name = 'Maryland Department of the Environment Beaches Data';
UPDATE Organizations
	SET org_image_url = 'https://upload.wikimedia.org/wikipedia/commons/f/fa/America_East_Conference_logo.svg'
	WHERE org_name = 'UNIVERSITY OF NEW HAMPSHIRE';
UPDATE Organizations
	SET org_image_url = 'https://upload.wikimedia.org/wikipedia/commons/a/a2/2003_AL_Proof.png'
	WHERE org_name = 'USGS Alabama Water Science Center';
UPDATE Organizations
	SET org_image_url = 'https://upload.wikimedia.org/wikipedia/commons/b/b7/Boundary_Map_of_the_Kenai_National_Wildlife_Refuge.jpg'
	WHERE org_name = 'Kenai National Wildlife Refuge';
UPDATE Organizations
	SET org_image_url = 'https://upload.wikimedia.org/wikipedia/commons/a/ae/AKMap-doton-Soldotna.PNG'
	WHERE org_name = 'Kenai Watershed Forum';
UPDATE Organizations
	SET org_image_url = 'https://upload.wikimedia.org/wikipedia/commons/b/bf/Aegopodium_podagraria1_ies.jpg'
	WHERE org_name = 'Division of Drinking and Ground Water (Ohio)';
UPDATE Organizations
	SET org_image_url = 'https://upload.wikimedia.org/wikipedia/commons/0/05/A_BULLDOZER_PILES_BOULDERS_IN_AN_ATTEMPT_TO_PREVENT_LAKE_SHORE_EROSION_-_NARA_-_547031.jpg'
	WHERE org_name = 'EPA Region 10 Superfund Historical Data';
UPDATE Organizations
	SET org_image_url = 'https://upload.wikimedia.org/wikipedia/commons/5/56/Beach_at_Seldovia%2C_Alaska%2C_June_21%2C_1908_%28COBB_175%29.jpeg'
	WHERE org_name = 'Seldovia Village Tribe';
UPDATE Organizations
	SET org_image_url = 'https://upload.wikimedia.org/wikipedia/commons/0/01/Flag_of_California.svg'
	WHERE org_name = 'Smith River Rancheria (California)';
UPDATE Organizations
	SET org_image_url = 'https://upload.wikimedia.org/wikipedia/commons/b/bf/Aegopodium_podagraria1_ies.jpg'
	WHERE org_name = 'Division of Drinking and Ground Water (Ohio)';
UPDATE Organizations
	SET org_image_url = 'https://upload.wikimedia.org/wikipedia/commons/6/6e/Flag_of_Oklahoma.svg'
	WHERE org_name = 'Oklahoma Corporation Commission';
UPDATE Organizations
	SET org_image_url = 'https://upload.wikimedia.org/wikipedia/commons/9/9d/Flag_of_Arizona.svg'
	WHERE org_name = 'Ak-Chin Indian Community';
UPDATE Organizations
	SET org_image_url = 'https://upload.wikimedia.org/wikipedia/commons/f/f5/1940_govt_photo_minnesota_farming_scene_chippewa_baby_teething_on_magazine_indians_at_work.jpg'
	WHERE org_name = 'Red Lake Band of Chippewa (MN)';
UPDATE Organizations
	SET org_image_url = 'https://upload.wikimedia.org/wikipedia/commons/1/13/Field_dressing_a_deer_23.jpg'
	WHERE org_name = 'Oklahoma Dept. of Agriculture';
UPDATE Organizations
	SET org_image_url = 'https://upload.wikimedia.org/wikipedia/commons/d/dd/BradhenryDEA.jpg'
	WHERE org_name = 'OWRB Streams Monitoring';
UPDATE Organizations
	SET org_image_url = 'https://upload.wikimedia.org/wikipedia/commons/9/94/Post0227_-_Flickr_-_NOAA_Photo_Library.jpg'
	WHERE org_name = 'Alaska Monitoring and Assessment Program';
UPDATE Organizations
	SET org_image_url = 'https://upload.wikimedia.org/wikipedia/commons/9/9d/Flag_of_Arizona.svg'
	WHERE org_name = 'Fort McDowell Yavapai Nation';
UPDATE Organizations
	SET org_image_url = 'https://upload.wikimedia.org/wikipedia/commons/d/d9/20161001_Kurtkowiec_i_Czerwone_Stawy_G%C4%85sienicowe_1730.jpg'
	WHERE org_name = 'North American Lake Management Society';
UPDATE Organizations
	SET org_image_url = 'https://upload.wikimedia.org/wikipedia/commons/9/9d/Flag_of_Arizona.svg'
	WHERE org_name = 'Salt River Pima-Maricopa Indian Community (SRPMIC)';
UPDATE Organizations
	SET org_image_url = 'https://upload.wikimedia.org/wikipedia/commons/9/9d/Flag_of_Arizona.svg'
	WHERE org_name = 'Salt River Pima-Maricopa Indian Community of the Salt River Reservation';
UPDATE Organizations
	SET org_image_url = 'https://upload.wikimedia.org/wikipedia/commons/4/48/US_river_basin_map.gif'
	WHERE org_name = 'Arkansas Water Resources Center';
UPDATE Organizations
	SET org_image_url = 'https://upload.wikimedia.org/wikipedia/commons/a/a6/High_plains_fresh_groundwater_usage_2000.svg'
	WHERE org_name = 'USGS Nevada Water Science Center';
UPDATE Organizations
	SET org_image_url = 'https://upload.wikimedia.org/wikipedia/commons/3/34/Anti-nuclear_protest_at_the_NTS_3.jpg'
	WHERE org_name = 'Nevada Division of Environmental Protection';
UPDATE Organizations
	SET org_image_url = 'https://upload.wikimedia.org/wikipedia/commons/4/4c/Angie_Bulletts_Paiute.jpg'
	WHERE org_name = 'Kaibab Band of Paiute Indians of the Kaibab Indian Reservation';
UPDATE Organizations
	SET org_image_url = 'https://upload.wikimedia.org/wikipedia/commons/b/b8/Anim7_us.gif'
	WHERE org_name = 'USGS Louisiana Water Science Center';
UPDATE Organizations
	SET org_image_url = 'https://upload.wikimedia.org/wikipedia/commons/3/38/Chicago_River_Bascule_Bridge%2C_LaSalle_Street%2C_Chicago.jpg'
	WHERE org_name = 'National Park Service Water Resources Division';
UPDATE Organizations
	SET org_image_url = 'https://upload.wikimedia.org/wikipedia/commons/0/01/Flag_of_California.svg'
	WHERE org_name = 'California Department Of Water Resources';
UPDATE Organizations
	SET org_image_url = 'https://upload.wikimedia.org/wikipedia/commons/0/01/Flag_of_California.svg'
	WHERE org_name = 'Mechoopda Indian Tribe of Chico Rancheria';
UPDATE Organizations
	SET org_image_url = 'https://upload.wikimedia.org/wikipedia/commons/0/01/Flag_of_California.svg'
	WHERE org_name = 'Resighini Rancheria';
UPDATE Organizations
	SET org_image_url = 'https://upload.wikimedia.org/wikipedia/commons/0/01/Flag_of_California.svg'
	WHERE org_name = 'Tolowa Dee-ni Nation (Smith River Rancheria)';
UPDATE Organizations
	SET org_image_url = 'https://upload.wikimedia.org/wikipedia/commons/0/01/Flag_of_California.svg'
	WHERE org_name = 'Table Mountain Rancheria of California';
UPDATE Organizations
	SET org_image_url = 'https://upload.wikimedia.org/wikipedia/commons/d/d9/Flag_of_Canada_%28Pantone%29.svg'
	WHERE org_name = 'Sarasota County Environmental Services (Florida)';
UPDATE Organizations
	SET org_image_url = 'https://upload.wikimedia.org/wikipedia/commons/b/b8/Anim7_us.gif'
	WHERE org_name = 'USGS Colorado Water Science Center';
UPDATE Organizations
	SET org_image_url = 'https://upload.wikimedia.org/wikipedia/commons/0/07/Entrance_to_Infinity_Park_in_Glendale_Colorado.jpg'
	WHERE org_name = 'City of Glendale (Colorado)';
UPDATE Organizations
	SET org_image_url = 'https://upload.wikimedia.org/wikipedia/commons/5/52/CO_Mineral_Belt.jpg'
	WHERE org_name = 'Southern Ute Tribe';
UPDATE Organizations
	SET org_image_url = 'https://upload.wikimedia.org/wikipedia/commons/d/d2/20180221-OSEC-LSC-0055_%2839518128545%29.jpg'
	WHERE org_name = 'Environmental Monitoring and Assessment Program';
UPDATE Organizations
	SET org_image_url = 'https://upload.wikimedia.org/wikipedia/commons/e/eb/Angel_Goodrich_at_2_August_2015_game_cropped.jpg'
	WHERE org_name = 'Cherokee Nation (Oklahoma)';
UPDATE Organizations
	SET org_image_url = 'https://upload.wikimedia.org/wikipedia/commons/1/1e/11eforsyth.JPG'
	WHERE org_name = 'City of Jacksonville';
UPDATE Organizations
	SET org_image_url = 'https://upload.wikimedia.org/wikipedia/commons/c/ca/Americas_%28orthographic_projection%29.svg'
	WHERE org_name = 'ALABAMA DEPT. OF ENVIRONMENTAL MANAGEMENT - WATER QUALITY DATA';
UPDATE Organizations
	SET org_image_url = 'https://upload.wikimedia.org/wikipedia/commons/e/e4/DEP-Logo-Web-Color.png'
	WHERE org_name = 'FL Dept. of Environmental Protection';
UPDATE Organizations
	SET org_image_url = 'https://upload.wikimedia.org/wikipedia/commons/b/bf/Aegopodium_podagraria1_ies.jpg'
	WHERE org_name = 'MISSISSIPPI DEPARTMENT OF ENVIRONMENTAL QUALITY';
UPDATE Organizations
	SET org_image_url = 'https://upload.wikimedia.org/wikipedia/commons/7/7b/A_Show_of_Power.tif'
	WHERE org_name = 'Slide Mine  Boulder County Colorado';
UPDATE Organizations
	SET org_image_url = 'https://upload.wikimedia.org/wikipedia/commons/6/6a/Northern_Colorado_Water_Conservancy_District_sign.JPG'
	WHERE org_name = 'Northern Colorado Water Conservancy District (Colorado)';
UPDATE Organizations
	SET org_image_url = 'https://upload.wikimedia.org/wikipedia/commons/e/e0/Algae_in_water_flowing_from_Argo_Tunnel.jpg'
	WHERE org_name = 'Clear Creek  Superfund (Colorado)';
UPDATE Organizations
	SET org_image_url = 'https://upload.wikimedia.org/wikipedia/en/9/99/Question_book-new.svg'
	WHERE org_name = 'California State Water Resources Control Board';
UPDATE Organizations
	SET org_image_url = 'https://upload.wikimedia.org/wikipedia/commons/9/9f/Bollinger_mill_dec29-07_%2837%29.JPG'
	WHERE org_name = 'Missouri Dept. of Natural Resources';
UPDATE Organizations
	SET org_image_url = 'https://upload.wikimedia.org/wikipedia/commons/0/01/Flag_of_California.svg'
	WHERE org_name = 'California Department of Water Resources';
UPDATE Organizations
	SET org_image_url = 'https://upload.wikimedia.org/wikipedia/commons/8/83/Adams_County_and_Arapahoe_County_and_Douglas_County_Colorado_Incorporated_and_Unincorporated_areas_Aurora_Highlighted_0804000.svg'
	WHERE org_name = 'City of Aurora (Colorado)';
UPDATE Organizations
	SET org_image_url = 'https://upload.wikimedia.org/wikipedia/commons/5/56/Eelrivermap.png'
	WHERE org_name = 'Standley Lake Watershed Group';
UPDATE Organizations
	SET org_image_url = 'https://upload.wikimedia.org/wikipedia/commons/0/00/2014-11-02_13_56_54_Sassafras_foliage_during_autumn_along_Poor_Farm_Road_in_Hopewell_Township%2C_New_Jersey.jpg'
	WHERE org_name = 'Northeast Iowa RC&D (USDA)';
UPDATE Organizations
	SET org_image_url = 'https://upload.wikimedia.org/wikipedia/commons/9/96/Flag_of_Connecticut.svg'
	WHERE org_name = 'Connecticut Department Of Energy And Environmental Protection';
UPDATE Organizations
	SET org_image_url = 'https://upload.wikimedia.org/wikipedia/commons/3/32/Delaware_River_Basin_Commission_map.svg'
	WHERE org_name = 'Delaware River Basin Commission';
UPDATE Organizations
	SET org_image_url = 'https://upload.wikimedia.org/wikipedia/commons/9/9c/1630_Hondius_Map_of_Virginia_and_the_Chesapeake_-_Geographicus_-_NovaVirginiaeTabula-hondius-1630.jpg'
	WHERE org_name = 'Chesapeake Bay Program (CBP)';
UPDATE Organizations
	SET org_image_url = 'https://upload.wikimedia.org/wikipedia/commons/1/12/Kiapojf2.JPG'
	WHERE org_name = 'USGS Florida Water Science Center';
UPDATE Organizations
	SET org_image_url = 'https://upload.wikimedia.org/wikipedia/commons/c/cc/AHR-logo.png'
	WHERE org_name = 'St.  Johns River Water Management District';
UPDATE Organizations
	SET org_image_url = 'https://upload.wikimedia.org/wikipedia/commons/8/84/Alt_plate.svg'
	WHERE org_name = 'Suwannee River Water Management District (Florida)';
UPDATE Organizations
	SET org_image_url = 'https://upload.wikimedia.org/wikipedia/commons/4/4b/Areas_Covered_by_the_BEACH_Act_of_2000.jpg'
	WHERE org_name = 'Indiana STORET';
UPDATE Organizations
	SET org_image_url = 'https://upload.wikimedia.org/wikipedia/en/b/bc/EFCO_LOGO.jpg'
	WHERE org_name = 'Des Moines River - Corp of Engineers (IOWA)';
UPDATE Organizations
	SET org_image_url = 'https://upload.wikimedia.org/wikipedia/commons/3/32/Delaware_River_Basin_Commission_map.svg'
	WHERE org_name = 'Delaware River Basin Commission';
UPDATE Organizations
	SET org_image_url = 'https://upload.wikimedia.org/wikipedia/commons/b/bf/Aegopodium_podagraria1_ies.jpg'
	WHERE org_name = 'Maryland Department Of  The  Environment Dredging Ambient Data';
UPDATE Organizations
	SET org_image_url = 'https://upload.wikimedia.org/wikipedia/commons/f/fb/Campephilus_principalisAO5F391LA-frBrehm.jpg'
	WHERE org_name = 'Choctawhatchee Basin Alliance (Florida)';
UPDATE Organizations
	SET org_image_url = 'https://upload.wikimedia.org/wikipedia/commons/f/fb/Campephilus_principalisAO5F391LA-frBrehm.jpg'
	WHERE org_name = 'Choctawhatchee Basin Alliance';
UPDATE Organizations
	SET org_image_url = 'https://upload.wikimedia.org/wikipedia/commons/b/bf/Aegopodium_podagraria1_ies.jpg'
	WHERE org_name = 'Maryland Dept. of the Environment Toxics Data';
UPDATE Organizations
	SET org_image_url = 'https://upload.wikimedia.org/wikipedia/commons/0/08/943_AP_Boardwalk.JPG'
	WHERE org_name = 'Monmouth County Health Department';
UPDATE Organizations
	SET org_image_url = 'https://upload.wikimedia.org/wikipedia/commons/7/79/American_Athletic_Conference_logo.svg'
	WHERE org_name = 'Environmental Quality Laboratory Coastal Carolina University';
UPDATE Organizations
	SET org_image_url = 'https://upload.wikimedia.org/wikipedia/commons/b/b8/Anim7_us.gif'
	WHERE org_name = 'USGS Alaska Water Science Center';
UPDATE Organizations
	SET org_image_url = 'https://upload.wikimedia.org/wikipedia/commons/a/a6/High_plains_fresh_groundwater_usage_2000.svg'
	WHERE org_name = 'USGS New Jersey Water Science Center';
UPDATE Organizations
	SET org_image_url = 'https://upload.wikimedia.org/wikipedia/commons/f/f7/Flag_of_Florida.svg'
	WHERE org_name = 'Florida Fish & Wildlife C C / Marine Research Institute';
UPDATE Organizations
	SET org_image_url = 'https://upload.wikimedia.org/wikipedia/commons/d/d2/20180221-OSEC-LSC-0055_%2839518128545%29.jpg'
	WHERE org_name = 'Environmental Monitoring and Assessment Program';
UPDATE Organizations
	SET org_image_url = 'https://upload.wikimedia.org/wikipedia/commons/b/bf/Aegopodium_podagraria1_ies.jpg'
	WHERE org_name = 'Alaska Dept of Environmental Conservation - Water Quality';
UPDATE Organizations
	SET org_image_url = 'https://upload.wikimedia.org/wikipedia/commons/f/fd/Seal_MWRD_Chicago.JPG'
	WHERE org_name = 'Metropolitan Water Reclamation District of Greater Chicago';
UPDATE Organizations
	SET org_image_url = 'https://upload.wikimedia.org/wikipedia/commons/4/46/Flag_of_Colorado.svg'
	WHERE org_name = 'Colorado Dept. of Public Health & Environment';
