import io, csv

with open('stateflagsql.txt', 'w') as sql_file:
	with open('stateflags.csv', 'r') as flags_csv:
		flag = csv.DictReader(flags_csv, delimiter=',')
		for row in flag:
			sql_file.write("UPDATE States\n\tSET state_flag_url = '" + row["URL"] + "'\n\tWHERE state_name = '" + row["State"] + "';\n")
