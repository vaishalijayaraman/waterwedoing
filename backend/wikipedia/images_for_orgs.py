import io, csv

with open('org_image_sql.txt', 'w') as sql_file:
	with open('Urls_orgs.csv', 'r') as image_csv:
		thumb = csv.DictReader(image_csv, delimiter=',')
		for row in thumb:
			sql_file.write("UPDATE Organizations\n\tSET org_image_url = '" + row["URL"] + "'\n\tWHERE org_name = '" + row["Organization"] + "';\n")
